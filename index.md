---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
Title: Bienvenue
layout: page
---

* [Présentation AAF : web de données et archives](presentations/aaf/webData/formationAAF.html)
* [SEDA : principes et mise en oeuvre](presentations/ae/formation-seda-CD54.html)
* [Les enjeux de l'archivage électronique](presentations/ae/formation-CD54-enjeux.html)

