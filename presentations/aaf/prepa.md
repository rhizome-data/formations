# Idées pour la formation Webdata AAF

## Contenu

A. jour 1
1. co-construction programme et attentes (9h-10h)
2. Contexte et enjeux
3. Présentation de l'historique du web et des données liées (10h-12h)
   1. 1 Au commencement était l’hypertexte
   2. Qu’est ce que le Web sémantique ?
4. Présentation du modèle de structuration par ontologie (14h-16h)
   1. Les concepts du Web sémantique
   2. Applications
5. Enjeux et prespectives : publication, visualisation, alignement (16h-17h30)
   1. Métadonnées culturelles et transition web 3.0
   2. Publication
   3. cycle de vie
   4. enjeux
     JOUR 2
6. présentation de SKOS (9h-10h30)
7. Présentation du modèle de données RIC (10h30-12h30)
8. présentation de l'outillage (14h-17h)
   1. Traitements
   2. SPARQL
   3. Intégration et transformation
   C. JOUR 3
9. exercices
- construire un vocabulaire skos avec cubicweb (une instance de saemref ou de sherpa à déployer) (9h-10h30)
- importer un fichier ods dans [skosPlay](http://labs.sparna.fr/skos-play/) (10h30-12h)
- faire une ontologie dans [WebProtegé](https://webprotege.stanford.edu/) (14h-15h30)
- utilisation de l'ontologie RIC (15h30-17h)
  - transformation d'un fichier xml-ead et d'un fichier xml-eac et alignement
  - visualisation des rebonds
  - évaluation (17h-17h30)

## Attentes des participant.es

- comprendre comment s'articulent les outils et normes et voir comment les appliquer ou les demander avec les outils que j'utilise
- diffusion et partage des connaissances sur le web
- méthodologie de mise en oeuvre, pré-requis et outils
- interopabilité des schémas pour la mise en relation
- méthodologie de mise en relation
- outils

---



## 1. co-construction programme et attentes (9h-10h)

## 2. Contexte et enjeux

### 2.1 Evolution des pratiques des utilisateurs

La recherche d'information aujourd'hui pour beaucoup de gens c'est le web:

![image recherche info : WEB](./images/rechercheWEB.png)

---

### 2.1 Evolution des pratiques des utilisateurs

Les archives sur le WEB aujourd'hui c'est :

Exemple : Jacques Chaban-Delmas

![image recherche Chaban-Delmas](./images/recherche-chaban.png)

---

### 2.1 Evolution des pratiques des utilisateurs

La connexion sur les sites des services d'archives (*Qui sont les publics des Archives ?*, SIAF, 2015)

![comment le lecteur se connecte sur les sites des services archives](./images/comment-arrive-site-archives.png)

---

### 2.1 Evolution des pratiques des utilisateurs

Le but d'une visite sur un site d'archives aujourd'hui c'est : 

![Le but d'une visite sur un site d'archives](./images/but-visite-archives.png)

---

### 2.2 Enjeux liés aux utilisateurs du Web

**Enjeux liés à une meilleure prise en compte du public dans toute sa diversité** 

<u>Adopter les standards du Web sémantique</u> : 

- Pour sortir nos instruments de recherche et nos notices d'autorité du Web profond 
- Pour aller à la rencontre de nouveaux utilisateurs sur le Web et sortir d'un public "d'habitués"

<u>Modifier la structure de l’information archivistique</u> : 

- Pour adapter nos sites aux besoins des utilisateurs et à leurs nouvelles pratiques de recherche 
- Pour mieux structurer les résultats d’une recherche simple
- Pour ouvrir les résultats vers de nouveaux horizons (favoriser la sérendipité)

---

### 2.2 Enjeux pour les archivistes

<u>Adopter les standards du Web de données</u> : 

- Pour enrichir leurs descriptions par l’apport de données externes publiées par d’autres fournisseurs du Web les rendant ainsi plus complètes et leur conférant une valeur ajoutée.
- Pour assurer la traçabilité des archives numériques

<u>Modifier la structure de l’information archivistique</u> :

- Pour ne pas avoir à répéter indéfiniment les mêmes informations d'un instrument de recherche à un autre ou d'une notice d'autorité à une autre ou d'un profil SEDA à un autre.

- Pour harmoniser/normaliser l’information fournie

Le Web de données représente une **opportunité** nouvelle pour diffuser et favoriser la réutilisation des métadonnées descriptives archivistiques. 

---

### 2.3 Enjeux économiques

Adopter les standards du Web de données : 

- Pour sortir de notre isolement technologique 

- Pour avancer du même pas que les autres, notamment que le monde des bibliothèques

- Pour qu’à terme, la maintenance de nos sites coûte moins cher

- Mutualiser le travail des archivistes 

---

### 2.4 La feuille de route sur les métadonnées culturelles et la transition Web 3.0 du ministère dela Culture et de la Communication

9 actions opérationnelles permettant au secteur culturel de prendre en compte les opportunités et les défis qu'offre le 3.0.

<u>Les 4 actions les plus significatives :</u> 

- L’identification normée et pérenne des ressources accessibles par Internet est un élément essentiel du Web sémantique.
- L’identification des auteurs est indispensable à la diffusion des ressources culturelles sur le Web : L’identifiant   international   ISNI   (International   Standard   Name   Identifier) 
- Créer et maintenir une interconnexion sémantique des grands référentiels culturels : le "graphe culture"
- Expérimenter l’interconnexion inter-institutionnelle des données culturelles



## 3. Présentation de l'histoire du web et des données liées (10h-12h)

### 3.1 Naissance d'internet

![naissance d'internet](./images/naissance-internet.png)

---

### 3.2 Le Web 

![histoire du web](./images/Web-histoire.png)

---

### 3.2 Le Web

**Les problématiques du Web actuel** 

- Il est de plus en plus volumineux et diversifié

- Les informations et les services sont de moins en moins exploitables

- Il n’est pas fait pour être manipulé de façon intelligente par les programmes informatiques

- Il engendre des frustrations dans la recherche    

- Perte de temps considérable avant d’obtenir l’information désirée

---

### 3.3 Qu’est ce que le Web sémantique ?











---

### 3.4 Web d'aujourd'hui versus Web de données

![Le but d'une visite sur un site d'archives](./images/web-versus-websem.png)



## 4.Présentation du modèle de structuration par ontologie (14h-16h)

1. Les concepts du Web sémantique
2. Applications

## Le modèle FRBR, récement refondu dans le modèle unique IFLA - LRM : modèle de données de la transition bibliographique 

### Modèle FRBR

​     Le modèle de données de [data.bnf.fr](http://data.bnf.fr/) expérimente et adapte le modèle [ le modèle FRBR (fonctionnalités requises pour les notices bibliographiques)     ](http://www.ifla.org/publications/functional-requirements-for-bibliographic-records) élaboré par la fédération internationale des associations et institutions de bibliothèques, l’[IFLA](http://ifla.org). [Data.bnf.fr](http://data.bnf.fr/) s’inscrit donc dans les évolutions récentes en matière de description bibliographique.    

Ce modèle comprend trois groupes d'entités liées par des  relations : les informations sur les documents, les personnes physiques  ou morales, et les thèmes. 

---



Des données structurées et enrichies qui ne doivent pas  nous faire perdre de vue les finalités de la Transition bibliographique  et notamment le confort de l’usager dans les cinq grandes « tâches  utilisateurs » :

1. *Trouver* : Rechercher tout critère pertinent afin de rassembler des informations sur une ou plusieurs ressources présentant un intérêt ;
2. *Identifier* : Comprendre clairement la nature des ressources trouvées et faire la distinction entre des ressources similaires ;
3. *Sélectionner* : Déterminer l’adaptation de la ressource trouvée et choisir (en acceptant ou rejetant) des ressources spécifiques ;
4. *Obtenir* : Accéder au contenu de la ressource ;
5. *Naviguer* : Utiliser les relations qui existent entre une ressource et une autre pour les situer dans un contexte.

## 5.Enjeux et prespectives : publication, visualisation, alignement (16h-17h30)

Métadonnées culturelles et transition web 3.0

**Exposition en Données liées (linked data) du Thésaurus pour l’indexation des archives locales.**
Le rôle des référentiels et vocabulaires contrôlés est de plus en plus déterminant dans la découverte des ressources. Les Archives de France ont donc saisi l’opportunité d’une révision de ce thésaurus en 2009 pour
expérimenter les opportunités offertes par le nouveau standard de structuration SKOS et le nouveau mode de publication proposés par le consortium W3C. Depuis 2011, un [site Web](http://data.culture.fr/thesaurus/) permet donc de naviguer au sein du thésaurus, d’en télécharger une version RDF/XML, mais surtout de l’interroger via le langage SPARQL qui est le langage de requête des données structurées en RDF, ces aspects de standardisation facilitant l’interopérabilité avec les systèmes qui utilisent ce référentiel. 

Ce projet a également été l’occasion d’expérimenter le décloisonnement des données en les alignant sur d’autres référentiels (le vocabulaire Rameau ainsi que DBPedia) et de travailler sur le choix et la mise en œuvre d’une solution pour la permanence des identifiants manipulés dans ce référentiel. La plate-forme de consultation du thésaurus a récemment fait l’objet d’importantes évolutions, avec l’intégration d’autres vocabulaires contrôlés du ministère de la Culture et de la Communication (MCC), notamment des services du patrimoine et de l’architecture. L’objectif est de créer un référentiel terminologique unifié permettant d’offrir aux usagers un accès unique et cohérent aux ressources terminologiques produites par le MCC et d’en démultiplier les usages.

1. Publication

2. cycle de vie

3. enjeux

    

   # JOUR 2

4. présentation de SKOS (9h-10h30)
5. Présentation du modèle de données RIC (10h30-12h30)
6. présentation de l'outillage (14h-17h)
   1. Traitements
   2. SPARQL
   3. Intégration et transformation
      C. JOUR 3
7. exercices

## A tester

- [skos editor](https://code.google.com/archive/p/skoseditor/)
- [Convert to RDF](https://www.w3.org/wiki/ConverterToRdf#Excel)
- [apache any23](http://any23.apache.org/)

## (In)Formations complémentaires

- MOOC Inria Web sémantique
- [PoolParty academy](https://www.poolparty.biz/academy/)
- [éditeurs d'ontologies](https://www.w3.org/wiki/Ontology_editors)
- [Bonnes pratiques pour publier un vocabulaire rdf](https://www.w3.org/TR/swbp-vocab-pub/)
- [tutorial Protégé Ontologie](https://protegewiki.stanford.edu/wiki/Ontology101)
- [liste d'outils pour ontologies](http://wiki.opensemanticframework.org/index.php/Ontology_Tools)

## Exemples

- [protégé](https://webprotege.stanford.edu/#projects/list)
- [BBC::Ontologie de la nourriture](https://www.bbc.co.uk/ontologies/fo)



## Bibiographie

*Qui sont les publics des Archives ? : enquêtes sur les lecteurs, les internautes et le public des activités culturelles dans les services publics d’archives ( 2013-2014)*, SIAF, 2015 : https://francearchives.fr/file/08ccbaa3654282501138a7739ac59dbecc364552/static_8431.pdf

Métadonnées culturelles et transition web 3.0 : http://www.culture.gouv.fr/var/culture/storage/pub/feuille_de_route__metadonnees_culturelles_et_transition_web_3_0_janvier_2014/index.htm

Transition bibliographique : https://www.transition-bibliographique.fr/se-former/supports/

Source Flickr : https://www.flickr.com/photos/safari_vacation/6315269494/ CC BY-SA

https://news.netcraft.com/archives/2019/