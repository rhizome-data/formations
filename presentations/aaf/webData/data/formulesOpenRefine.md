Name and Coordinates: cell.recon.match.name (will pull the GeoNames Name plus coordinates, separated by a | - you can split that column later to have just name then coordinates)

URI: cell.recon.match.id (will pull the GeoNames URI/link)

Coordinates Only: replace(substring(cell.recon.match.name, indexOf(cell.recon.match.name, ' | ')), ' | ', '')

Name, Coordinates, and URI each separated by | (for easier column splitting later): cell.recon.match.name + " | " + cell.recon.match.id
http://api-adresse.data.gouv.fr/search/?q="+(cells["Adresse"].value+" “+cells[“Code postal”].value+” “+cells[“Ville”].value).replace(“ “,”%20")+”&limit=1

Score: value.parseJson().features[0].properties.score

Type: value.parseJson().features[0].properties.type

Longitude : value.parseJson().features[0].geometry.coordinates[0]

Latitude : value.parseJson().features[0].geometry.coordinates[1]

9 AVENUE ROLAND DORGELES 33120 ARCACHON

http://api-adresse.data.gouv.fr/search/?q="+(cells["9 AVENUE ROLAND DORGELES"].value+" “+cells[“33120”].value+” “+cells[“ARCACHON”].value).replace(“ “,”%20")+”&limit=1