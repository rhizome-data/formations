
class: center, top
background-image: url(images/triplet-rhizome-data-4.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.credit[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data pour le compte de l'Association des Archivistes Français.]

# formation Webdata AAF

---
<!--slide 1-->

## Jour 1
1. co-construction programme et attentes
2. [Enjeux et prespectives](#enjeux)
3. Présentation de l'[historique du web](#web) et des données liées
4. Présentation du modèle de structuration par [ontologie](#ontologie)
5. présentation de [SKOS](#skos)
6. présentation de l'[outillage](#outils)

## Jour 2
7. Présentation du modèle de données [RIC](#ric)

## Jour 3
8. [exercices](#exercices)

---
layout: true
class: left, top
background-image: url(images/logo-nom-triplet.jpg)
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programme1)]

---
<!--slide 2-->
## Attentes des participant.es

* Avoir une meilleure connaissance du web sémantique
* Comprendre comment s'articulent les outils et normes
* diffusion et partage des connaissances sur le web
* méthodologie de mise en oeuvre, pré-requis et outils
* interopabilité des schémas pour la mise en relation
* méthodologie de mise en relation
* outils

---
layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat

# Programme

.footnote[
      [Programme](#programme1)]

---

layout: true
class: left, top
background-image: url(images/logo-nom-triplet.jpg)
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programme1)]

---
name: enjeux

<!--slide 4-->
## 2. Contexte et enjeux

Présentation de l'historique du web et des données liées (2h)
   1. Les usages du web
   2. Enjeux et prespectives
      1. Métadonnées culturelles et transition web 3.0
      2. Publication
      3. cycle de vie
   3. Au commencement était l’hypertexte
   4. Qu’est ce que le Web sémantique ?

---
<!--slide 5-->
### 2.1 Evolution des pratiques des utilisateurs

La recherche d'information aujourd'hui pour beaucoup de gens c'est le web:

.reduite[![image recherche info : WEB](images/rechercheWEB.png)]

---
<!--slide 6-->
### 2.1 Evolution des pratiques des utilisateurs

Les archives sur le WEB aujourd'hui c'est :

Exemple : Jacques Chaban-Delmas

.reduite[![image recherche Chaban-Delmas](images/recherche-chaban.png)]

---
<!--slide 7-->
### 2.1 Evolution des pratiques des utilisateurs

La connexion sur les sites des services d'archives (*Qui sont les publics des Archives ?*, SIAF, 2015)

.reduite[![comment le lecteur se connecte sur les sites des services archives](images/comment-arrive-site-archives.png)]

---
<!--slide 8-->
### 2.1 Evolution des pratiques des utilisateurs

Le but d'une visite sur un site d'archives aujourd'hui c'est : 

.reduite[![Le but d'une visite sur un site d'archives](images/but-visite-archives.png)]

---
<!--slide 9-->
### 2.2 Enjeux liés aux utilisateurs du Web

**Enjeux liés à une meilleure prise en compte du public dans toute sa diversité** 

**Adopter les standards du Web sémantique** : 

* Pour sortir nos instruments de recherche et nos notices d'autorité du Web profond
* Pour aller à la rencontre de nouveaux utilisateurs sur le Web et sortir d'un public "d'habitués"
* Modifier la structure de l’information archivistique :
  * Pour adapter nos sites aux besoins des utilisateurs et à leurs nouvelles pratiques de recherche
  * Pour mieux structurer les résultats d’une recherche simple
  * Pour ouvrir les résultats vers de nouveaux horizons (favoriser la sérendipité)

---
<!--slide 10-->
### 2.2 Enjeux pour les archivistes

**Adopter les standards du Web de données** :

* Pour enrichir leurs descriptions par l’apport de données externes publiées par d’autres fournisseurs du Web les rendant ainsi plus complètes et leur conférant une valeur ajoutée.
* Pour assurer la traçabilité des archives numériques
* Pour ne pas avoir à répéter indéfiniment les mêmes informations d'un instrument de recherche à un autre ou d'une notice d'autorité à une autre ou d'un profil SEDA à un autre.
* Pour harmoniser/normaliser l’information fournie

Le Web de données représente une **opportunité** nouvelle pour **diffuser et favoriser la réutilisation** des métadonnées descriptives archivistiques.

---
<!--slide 11-->
### 2.3 Enjeux économiques

Adopter les standards du Web de données : 

* Pour sortir de notre **isolement technologique** 
* Pour avancer du même pas que les autres, notamment que le monde des bibliothèques
* Pour qu’à terme, la maintenance de nos sites coûte moins cher
* **Mutualiser** le travail des archivistes 

---
<!--slide 12-->
#### 2.4 La feuille de route sur les métadonnées culturelles 

9 actions opérationnelles permettant au secteur culturel de prendre en compte les opportunités et les défis liés à la transition Web 3.0 du ministère dela Culture et de la Communication

Les 4 actions les plus significatives dans le domaine des Archives :

* L’identification **normée et pérenne** des ressources accessibles par Internet est un élément essentiel du Web sémantique.
* L’**identification des producteurs** est indispensable à la diffusion des ressources culturelles sur le Web : L’identifiant   international   ISNI   (International   Standard   Name   Identifier) 
* Créer et maintenir une interconnexion sémantique des **grands référentiels culturels** : le "graphe culture"
* Expérimenter l’**interconnexion inter-institutionnelle** des données culturelles

---
name: web
<!--slide 13-->
### 3. Présentation de l'histoire du web et des données liées

#### 3.1 Naissance d'internet

.reduite[![naissance d'internet](images/naissance-internet.png)]

---
<!--slide 14-->
#### 3.1 Le Web 

.reduite[![histoire du web](images/Web-histoire.png)]

---
<!--slide 15-->
#### 3.1 Web : a proposal (1989)

![Le draft de Tim Berners Lee](images/tbl99.jpg)

---
<!--slide 16-->
#### 3.2 Le modèle d'architecture du Web

![Le modèle du web](images/tripletWeb.jpg)

.credit[ Mooc Inria : Le web sémantique]

---
<!--slide 17-->
#### 3.2 Les problématiques du Web actuel

* Il est de plus en plus volumineux et diversifié
* Les informations et les services sont de moins en moins exploitables
* Il n’est pas fait pour être manipulé de façon intelligente par les programmes informatiques
* Il engendre des frustrations dans la recherche
* Perte de temps considérable avant d’obtenir l’information désirée

---
<!--slide 18-->
#### 3.3 Qu'est ce que le web de données ?

- Tim Berners-Lee :

  > « Je rêve d'un Web [dans lequel les  ordinateurs] deviennent capables d'analyser toutes les données du Web :  le contenu, les liens, et les transactions entre personnes et ordinateurs. Un « Web Sémantique », qui devrait rendre cela possible,  n'a pas encore émergé, mais quand il le fera, les mécanismes journaliers  du commerce, de l'administration et de nos vies quotidiennes seront  traités par des machines dialoguant avec d'autres machines. Les « agents  intelligents » qu'on nous vante depuis longtemps se concrétiseraient  enfin. »

  — Weaving the Web, 1999.

.reduite[![Logo W3C, Semantic Web](images/Sw-horz-w3c-v.svg.png)]

---
<!--slide 19-->
#### 3.3 Qu'est ce que le web de données ?

>« Le Web sémantique n'est pas un Web distinct mais bien un prolongement du Web que l'on connaît et dans lequel on attribue à l'information une signification clairement définie, ce qui permet aux ordinateurs et aux humains de travailler en plus étroite collaboration.  »

Rendre sémantique le web c'est : 

* permettre aux utilisateurs d'utiliser la totalité du potentiel du Web pour trouver, partager et combiner des informations plus facilement.
* obtenir des résultats plus significatifs tout en aidant les ordinateurs à effectuer de la [collecte d'informations automatisée](https://fr.wikipedia.org/wiki/Crawler) et ainsi faciliter la [recherche](https://fr.wikipedia.org/wiki/Recherche_scientifique).
* compléter les balises HTML par des balises porteuses de sens (XML) ; 
* Le Web sémantique met en œuvre le [Web des données](https://fr.wikipedia.org/wiki/Web_des_données) qui consiste à lier et structurer l'information sur Internet pour accéder simplement à la connaissance qu'elle contient déjà.

---

<!--slide 20-->

#### 3.3 Qu’est ce que le Web de données ?

 **Web d'aujourd'hui versus Web de données**

.reduite[![Le web des documents aux données](images/web-versus-websem.png)]

.credit[
      Crédits : Lydia Carine Mampais KI et Bamba SISSOKO [web sémantique](http://slideplayer.fr/slide/3409713/)
]

---
<!--slide 21-->

#### 3.3 Qu’est ce que le Web de données ?

Le **Web des données** (*linked data*, en anglais) cherche donc à favoriser la publication de données structurées sur le Web, non pas sous la forme de silos de données isolés les uns des autres, mais en les reliant entre elles pour constituer un réseau global d'informations.

Pour le domaine de la Culture il  permet :

* de gagner en visibilité.
* de décloisonner les données des catalogues et des instruments de recherche pour les rendre, afin plus accessibles aux usagers et ceux en devenir.
* de relier des données d'archives, de musées ou des bibliothèques pour offrir au chercheur une information plus complète.

---
<!--slide 22-->

#### 3.3 Qu’est ce que le Web de données ?

.reduite[![De la ratatouille pour les data](images/ratatouille.jpg)]

.credit[Crédits: Gautier Poupeau [notice Viaf](https://viaf.org/viaf/305852061/#Poupeau,_Gautier)]

---

<!--slide 23-->

#### 3.3 Qu’est ce que le Web de données ?

.reduite[![De la datatouille pour les data](images/datatouille.jpg)]

.credit[
      Crédits: Gautier Poupeau [notice Viaf](https://viaf.org/viaf/305852061/#Poupeau,_Gautier)
]

---

<!--slide 24-->

#### 3.3 Qu'est ce que le web de données ?

Je veux décrire des archives avec des informations sur les producteurs
et gérer les liens entre les différentes formes de représentation de ces archives conservées dans un lieu particulier et qui peuvent avoir des archives ou d'autres documents (livres) liés conservés dans des lieux différents.

---

<!--slide 25-->

#### Qu’est-ce que je souhaite décrire ?


* des archives = un intitulé, une description, des dates, des lieux, des sujets
* Un producteur = un nom, un prénom / un nom d'organisme, des dates, des lieux
* Des fonctions = des relations, des organisations, des mandats et des activités
* Des lieux de conservation : une provenance, des activités, des relations

--

<!--slide 26-->

Les ingrédients existants

* spécialisés : EAC / SEDA / EAD
* généralistes : Dublin Core / FOAF / wgs84-pos / time / SKOS / schema.org / PROV-O 
* pour lier : RDF / RDFS / OWL

---

<!--slide 27-->

### 3.4 Les standards du WEB sémantique


* Le [XML](https://fr.wikipedia.org/wiki/XML) : syntaxe élémentaire, pour la structure du contenu dans les  documents, mais il ne décrit pas la sémantique du document. 
* Le [XSD](https://fr.wikipedia.org/wiki/XSD)  : langage de description de format de document XML permettant de  définir la structure et le type de contenu d'un document XML. Cette  définition permet notamment de vérifier la validité de ce document.
* Le [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework) : langage simple pour exprimer des [modèles de données](https://fr.wikipedia.org/wiki/Modèle_de_données) sous forme d'objets ([« ressources »](https://fr.wikipedia.org/wiki/Ressource_du_World_Wide_Web))  et de leurs relations. 
* [RDF Schema](https://fr.wikipedia.org/wiki/RDF_Schema) étend le RDF et son vocabulaire pour pouvoir structurer les propriétés et les classes au sein d'une ressource décrite en RDF.
* [OWL](https://fr.wikipedia.org/wiki/Web_Ontology_Language) ajoute plus de vocabulaire pour décrire les propriétés et les classes : comme avec les relations entre les classes, la [cardinalité](https://fr.wikipedia.org/wiki/Cardinalité_(programmation)), l'égalité, le typage des propriétés,  les caractéristiques de propriétés (par exemple la symétrie), etc.
* [SPARQL](https://fr.wikipedia.org/wiki/SPARQL) : un [langage de requête](https://fr.wikipedia.org/wiki/Langage_de_requête) et un [protocole](https://fr.wikipedia.org/wiki/Protocole_de_communication) qui permet de rechercher, d'ajouter, de modifier ou de supprimer des données [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework) disponibles dans le Web à travers l'internet.

---

<!--slide 28-->

#### 3.4 Web stack

.reduite[![Les standards du WEB de données](images/Semantic_Web_Stack.png)]

.credit[
      Crédits:  [Wikipédia](http://it.wikipedia.org/wiki/File:W3C-Semantic_Web_layerCake.png)

]

---

<!--slide 29-->

### 3.5 Le modèle de description RDF

#### 3.5.1 Définition

> Définition : Le [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework) ou ***Resource Description Framework*** est un langage simple pour exprimer des [modèles de données](https://fr.wikipedia.org/wiki/Modèle_de_données) sous forme d'objets ([« ressources »](https://fr.wikipedia.org/wiki/Ressource_du_World_Wide_Web)) et de leurs relations.

Un document structuré en RDF est un ensemble de triplets.

Un triplet est toujours construit suivant la forme suivante : **sujet → prédicat → objet**

* Le **sujet** représente la **ressource** à décrire ;
* Le **prédicat** représente un **type de propriété** de la **ressource** ;
* L'**objet** représente **la valeur** de la **propriété**.

---

<!--slide 30-->

#### 3.5.2 Le modèle de description RDF : exemple

![Triplet RDF](images/triplet-RDF.png)

---

<!--slide 31-->

#### 3.5.3 URI et URL

> Un **URI**, *Uniform Resource Identifier*, soit littéralement ***identifiant uniforme de ressource***, est une courte chaîne de caractères identifiant une ressource sur un réseau (par exemple une [ressource Web](https://fr.wikipedia.org/wiki/Ressource_Web)) physique ou abstraite, et dont la [syntaxe](https://fr.wikipedia.org/wiki/Syntaxe) respecte une norme d'Internet mise en place pour le Web

> Un URI doit permettre d'identifier une ressource de manière permanente, même si la ressource est déplacée.

L'URI regroupe deux types d'identifiants :

* Les URL : *Uniform Resource Locator*, littéralement « localisateur uniforme de ressource » pour adresser les ressources internet. Une URL identifie en fait l'emplacement d'une ressource, plutôt que la  ressource elle-même ;
* Les URN : *Uniform Resource Name*, littéralement  « nom uniforme de ressource »

> **Un des pilliers du Web de données** : utiliser des adresses URI uniques pour identifier les choses

---

<!--slide 32-->

### 3.5.3 les URI

Les URI (Unique Ressource Identifier) sont les cléfs de voute du Web

Syntaxe :

.remark-code.inline[

scheme:protocole://chaînede.caractère/ressource

http://monsite.com/dossier/fichier.html#ancre

http://mondomaine.org/ressource/1234
]

**Composition** :

* un préfixe qui indique le contexte dans lequel l’identifiant est attribué (par ex. http:,ftp:, urn:, etc.)
* un élément qui permet de désigner l’autorité nommante qui a attribué l’identifiant au sein de ce système
* le « nom » lui-même, c'est-à-dire une chaîne de caractères qui identifie la ressource de manière unique, au sein de ce système et pour cette autorité.

Les Url sont des URI qui identifient la ressource par le moyen d'y accéder

Les Url peuvent être pérennes à condition d'adopter quelques bonnes pratiques.

---

<!--slide 33-->

### 3.5.3 URI : le système de gestion ark

Ark propose un système d'attribution d'identifiants pour garantir leur unicité :

* Attribution centralisée d'un identifiant d'autorité nommante
* Les parties Name et qualifier sont construites sous la forme d’une chaine de caractères aléatoires avec les contraintes suivantes :
* une chaîne de caractères alphanumériques avec une séquence en base 29 :
  * uniquement des consonnes (sauf l) et des chiffres 0 à 9
  * la partie Name commence par un préfixe « rf » fixant le contexte d’attribution des identifiants
  * une longueur fixe de 10 caractères

Exemple :

.remark-code.inline[

identifiant de vocabulaire : ark:/25651/rf5v812szg

identifiant d'un concept dans le vocabulaire : ark:/25651/rf5v812szg/f7q82r9zhs

adresse du concept : https://referentiel.saemgirondin.fr/ark:/25651/rf5v812szg/f7q82r9zhs
]

---

<!--slide 34-->

#### 3.5.4 Le modèle de description RDF : plus précisément

Le sujet est toujours un **URI**.

Toute “chose” sur laquelle on veut faire des assertions (sujet) doit avoir un URI.

Les “choses” ont toujours **un type** : **une CLASSE**.

Le prédicat est toujours un **URI**.

Il permet d'exprimer les **propriétés** des “choses”, ou les **relations** des “choses” entre elles

L‘objet peut être un **texte** (littéral) ou un **URI**.

Les **classes** et les **propriétés** sont déclarées dans des **vocabulaires** pour être réutilisées.

---

<!--slide 35-->

### 3.5.4 Le triplet RDF

![Triplet RDF](images/triplet-RDF2.png)

---

<!--slide 36-->

#### 3.5.5 Le modèle de description RDF : Décrire un concept du thésaurus matière

Exemple :

POLICE est un terme français précisant le terme ADMINISTRATION qui englobe l’ensemble des descripteurs contrôlés utilisés par le [Thésaurus-matières pour l'indexation des archives locales](http://data.culture.fr/thesaurus/resource/ark:/67717/Matiere) pour décrire et l'indexer les archives locales

.remark-code.inline[

POLICE est un terme

POLICE est en français

POLICE précise, spécifie le terme administration

POLICE a pour équivalent le terme POLICE dans le vocabulaire RAMEAU

POLICE fait partie du vocabulaire Thésaurus-matières pour l'indexation des archives locales

Thésaurus-matières pour l'indexation des archives locales est un vocabulaire pour décrire ldécrire et l'indexer les archives locales
]

---
name: ontologie

<!--slide 37-->

### 3.6 Présentation du modèle de structuration par ontologie

1. Présentation du modèle de structuration par ontologie
   1. Qu'est-ce qu'une ontologie ?
   2. Les concepts du Web sémantique
   3. Applications

---

<!--slide 38-->

#### 3.6.1 Définition

> **En philosophie**, c'est une partie de la philosophie qui a pour objet l'étude des propriétés les plus générales de l'être, telles que l'existence, la possibilité, la durée, le devenir. 

> Au sens strict, la métaphysique c'est l'**ontologie** [it. ds le texte], c'est-à-dire l'étude de l'être dans ses propriétés générales et dans ce qu'il peut avoir d'absolu; c'est l'étude de ce que sont les choses en elles-mêmes, dans leur nature intime et profonde, par opposition à la seule considération de leurs apparences ou de leurs attributs séparés.
> L. Meynard,*Métaphysique*, 1959, p.15 ds Foulq.-St-Jean 1962.

.credit[
      Crédits:  [Centre national de ressources textuelles et lexicales](https://www.cnrtl.fr/lexicographie/ontologie)

]

> **En informatique et en science de l'information**, une **ontologie** est l'ensemble structuré des termes et concepts représentant le sens d’un champ d'informations, que ce soit par les métadonnées d'un espace de noms, ou les éléments d'un domaine de connaissances.

---

<!--slide 39-->

#### 3.6 Qu'est-ce qu'une ontologie ?

Les schémas de métadonnées servent à contrôler la saisie et développer la sémantique des informations

On assemble les descripteurs « métiers » dont on a besoin en fonction de ce que l’on veut représenter et des usages, des métadonnées, une méthode de raisonnement, un format d’organisation des connaissances et des systèmes d’organisation des connaissances

![schéma par ontologie](images/owl_ontology.jpg)

---

<!--slide 40-->

#### 3.7 La représentation en graphe

.reduite[![Entité relation et propriétés](images/graphModele.jpg)]

---

<!--slide 41-->

#### 3.8 Du point de vue des usages

Des données structurées et enrichies qui ne doivent pas nous faire perdre de vue les finalités de la Transition bibliographique et notamment le confort de l’usager dans les cinq grandes « tâches  utilisateurs » :

1. **Trouver** : Rechercher tout critère pertinent afin de rassembler des informations sur une ou plusieurs ressources présentant un intérêt ;
2. **Identifier** : Comprendre clairement la nature des ressources trouvées et faire la distinction entre des ressources similaires ;
3. **Sélectionner** : Déterminer l’adaptation de la ressource trouvée et choisir (en acceptant ou rejetant) des ressources spécifiques ;
4. **Obtenir** : Accéder au contenu de la ressource ;
5. **Naviguer** : Utiliser les relations qui existent entre une ressource et une autre pour les situer dans un contexte.

---

<!--slide 42-->

#### 3.9 Les modèles de données : de FRBR à FRBRoo

[Le modèle FRBR (fonctionnalités requises pour les notices bibliographiques)](http://www.ifla.org/publications/functional-requirements-for-bibliographic-records) a été élaboré par la fédération internationale des associations et institutions de bibliothèques, l’[IFLA](http://ifla.org).

![Entité relation et propriétés](images/modelegeneralfrbr2.png)

.credit[
Ph. Le Pape, [Vingt ans après : LRM, un pour tous !](https://rda.abes.fr/tag/frbr/#_ftn4).
]

---

<!--slide 43-->

#### 3.9 Les modèles de données : De FRBR à FRBRoo

![Entité relation et propriétés](images/frbr.png)

.credit [
      Bénézet Joly, [http://slideplayer.fr/slide/3213771/](http://slideplayer.fr/slide/3213771/).
]

---

<!--slide 44-->

#### 3.9 Les modèles de données : FRBRoo

FRBRoo (FRBR « orienté objet »), publié dans sa première version en 2009, est une formalisation de FRBR en ontologie, par le biais d’une harmonisation avec CIDOC CRM, modèle conceptuel créé pour les objets de musée. 

FRBRoo, qui se présente comme « une ontologie ou modèle conceptuel de haut niveau pour les données bibliographiques »,  en est une extension. Son [édition actuelle (version 2.4, 2015)](https://www.ifla.org/files/assets/cataloguing/FRBRoo/frbroo_v_2.4.pdf) intègre les modèles FRAD et FRSAD.

---

<!--slide 45-->

#### 3.9 Les modèles de données : data.bnf

Le modèle de données de [data.bnf.fr](https://data.bnf.fr/images/modele_donnees_2018_02.pdf) expérimente et adapte le modèle FRBR.

[data.bnf.fr](http://data.bnf.fr/) s’inscrit dans les évolutions récentes en matière de description bibliographique.

Ce modèle comprend trois groupes d'entités liées par des  relations : 

* les informations sur les documents
* les personnes physiques ou morales
* les thèmes

---

<!--slide 46-->

#### 3.9 data.bnf: vue d'ensemble

![Modèle data.bnf.fr](images/data-bnf.png)

---

<!--slide 47-->

#### 3.9 data.bnf : la réutilisation des modèles

![les modèles imbriqués](images/data-bnf-onto.png)

---

<!--slide 48-->

#### 3.9 Les modèles de données : Le modèle MDFA

Développé pour les sociétés Anaphore et Sparna pour la conception du navigateur Bach

.reduite[![Le modèle de l'otologie MDFA](images/ontologieMDFA.jpg)]

---

<!--slide 49-->

#### 3.9 Les modèles de données : Le modèle archivesHub

Développé pour le projet [ArchivesHub](https://archiveshub.jisc.ac.uk/)

![Le modèle de l'ontologie archivesHub](images/locahOntology.jpg)

---

<!--slide 50-->

#### 3.9 Les modèles de données : Le modèle SAEM

Développé pour le projet [SAEM Girondin](https://referentiel.saemgirondin.fr)

.reduite[![Le modèle de l'ontologie saem](images/ontologieSAEM.jpg)]

---
name: enjeux

<!--slide 51-->

## 4. Enjeux et prespectives

* Contrôler le vocabulaire de description
* Relier des objets et les aligner
* Proposer différents axes de navigation
* Offrir des vues différentes d’une même information
* Faciliter la pérennisation de l’information
* décrire les objets en garantissant leur traçabilité

---

<!--slide 52-->

### 4.1 Relier des objets et des corpus (fonds)

Appliquer une norme métier :

* EAD / EAC, SEDA bientôt RIC pour les archives,
* MARC, FRBR pour les bibliothèques,
* CIDOC-CRM / LIDO pour les musées

Pour fédérer les pratiques et échanger facilement des informations

Réconcilier des référentiels : rapprocher ou réutiliser des thésaurus existants

**exemples** :

* [Thesaurus Archives](http://data.culture.fr/thesaurus)
* [Rameau](http://data.bnf.fr/liste-rameau)
* [lcsh](http://id.loc.gov/authorities/subjects.html)

---

<!--slide 53-->

### 4.2 Décrire un objet en RDF

Le modèle RDF permet de décloisonner les silos de données.

Les référentiels permettent alors de les relier.

.reduite[![Boris Vian](images/borisVian.jpg)]

---

<!--slide 54-->

[Explorer la galaxie depuis Boris Vian](http://en.lodlive.it/?http://dbpedia.org/resource/Boris_Vian)

* L’Écume des jours est un roman de Boris Vian publié en 1947 avec pour thèmes centraux l’amour, la maladie, la mort
* L’Écume des jours est un roman
* L’Écume des jours a pour auteur Boris Vian
* L’Écume des jours est paru en 1947
* L’Écume des jours a pour thèmes l’amour, la maladie, la mort
* J’irais cracher sur vos tombes a pour auteur Vernon Sullivan

.remark-code.inline[
http://dbpedia.org/page/Froth_on_the_Daydream 

http://data.bnf.fr/13091689/boris_vian/ 

dbpedia:Froth_on_the_Daydream rdf:type yago:Novel106367879

dbpedia:Froth_on_the_Daydream dcterms:creator dbpedia:Boris_Vian

dbpedia:Froth_on_the_Daydream dcterms:date "1947"

dbpedia:Froth_on_the_Daydream skos:subject dbpedia:Love

dbpedia-owl:wikiPageRedirects dcterms:creator dbpedia:Vernon_Sullivan
]

---

<!--slide 55-->

### 4. 3 Contrôler le vocabulaire de description

Permet de proposer des rebonds vers d'autres contenus

.pull-left[.reduite[![Notice Charles Darwin](images/charlesDarwin.jpg)]]
.pull-right[.reduite[![Rebond Charles Darwin](images/charlesDarwinRebond.jpg)]]

---

<!--slide 56-->

#### Proposer différentes modalité d'exploration

![navigateur Bach](images/bach.jpg)

---

<!--slide 57-->

#### 4.4 Relier des objets et les aligner : wikidata

Le référentiel Wikidata est constitué principalement d'éléments, chacun ayant un libellé, une description et un nombre quelconque d'alias. Les éléments sont identifiés de manière unique par un Q suivi d'un nombre, comme Douglas Adams (Q42).

Les déclarations décrivent les caractéristiques détaillées d'un élément et consistent en une propriété et une valeur. Les propriétés dans Wikidata ont un P suivi d'un nombre, comme avec scolarité (P69). 

.reduite[![le modèle wikidata](images/1280px-Datamodel_in_Wikidata_fr.svg.png)]

---

<!--slide 58-->

#### 4.4 construire des outils d'exploration (à partir de Wikidata)

* [Moteur de recherche et d'affichage d'œuvres d'art basé sur Wikidata](http://zone47.com/crotos/?l=fr&p=&nb=20&disp=1&s=paris&y1=-40000&y2=2016&p31=125191)
* [les emplacements des tombes du père Lachaise](http://graves.wiki/#/map/@48.86122,2.39393,16z)
* [générer des frises chronologique](http://histropedia.com/timeline/hrfrtpg9bg0t/History-of-anarchism)

![[vizdata](https://sylum.lima-city.de/viziData/#d=0&m=humans&l=en&f=1&e=1700,2015&g=1&h=1.2&o=1&p=3&x=0&y=0&z=2)](images/vizdata.JPG)

---

<!-- Slide 59 -->

### Wikidata : Ici on peut bifurquer vers la galaxie wikidata

https://hackmd.logilab.fr/AFdoVQfsQnOAAvB-CfBMjA#

---

<!--slide 59-->

#### 4.5 Décrire les objets et garantir leur traçabilité

Avec le modèle de données PROV, on cherche à modéliser 3 types d'objets :

* Agents : qui a créé, qui a modifié, qui a contribué, qui a transformé, etc..
* Activités : création, modification, contribution, suppression, archivage, etc…
* Entités : fichier, dossier, collection manipulés par des agents au travers d'activités

.reduite[
      ![le modèle prov-o](images/prov-o.png>
      ]

---

<!--slide 60-->

#### 4.5 Le modèle Prov-O

Dans le modèle PROV, une entité est une ressource dont on veut décrire la provenance.

Les activités sont **les processus** qui ont utilisé ou généré des entités, comme par exemple : calculer un résultat, 
écrire un livre, faire une présentation. Les activités ne sont pas des entités.

Les agents sont **responsables des activités** affectant les entités.

Un agent est quelque chose qui porte une forme de responsabilité dans le déroulement d'une activité, dans l’existence 
d’une entité ou dans l’activité d’un autre agent.

---

<!--slide 61-->

#### 4.5 Le modèle Prov-o étendu

![le modèle étendu](images/prov-o-extended.png)

---

<!--slide 62-->

#### 4.5 Premis : conserver les données numériques

L'ontologie Premis [v3 2018](http://www.loc.gov/standards/premis/ontology/owl-version3.html) est publiée par la librairie du Congrès et reprend de nombreuses propriétés de Prov pour **décrire les objets numériques en lien avec les agents qui les manipulent et les évènements qui les affectent**.

2 des principales entités sont des sous-classes d'entités **provenant d'autres modèles de données** :

* agent : sous-classe de foaf:agent et prov:agent
* event : sous-classe de prov:activity

L'entité object a pour sous-classe premis:file, premis:intellectualEntity, premis:reprsentation, premis:bitSteam

L'entité Rights est propre à l'ontologie et définit les droits associés aux agents et aux objets

---

<!--slide 63-->

#### 4.5 Premis: le modèle de données

![premis model](images/Premis.jpg)

---
name: skos

<!--slide 64-->

### 5. Présentation de SKOS

[SKOS](https://lov.linkeddata.es/dataset/lov/vocabs/skos) est construit sur la base du langage RDF, et son principal objectif est de permettre la **publication facile de vocabulaires structurés** pour leur utilisation dans le cadre du Web de données. C'est une recommandation du W3C publiée le 18 août 2009.

Les propriétés de mise en correspondance proposées dans SKOS permettent d'**exprimer des correspondances entre concepts** provenant de schémas différents

* skos:exactMatch ou skos:closeMatch = **équivalence et proximité**
* skos:broader, skos:narrower, skos:related = **hiérarchie et relations**
* skos:semanticRelation = **association** sémantique

![lov](images/lov.jpg)

---

<!--slide 65-->

#### 5.1 Les systèmes d'organisation de la connaissance

Par domaine :

* bibliothèque : [LCSH](http://id.loc.gov/authorities/sh85145447#concept), [Rameau](http://stitch.cs.vu.nl/vocabularies/rameau/ark:/12148/cb11931913j)
* agriculture : [agrovoc](http://aims.fao.org/aos/agrovoc/c_8309)
* administration : [eurovoc](https://publications.europa.eu/en/web/eu-vocabularies/th-dataset/-/resource/dataset/eurovoc)
* geographie : [tgn](http://www.getty.edu/research/tools/vocabularies/tgn/index.html)

Généralistes : 

* lexical : [wordnet](http://wordnetweb.princeton.edu/perl/webwn)
* [géographie administrative française](http://data.ign.fr/def/geofla/20190212.htm), [pays](http://eulersharp.sourceforge.net/2003/03swap/countries#)
* [code de langue](http://eulersharp.sourceforge.net/2003/03swap/languages#)

* [Liste de thésaurii skos](https://www.w3.org/2001/sw/wiki/SKOS/Datasets)

---

<!--slide 66-->

#### 5. 2 Les types de données du SKOS

* **Concepts**: concept, conceptScheme (ex:thesaurus), inScheme, hasTopConcept, topConceptOf 
* **propriétés lexicales** : lang
* **relations** : broader, narrower, related, broaderTransitive, narrowerTransitive, semanticRelation
* **Labels et notations** : prefLabel, altLabel, hiddenLabel, notation (typage)
* **documentation** : note, changeNote, definition, editorialNote, example, historyNote, scopeNote
* **collections** : collection, orderedCollection, member, menberList

---

<!--slide 67-->

#### 5. 2 Tout est concept

Un **concept SKOS** est défini comme une ressource RDF, donc identifiée par une URI.

![skos Concept](images/skosConcept.jpg)

.credit[Un concept ne peut avoir qu'un seul label préféré par langue]

---

<!--slide 68-->

#### 5. 2 avec des labels

Le terme préférentiel identifie le concept de façon unique dans le cadre d'un schéma (et dans une langue donnée)

![skos label](images/labelSkos.jpg)

---

<!--slide 69-->

#### 5. 2 et des notes

Les notes permettent de décrire le contenu d'un concept et son évolution

![skos documentation](images/documentationSkos.jpg)

---

<!--slide 70-->

#### 5. 2 et des relations

Les relations permettent de construire des collections, ordonnées ou nom, assemblées dans un vocabulaire de manière hiérarchique ou horizontale

![skos relation](images/relationSkos.jpg)

---

<!--slide 71-->

#### 5. 2 exemple de Concept

Rhizome [concept](http://aims.fao.org/aos/agrovoc/c_6565.html) [agrovoc](http://aims.fao.org/standards/agrovoc/functionalities/search)

![le concept de Rhizome dans le vocabulaire Agrovoc](images/conceptSkosRhizome.jpg)

---

<!--slide 72-->

#### 5. 2 En rdf/xml

.remark-code.inline[

skos:Concept rdf:about="http://aims.fao.org/aos/agrovoc/c_7430"

skos:prefLabel xml:lang="fr" Organe de réserve

skos:exactMatch rdf:resource="http://aims.fao.org/aos/agrovoc/c_6565"/

skos:Concept rdf:about="http://aims.fao.org/aos/agrovoc/c_6565"

skos:prefLabel xml:lang="fr" Rhizome

skos:prefLabel xml:lang="ja" 根茎

skos:broader

skos:Concept rdf:about="http://aims.fao.org/aos/agrovoc/c_7390"

skos:prefLabel xml:lang="fr" Tige

skos:related

skos:Concept rdf:about="http://aims.fao.org/aos/agrovoc/c_1897"

skos:prefLabel xml:lang="fr" Cormus
]

---

<!--slide 73-->

#### 5.3 SKOS et les archives : Le Thésaurus-matières pour l’indexation des archives locales.

Révision du thésaurus en 2009 ==> structuration SKOS ==> publication depuis 2011, sur [site Web](http://data.culture.fr/thesaurus/)

* naviguer au sein du thésaurus,
* télécharger une version RDF/XML, 
* interroger via le langage SPARQL

---

<!--slide 74-->

#### 5.3 Le thésaurus matière en version RDF/XML

![vue xml du thesaurus](images/Skos-thesaurus.png)

---

<!--slide 75-->

### 5.4 utilisation de SKOS

![les concepts skos dans le modèle data.bnf](images/databnf-skos.png)

---

### 6. Record in Context

A toi de jouer :) 

---


.reduite[![EGAD](images/EGAD.png)]

*L’EGAD a eu pour mission d’élaborer un « modèle conceptuel » pour la description archivistique, afin de traiter l’interrelation des composants de la description dans un système de description archivistique et de permettre la mise en relation de la description archivistique avec les normes de description associées du patrimoine culturel.*

EGAD a publié une première version du modèle conceptuel (RiC-CM). Il finalise actuellement une version 2, qui complète, affine et réorganise la première version suite à l'appel à commentaires (2017)

---

#### 6.1 Le modèle conceptuel *Record in Context* (RIC-CM)

Intégration des standards de description archivistique : ISAD(G), ISAAR(CPF), ISDF et ISDIAH
.reduite[![La fusion des normes de description archivistique](images/ricOrigin.jpg)]

---

#### 6.2 RIC-CM : Une description élémentaire pour une représentation en graphe

L'idée générale de RiC est de concevoir la **description archivistique comme l’agencement d’éléments descriptifs** fondamentaux réutilisables à loisir. Le modèle cherche donc à définir les éléments fondamentaux de la description archivistique à la plus petite unité possible, à donner les caractéristiques de ces éléments et à **faire des liens** entre eux.

.reduite[![La description élémentaire du modèle RIC](images/RIC1.png)]

---

#### 6.3 RIC-CM : 14 entités fondamentales

Les entités de ce modèle s’articulent autour de la notion de record.

.reduite[![Les entités du modèle RIC](images/RIC-entites.png)]

---

#### 6.4 RIC-CM : Les propriétés des entités

**67 propriétés** qui définissent les entités

* des propriétés communes à toutes les entités
* des propriétés récurrentes
* des propriétés particulières

---

#### 6.4 RIC-CM : Les propriétés des entités

![Les propriétés du modèle RIC](images/ric-cm-4.png)

---

#### 6.5 RIC-CM : Les relations entre les entités

**792 relations** possibles entre les entités

.reduite[![Les relations du modèle RIC](images/ric-CM-3.png)]

---

#### 6.6 Le modèle de données : RIC-O

RIC-O est une **ontologie formelle**, exprimée dans les langages de modélisation utilisés pour le Web de données (RDF, RDFS et OWL) et transposant le modèle conceptuel RIC-CM, afin que ses utilisateurs puissent disposer d’un vocabulaire de référence pour **pousser les métadonnées archivistiques dans l’univers du web de données**.

.reduite[![lLe modèle](images/ricModele.jpg)]

---

#### 6.7 Le modèle de données : RIC-O

Les principes d’élaboration de RiC-O :

*conforme à RiC-CM
flexible
utilisable
fonctionnelle
extensible

.credit[Sources : *Records in Contexts – Ontology (RiC-O) :  objectifs, principes d’élaboration et feuille de route*, Florence Clavaud, 2019 : [lien externe](https://www.archivistes.org/IMG/pdf/archivistes_129_avril-juin.pdf)]

RiC-O : [Extension de l'Appel à commentaires](https://www.ica.org/fr/ric-o-extension-de-l-appel-a-commentateurs)

---
name: outils

### 7. Quelques applications

---

#### 7.1 Visualisation de métadonnées archivistiques

Mise en ligne du prototype français PIAAF

« **Pilote d’interopérabilité pour les autorités archivistiques françaises** » (PIAAF) :

Objetif : constituer une première expérimentation dans l’application des technologies du web sémantique à la description archivistique.

Un partenariat : SIAF, Archives nationales, BNF et le CRIHN

Un socle logiciel : CubicWeb de Logilab

Disponible en ligne : http://piaaf.demo.logilab.fr/

---

#### 7.1 Le prototype en ligne

.reduite[![PIAAF](images/PIAAF.png)]

---

#### 7.2 Au-delà des tranchées

Un [projet](http://www.canadiana.ca/rpcpd-dol) des données ouvertes liées autour de la Première Guerre mondiale

![Canadiana](images/canadiana.png)

---

#### 7.3 Linked Jazz

![(https://linkedjazz.org/network/)](images/Linked-jazz.png)

site web : [Linked Jazz](https://linkedjazz.org/network/)

---

#### 7.4 : The New-York Public Library Archives & Manuscrits 

![(http://archives.nypl.org/terms/)](images/NYPL-1.png)

![The New-York Public Library](images/NYPL-2.png)

---

#### 7.5 Outillage skos

* [produire et visualiser des vocabulaires skos](http://labs.sparna.fr/skos-play/)
* [produire des données archivistiques liées](https://demo.logilab.fr/saem-demo/)

.reduite[![visualiser le thesaurus W](images/thesaurusWSkoPay.jpg)]

---

#### 7.6 outillage alignement de données

* [structurer des données ouvertes](https://www.wikidata.org/wiki/Wikidata:Main_Page)
* [géocoder des données](http://adresse.data.gouv.fr/)

.reduite[![les outils de data.gouv](images/adresse.gouv.jpg)]

---

#### 7.7 outillage rdf/owl

* [produire des ontologies ou des instances rdf](https://protege.stanford.edu/)
* [aligner des données sur des référentiels](http://openrefine.org/)

.reduite[![aligner wikidata dans openrefine](images/Wikidata_reconciliation_in_OpenRefine_screenshot.png)]

---

#### 7.8 outillage archives : ATOM3

![ATOM](images/atom.png)

AtoM est une contraction de l’appellation "***[Access to Memory](https://www.accesstomemory.org/fr/)***" (accès à la mémoire).

C'est une application internet open source pour la description archivistique et l'accès, qui se repose sur les normes et qui est offert  selon un environnement multilingue et à multiples dépôts.

[AtoM 3: Étude de faisabilité](https://atomfoundation.files.wordpress.com/2019/02/atom-3-proposition-detude-de-faisabilite_final_fr.pdf) – Proposition  en cours concernant l’établissement d’une étude de faisabilité pour une application logicielle qui succéderait à Access to Memory 2.

Un sondage auprès des utilisateurs est ouvert jusqu'au 31 mai 2019.

---
name: exercices

### 8. Exercices

1. Produire un vocabulaire skos avec Skosplay et cubicweb
2. Contribuer à wikidata et interroger le point d'accès SPARQL

---

### 8. Exercices : Produire un vocabulaire et l'exporter en SKOS

---

#### 8. Interroger wikidata

https://hackmd.logilab.fr/VQ6mtSJlRO2TM8fjE5TYaA#

---
#### 8. Exercices : Contribuer à wikidata

https://hackmd.logilab.fr/ROeRPaMfT9O8jsG2aSEa-w#

---
### 8. Exercices : installer et utiliser openRefine

---
### 8. Exercices

---
### 9. Ressources

![les outils](images/outillage.jpg)

---

#### 9.1 (In)Formations complémentaires

- [MOOC Inria Web sémantique](https://www.fun-mooc.fr/courses/inria/41002S02/session02/about)
- [éditeurs d'ontologies](https://www.w3.org/wiki/Ontology_editors)
- [Bonnes pratiques pour publier un vocabulaire rdf](https://www.w3.org/TR/swbp-vocab-pub/)
- [tutorial Protégé Ontologie](https://protegewiki.stanford.edu/wiki/Ontology101)
- [liste d'outils pour ontologies](http://wiki.opensemanticframework.org/index.php/Ontology_Tools)

**Ressources OpenRefine** :

* [Tutoriels complets + ressources web](https://www.patrimoine-et-numerique.fr/tutoriels/52-36-openrefine-excel-aux-hormones-pour-nettoyage-de-donnees)
* [Atelier OpenRefine (Forum AAF 2019)](https://framaslides.org/share/5ca31af2259478.66971765)
* [OpenRefine et Wikidata (Journées Wikimédia 23 mai 2019)](https://framaslides.org/share/5ce5636c02d268.91852533)
* [Pensum](data/Webdonnees.pdf)

---

#### 9.2 Préparer les métadonnées archivistiques

**Indexer** :

* [L'EAD2](https://francearchives.fr/file/0def64f5a10f3f1ae03fdea59399a3e0755ef157/static_1066.pdf) permet d'encoder les identifiants d'autres ressources présentes (terme d'indexation et point d'accès) sur le WEB
* On peut utiliser l’attribut AUTHFILENUMBER pour identifier un lien vers une notice d'un fichier d’autorité contenant plus d’informations sur une vedette et des renvois.

Il faut alors associer cet attribut avec l'attribut Source du vocabulaire.

.remark-code.inline[
corpname normal="Institut de France. Bibliothèque" source="BNF" authfilenumber="frBN008156065" Bibliothèque de l'Institut de France

persname source="BNF" authfilenumber="frBN000488806" Blanche, JacquesÉmile
]

> Authfilenumber est un attribut rendu obsolète par l'EAD3.

---

### Encoder des concepts en EAD

[L'EAD 3](https://www.loc.gov/ead/EAD3taglib/EAD3.html) permet d'endoder des URI vers des termes d'indexation ou des ressources exposés sur le WEB

* @identifier est un nombre, code, ou chaîne de caractère (ex: URI) qui identifie de manière unique le terme utilisé dans un vocabulaire contrôlé ou une liste d'autorité.

**Examples** :

remark-code.inline[
controlaccess

corpname encodinganalog="610" identifier="http:// viaf.org/viaf/139169065" lang="eng" 

part Hudson's Bay Company

corpname encodinganalog="610" identifier="http:// viaf.org/viaf/139169065" lang="fre"

Compagnie de la Baie d'Hudson

]

---

#### 9.3 Préparer les métadonnées archivistiques

**Faire des notices d'autorité :**

**Les outils**

* tableur CSV : libre office [calc](https://fr.libreoffice.org/discover/calc/), excel, [framacalc](https://accueil.framacalc.org/fr/)
* [ATOM (logiciel libre)](https://www.accesstomemory.org/fr/)
* [Module Référentiel du SAEM Girondin (libre)](http://hg.logilab.org/master/cubes/saem_ref)

Pour les AD : Récupérer les notices élaborées par le groupe de travail [SIAF/AAF](https://aaf.ica-atom.org/)

---

#### 9.4 Bibiographie

* *Qui sont les publics des Archives ? : enquêtes sur les lecteurs, les internautes et le public des activités culturelles dans les services publics d’archives ( 2013-2014)*, SIAF, 2015 : [lien externe](https://francearchives.fr/file/08ccbaa3654282501138a7739ac59dbecc364552/static_8431.pdf)
* Métadonnées culturelles et transition web 3.0 : [lien externe](http://www.culture.gouv.fr/var/culture/storage/pub/feuille_de_route__metadonnees_culturelles_et_transition_web_3_0_janvier_2014/index.htm)
* Transition bibliographique : [lien externe](https://www.transition-bibliographique.fr/se-former/supports/)
* Le projet SNAC : Social Networks and Archival Context https://snaccooperative.org/view/83488235 
* Records in Contexts – Ontology (RiC-O) :  objectifs, principes d’élaboration et feuille de route : [lien externe](https://www.archivistes.org/IMG/pdf/archivistes_129_avril-juin.pdf)
* Source Flickr : [lien externe](https://www.flickr.com/photos/safari_vacation/6315269494/) (CC BY-SA)
* (https://news.netcraft.com/archives/2019/)
* [Le modèle conceptuel Record in Context](https://www.ica.org/sites/default/files/RiC-CM-0.1.pdf) 
* *Records in Contexts – Ontology (RiC-O) :  objectifs, principes d’élaboration et feuille de route*, Florence Clavaud, 2019
* Présentation de PIAAF par Florence Claveau : [lien externe](http://semweb.pro/semwebpro-2017.html#p11)