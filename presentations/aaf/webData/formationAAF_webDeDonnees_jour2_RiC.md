
class: center, top
background-image: url(images/RiC-O/fondEcran-graphe.png)
background-position: center;
background-repeat: no repeat;
background-size: contain;

<!-- .credit[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Florence Clavaud (Archives nationales, Mission référentiels | Conseil international des Archives, groupe d’experts EGAD) pour le compte de l’Association des Archivistes Français.]-->

# Formation Webdata AAF : jour 2<br>Records in Contexts-Ontology (RiC-O)

Support de formation distribué sous licence Creative Commons [CC-By-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).  

Préparé par Florence Clavaud <br>(Archives nationales, Mission référentiels | Conseil international des Archives, groupe d’experts EGAD ;<br> courriel : [florence.clavaud@culture.gouv.fr](mailto:florence.clavaud@culture.gouv.fr))<br> pour le compte de l’Association des Archivistes Français.

Date : 22 septembre 2020 (document revu le 8 octobre 2020)
---
name:programmeJ2
<!--slide 2-->

## Jour 2 : programme

 
   1. [Records in Contexts-Ontology (RiC-O) : informations générales](#overview)  
   2. [RiC-O : présentation détaillée](#features)  
   3. [Le cas d’usage des Archives nationales](#anf)  
   4. [Exercices : produire et utiliser des données RDF/RiC-O](#exercices) 
   5. [Conclusion](#conclusion) 
   
 
---
layout: true
class: left, top


---

<!--slide 3-->

## Vos attentes 

D’après vos fiches de présentation :

* Connaître l’essentiel du standard RiC et des raisons et enjeux de l’évolution des pratiques de la description archivistique
* Avoir une vision précise de RiC-O
* Disposer de repères pour réfléchir à une stratégie de mise en œuvre de RiC-O (notamment, comment passer d’EAD à RDF)

À discuter ensemble.

Noter cependant que ce stage est juste une courte formation introductive.

---
layout: true
class: left, top
background-image: url(images/RiC-O/ICA_Logo_ExpertGroups_EGAD_small.png)
background-size:150px
background-position: right top
background-repeat: no-repeat

.footnote[
      [Programme](#programmeJ2)]

---
name: overview
<!-- slide 4 -->
## 1. Records in Contexts-Ontology (RiC-O) : informations générales


.reduite[![un graphe](images/RiC-O/fondEcran-graphe.png)]

---
<!-- slide 5 -->
### RiC-0, qu’est-ce que c’est ?
* une **ontologie OWL générique pour la description des archives**
* **transposition formelle du modèle conceptuel Records in Contexts-Conceptual Model (RiC-CM)**
* **2e partie du standard international RiC**
* RiC est développé par [le groupe d’experts EGAD du CIA](https://www.ica.org/en/egad-steering-committee-0)<br>Un groupe d’experts formé en 2013, dont le mandat a été renouvelé depuis, jusqu’à 2021 pour l’instant.<br>Le travail du groupe EGAD est régulièrement soumis à commentaires pour faire participer les communautés professionnelles concernées.<br>Il devra être poursuivi en continu après 2021.

---
<!-- slide 6-->
### Records in Contexts : pourquoi ?
* Pour **remplacer les quatre standards internationaux existants**
* Pour passer de principes et règles de description à **un unique standard fondé sur une approche entités-relations**
* Pour **mieux représenter les archives, leur histoire et les multiples couches de _contextes_ dans lesquelles elles s’inscrivent**
* Permettre aux archivistes et aux records managers de passer à une **description plus précise, plus nuancée, plus facile à exploiter, multidimensionnelle**
* Et de ce fait, **faciliter l’accès aux métadonnées archivistiques donc aux ressources documentaires qu’elles décrivent**
---
<!-- slide 7-->
#### Records in Contexts : voir le monde des archives comme un graphe orienté
Exemple : une vue très partielle, conforme à RiC-O v0.1, du graphe formé par les archives de J.-N. Jeanneney, président de Radio France, produite à partir de l’instrument de recherche en EAD consultable à : [https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_050629](https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_050629)

.reduite[![les archives de Jean-Noël Jeanneney, président de Radio France](images/RiC-O/IR_jeanneney_10022020.jpg)]

---
<!-- slide 8 -->
### Records in Contexts (RiC), un standard en trois parties
* Un **modèle conceptuel abstrait**, indépendant de toute implémentation (RiC-Conceptual Model ou RiC-CM)
* Une **ontologie OWL**, transposition de RiC-CM, pour définir le vocabulaire et les règles applicables aux métadonnées archivistiques conformes à RiC et ayant la forme de jeux de données RDF
* Un **manuel de mise en œuvre** (RiC-Application Guidelines, RiC-AG)
---
<!-- slide 9 -->
### Historique et feuille de route globale de RiC
* Août 2016 : **publication de RiC-CM v0.1** avec un appel à commentaires<br>Entre août 2016 et janvier 2017, EGAD a reçu de très nombreux commentaires, et les a pris en compte depuis
* Février-novembre 2019 : plusieurs versions beta de RiC-O successivement envoyées aux personnes qui avaient répondu à un appel à commentateurs
* 12 décembre 2019 : **première publication officielle de [RiC-O](https://www.ica.org/standards/RiC/ontology) (v0.1)** assortie d’un appel à commentaires ; **publication d’une [prévisualisation de RiC-CM v0.2](https://www.ica.org/sites/default/files/ric-cm-0.2_preview.pdf) qui remplace RiC-CM v0.1** devenu obsolète
* Mars 2020 : **ouverture de l’entrepôt de sources de RiC-O sur GitHub**

À venir :
* **Novembre 2020** : publication du modèle conceptuel **RiC-CM v0.2 complet**, et de **RiC-O v0.2** ; appel à commentaires
* Octobre 2021 : **publication de RiC-CM et RiC-O v1.0**

Toutes les versions de RiC antérieures à la v1.0 sont par définition des _drafts_ (projets). 
Cependant RiC-CM et RiC-O v0.2 sont proches de cette version 1.0.
---
<!-- slide 10-->
### Comment se tenir au courant des avancées de RiC ?

Les principales actualités sur RiC sont publiées sur le **site web du CIA**.<br>Voir la [dernière actualité en date](https://www.ica.org/fr/nouvelles-de-la-norme-records-in-contexts). 

* Sur RiC-CM : surveiller le site web du CIA et les listes professionnelles françaises de discussion des archivistes (AAF, SIAF)
* Sur RiC-O :
    * Consulter régulièrement le [site web officiel de RiC-O](https://ica-egad.github.io/RiC-O/), qui fournit également des informations sur les projets, événements et outils
    * Si on souhaite récupérer le dernier état de RiC-O, des exemples, participer aux discussions et travaux en cours : consulter [l’entrepôt de sources sur GitHub](https://github.com/ICA-EGAD/RiC-O)
    * Il est possible de s’abonner à une liste des utilisateurs de RiC-O
* **Ne pas hésiter à me contacter** par courriel ([florence.clavaud@culture.gouv.fr](mailto:florence.clavaud@culture.gouv.fr)), pour toute question, remarque, demande de conseil ou de présentation

---
name: features
<!-- slide 11 -->
## 2. Records in Contexts-Ontology (RiC-O) : présentation détaillée



.reduite[![un graphe](images/RiC-O/fondEcran-graphe.png)]

---
<!-- slide 12 -->
### 2.1. Pour commencer, le mieux est de connaître un peu RiC-CM !

Rappel : la version _0.2 preview_ de RiC-CM est téléchargeable depuis le site web du CIA : [https://www.ica.org/sites/default/files/ric-cm-0.2_preview.pdf](https://www.ica.org/sites/default/files/ric-cm-0.2_preview.pdf)

La version 0.2 complète en sera assez proche ; cependant :
* elle inclura une introduction et des annexes
* **certains changements ont été faits dans les définitions des composants ou leurs noms**

La présentation tient compte de ces évolutions.

---
<!-- slide 13 -->
#### RiC-CM définit et organise 22 entités

Quatre entités essentielles, au cœur du modèle : Record Resource, Instantiation, Agent, Activity

.reduite[![Les entités dans RiC-CM](images/RiC-O/RiC-CM-entites.png)]


---
<!-- slide 14 -->
#### Les entités RiC-CM ont des attributs

Il y a 41 attributs :

* Des attributs communs à toutes les entités : identifier, name, descriptive note
* Des attributs pour chacune des entités de haut niveau (les sous-entités « héritent » de ces attributs)
* Des attributs propres à certaines entités

Certains attributs sont répétables ou extensibles.

.reduite[![Le modèle utilisé pour la spécification des attributs](images/RiC-O/attributes-template.png)]

---
<!-- slide 15 -->
#### Exemple : les attributs des entités RiC-E04 Record et de RiC-E06 Instantiation

.reduite[![Les attributs de Record et de Instantiation](images/RiC-O/attributs-Record-Instantiation.jpg)]



---
<!-- slide 16 -->
#### Les entités RiC-CM sont reliées entre elles par des relations

Dans RiC-CM v0.2, il y a actuellement **77 relations** (et les relations inverses), organisées en **système poly-hiérarchique**. La relation de haut niveau est ‘is related to’.

Elles ont des attributs (identifier, description, date, certainty, source).

.pull-left[![Les grandes catégories de relations](images/RiC-O/relations-categories.jpg)]
.pull-right[![Un extrait du tableau hiérarchique des relations](images/RiC-O/relations-chart.jpg)]

---
<!-- slide 17 -->
#### Les principales entités RiC-CM et quelques-unes des relations existantes

.reduite[![Un diagramme général très simplifié](images/RiC-O/diagram_RiC-CM-overview-v2.jpg)]


---
<!-- slide 18 -->
### 2.2. Et maintenant, RiC-O

Principes d’élaboration : disposer d’un modèle OWL qui soit :
* une transposition formelle de RiC-CM
* une **ontologie de référence**, de domaine
* **immédiatement utilisable**
* flexible
* **utile**
* extensible

Voir aussi :
* [RiC-O design principles](https://www.ica.org/standards/RiC/ontology.html#design-principles)
* [Why use RiC-O?](https://ica-egad.github.io/RiC-O/why-use-RiC-O.html)

---

<!-- slide 19 -->

#### Accéder à RiC-O (pour les humains)

La version officielle de RiC-O (donc, en l’occurrence, RiC-O v0.1) est accessible via son URI: [https://www.ica.org/standards/RiC/ontology](https://www.ica.org/standards/RiC/ontology).

.pull-left[.reduite2[![La vue HTML de RiC-O](images/RiC-O/RiC-O-html.jpg)]]

.pull-right[Cet URI permet d’atteindre une représentation HTML de l’ontologie, qui donne à lire toute sa documentation interne, actuellement uniquement en anglais.

L’entrepôt de sources de RiC-O permet en outre de télécharger :
* la [dernière version en date de RiC-O](https://github.com/ICA-EGAD/RiC-O/tree/master/ontology/current-version) (attention ! elle va évoluer fortement d'ici à la publication de RiC-O v0.2)
* Des [exemples](https://github.com/ICA-EGAD/RiC-O/tree/master/examples)
* Des [diagrammes](https://github.com/ICA-EGAD/RiC-O/tree/master/diagrams)

]


---
<!-- slide 20-->

#### Le fichier source de la version officielle de RiC-O

Pour le télécharger directement : [https://www.ica.org/standards/RiC/RiC-O_v0-1.rdf](https://www.ica.org/standards/RiC/RiC-O_v0-1.rdf)

.reduite3[![le fichier OWL de RiC-O](images/RiC-O/RiC-O-owl.jpg)]


C’est un fichier XML conforme à [OWL 2](https://www.w3.org/2001/sw/wiki/OWL) (qui peut bien sûr être converti aisément dans d’autres formats de sérialisation de RDF, comme [Turtle](http://www.w3.org/TR/turtle/)). Utiliser au moins un éditeur XML pour l’ouvrir et le manipuler, ou, mieux, un **éditeur d’ontologies** comme [Protégé](https://protege.stanford.edu/).

---
<!-- slide 21 -->

#### Il y a plus de composants dans RiC-O que dans RiC-CM

Comme dans de nombreuses représentations formelles, techniques, d’un modèle, RiC-O est plus précise et plus riche que RiC-CM.

* Les entités RiC-CM y sont représentées par des classes
* D’autres classes ont été ajoutées pour pouvoir donner une définition plus précise d’une entité. Ainsi l’entité RiC-CM Place est représentée dans RiC-O par les classes Place, Physical Location et Coordinates
* La plupart des attributs dont le type de contenu est du texte libre sont des ’datatype properties’ dans RiC-O (elles ont pour objet du texte). Par ex. _rico:history_ ou _rico:scopeAndContent_
* Par ailleurs, certains attributs sont aussi représentés par des classes, lorsqu’on a besoin de leur assigner des propriétés donc de les considérer comme des objets à part entière. C’est le cas pour les attributs _type_ qui ont des valeurs contrôlées dans RiC-CM. C’est aussi le cas pour les attributs Language, Name et Identifier de RiC-CM.
* Dans certains cas, pour assurer une certaine flexibilité au modèle et permettre de l’utiliser immédiatement, RiC-O propose plusieurs solutions de modélisation. C’est le cas pour les relations RiC-CM, qui sont représentées par des propriétés binaires dans RiC-O, mais aussi par un système de classes.

Des explications plus précises figurent ici : [https://www.ica.org/standards/RiC/ontology.html#fromRiCCM-to-RiCO](https://www.ica.org/standards/RiC/ontology.html#fromRiCCM-to-RiCO).

On va détailler tout cela par l’exemple.

---
<!-- slide 22 -->
#### Record resources et instantiations : l’exemple d’un acte dans un cartulaire

.reduite[![un acte médiéval dans un cartulaire](images/RiC-O/a-medieval-act-in-a-cartulary.jpg)]

---
<!-- slide 23 -->
#### Record resources et instantiations : l’exemple d’une série de photographies dans un album

.reduite[![une série de photographies dans un album](images/RiC-O/a-series-of-albums-of-aerial-photographs.jpg)]

---
<!-- slide 24 -->
#### Record resources et instantiations : l’exemple d’un tirage dans la série de photographies

.reduite[![une série de photographies dans un album](images/RiC-O/a-print-in-one-of-the-albums.jpg)]


---
<!-- slide 25 -->

#### RiC-CM et RiC-O, des modèles flexibles

Il s’agit notamment de rendre RiC-O **immédiatement utilisable**.
Par exemple : 

* pour consigner **l’histoire d’une entité** (comme un Record), RiC-CM propose soit l’attribut RiC-A21 History, soit la relation RiC-R059i (_is or was affected by_)  et une instance de l’entité Event. Dans RiC-O, c’est pareil, on a le choix.
* pour spécifier que **telle entité (comme une activité) est régulée par un texte réglementaire officiel**, RiC-CM propose la relation RiC-R063i (_is or was regulated by_) et une instance de l’entité Rule. Dans RiC-O, c’est pareil ; mais on trouve aussi la propriété _rico:ruleFollowed_ qu’on peut utiliser pour stocker du texte.
* pour consigner **l’appartenance d’une entité à une catégorie**, RiC-CM propose des attributs, comme, pour les documents, RiC-A17 Documentary Form Type. Dans RiC-O, on peut faire le même choix et se contenter de stocker du texte comme objet de la propriété _rico:type_. Mais on peut aussi choisir d’utiliser _rico:belongsToCategory_ + une instance de la classe rico:Type (donc, ici, un  concept qui  serait une instance de rico:DocumentaryFormType).
* Si on souhaite indiquer que **telle personne a dirigé tel groupe**, on trouvera dans RiC-CM la relation RiC-R042 _is or was leader of_. La même possibilité existe dans RiC-O, qui propose également de représenter cette relation sous la forme d’un graphe plus complexe, impliquant une instance de la classe rico:LeadershipRelation

---
<!-- slide 26 -->
#### Les ’types’ dans RiC-O (1)

**Dans RiC-CM, des attributs** (comme Activity Type, Carrier Type, Corporate Body Type, Documentary Form Type, Event Type, Place Type, Record Set Type) **permettent de catégoriser les entités**. **Leur valeur devrait normalement être prise dans des vocabulaires contrôlés**.

Les concepts de tels vocabulaires sont des **points d’accès majeurs aux métadonnées**, et constituent potentiellement des **clés de liage essentielles** entre jeux de métadonnées archivistiques.

En RDF, le modèle de référence pour représenter un vocabulaire contrôlé est [SKOS](https://www.w3.org/2004/02/skos/). Il permet de définir des concepts et les relations sémantiques (hiérarchiques ou associatives) qu’ils ont entre eux, ainsi que des relations d’alignement avec des concepts d’autres vocabulaires.

Il existe déjà des vocabulaires SKOS nationaux et internationaux pour certaines de ces notions (et d’autres vont voir le jour, en France par exemple ; on le souhaite en tout cas). Voir par exemple le thésaurus matières pour l’indexation des archives locales.

---
<!-- slide 27-->

#### Les ’types’ dans RiC-O (2)

Si l’on en fait une analyse scientifique fine, certaines notions au moins (ou les contenus de certains de ces vocabulaires) peuvent être considérées comme des entités historiques, ayant une histoire, voire des dates d’existence et une dimension spatiale, précédant ou suivant d’autres concepts. C’est le cas notamment pour rico:DocumentaryFormType ou rico:ActivityType.
Le modèle SKOS n’inclut pas les propriétés nécessaires pour exprimer ces facettes contextuelles, historiques.

Afin de permettre, si et quand ce sera nécessaire, de décrire ces notions comme des entités historiques, **RiC-O définit donc ses propres classes, sous-classes de rico:Type, pour représenter les attributs de catégorisation de RiC-CM**. 

**Il est très simple d’articuler un vocabulaire SKOS existant avec une des classes RiC-O ainsi définie**. Il suffit de spécifier que les concepts de ce vocabulaire sont aussi des instances de la classe RiC-O correspondante. 
RiC-O est accompagnée de l’embryon de deux vocabulaires, un pour les Record Set Type (types d’ensemble documentaire), un pour les Documentary Form Type (types de document).

---
<!-- slide 28-->
#### Les ’types’ dans RiC-O (3)

.reduite[![le fichier OWL de RiC-O](images/RiC-O/a-documentary-form-type.jpg)]

---
<!-- slide 29 -->
#### La modélisation des lieux dans RiC-O (1)

Les lieux dans RiC sont considérés, non pas comme des concepts, mais comme des **entités géo-historiques**.

RiC-O propose d’utiliser jusqu’à trois classes pour représenter un lieu, si besoin. 

Ce modèle est assez proche d’autres modèles en cours d’élaboration, comme [Linked Places Format](https://github.com/LinkedPasts/linked-places).


---
<!-- slide 30 -->
#### La modélisation des lieux dans RiC-O (2)

Exemple ici avec la commune d’Ambérieux-en-Dombes (dans le référentiel des lieux des Archives nationales).

.reduite[![le fichier OWL de RiC-O](images/RiC-O/a-place.jpg)]

.smaller[On aurait pu ajouter des relations vers les noms (instances de rico:PlaceName) de la commune, vers des lieux qui ont précédé cette commune, vers des lieux localisés dans cette commune… Et bien sûr, on peut ensuite spécifier, par exemple, des relations vers des agents dont le ressort ou le siège social est dans cette commune, ou des documents ayant cette commune pour sujet.]

---
<!-- slide 31-->
#### Les classes de relations dans RiC-O (1)

Une relation peut être considérée comme un objet à part entière, ayant par exemple une date et une description.
C’est particulièrement vrai pour certaines relations dont la portée est importante en archivistique, comme les relations entre agents, ou celles entre agents et activités. Et dans de nombreux autres cas !

Exemple : « Jean-Noël Jeanneney (1942-….) a été président de Radio France et RFI de 1982 à 1986. »

En EAC-CPF (ISAAR-CPF) (adapté de la notice d’autorité 050789 des Archives nationales) :

.reduite[![Jean-Noêl Jeanneney et Radio-France, une relation EAC-CPF](images/RiC-O/Jeanneney-RadioFrance.jpg)]

---
<!-- slide 32 -->
#### Les classes de relations dans RiC-O (2)

Dans RiC-O, les 77 relations binaires définies dans RiC-CM sont présentes. Cependant, comme une propriété RDF (par ex. _rico:hasProvenance_, transposition de la relation RiC-R026 de RiC-CM) ne peut pas avoir d’attributs et qu’ici on a besoin notamment de décrire et de dater la relation, les relations sont aussi représentées comme des classes.

.smaller[Cette méthode de modélisation permet aussi de connecter, à l’aide d’une telle relation, de nombreuses entités autres que la source et la cible d’une relation binaire. La relation binaire est alors par ailleurs formellement définie dans RiC-O comme un chemin simple, déclaré comme déductible, par inférence, de la combinaison de deux triplets dans lesquels une instance d’une classe Relation est centrale.]

.reduite3[![Une relation complexe et son raccourci](images/RiC-O/a-complex-relation-and-its-shortcut.jpg)]

---
<!-- slide 33 -->
#### De la théorie à la pratique : mises en œuvre actuelles et futures de RiC-O

RiC-CM et RiC-O n’en sont qu’à leurs **débuts** et il y a peu d’implémentations de RiC-O.

Voir la page [RiC-O projects and tools](https://ica-egad.github.io/RiC-O/projects-and-tools.html) qui sera mise à jour prochainement.

Parmi les utilisateurs présents ou futurs que le groupe EGAD connaît :
* le projet [Social Networks and Archival Context (SNAC) Cooperative](https://snaccooperative.org/)
* la société suisse [Docuteam](http://www.docuteam.ch), éditeur de logiciels
* le projet de recherche allemand [SONAR(idh)](https://sonar.fh-potsdam.de/)
* très probablement, à la suite d’une étude précise qui a préconisé le choix de RiC-O, et après réalisation des évolutions nécessaires, le **portail FranceArchives**
* et les **Archives nationales de France**

Il est très probable qu’aucun projet ou institution n’utilise la totalité des composants RiC-CM ou a fortiori RiC-O.

Ils en utiliseront un sous-ensemble, et l’étendront aussi sans doute (ajouteront des sous-classes et sous-propriétés pour satisfaire des besoins spécifiques).

---
layout: true
class: left, top
background-image: url(images/RiC-O/logo-archives-nationales.png)
background-size:150px
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programmeJ2)]



---
name: anf
<!-- slide 34 -->

## 3. Records in Contexts-Ontology (RiC-O) aux Archives nationales


.reduite[![un graphe](images/RiC-O/fondEcran-graphe.png)]

---
<!-- slide 35 -->
### Cette présentation...

...est une synthèse actualisée de la présentation faite le 28 janvier aux Archives nationales sur ce sujet, lors de la journée d’étude sur « Les métadonnées archivistiques en transition : le nouveau cadre normatif, les enjeux et les premières réalisations ».

La présentation est téléchargeable ici : [https://f.hypotheses.org/wp-content/blogs.dir/2167/files/2020/02/20200128_3_RiCauxAN_EnjeuxPremieresRealisations.pdf](https://f.hypotheses.org/wp-content/blogs.dir/2167/files/2020/02/20200128_3_RiCauxAN_EnjeuxPremieresRealisations.pdf).
---
<!-- slide 36-->
#### L’existant et les problématiques

Les métadonnées des AN aujourd’hui : 
* une **série de silos** ; le principal contient **plus de 29000 instruments de recherche (en EAD)** et **15000 notices d’autorité sur les producteurs (en EAC-CPF)**, ainsi qu’une vingtaine de vocabulaires d’indexation dans un format XML maison - mais il est loin d’être seul !
        * Déjà beaucoup de relations, peu visibles et non cherchables dans la SIV
        et relevant d’une seule catégorie (la provenance) pour ce qui concerne les IR et les producteurs
        * Des vocabulaires ou notices destinés à l’indexation, sous-employés et pauvres
        * Un grand nombre de bases de données, et d’autres réservoirs (métadonnées du SAE, bibliothèque numérique...)
* Plusieurs interfaces utilisateurs
* Très peu de portes d’accès intuitives (qui, quand, où, quoi...)
* Beaucoup de redondances
* Difficulté pour le lecteur à comprendre les listes de résultats et leur affichage (dans le contexte des IR)
* Très peu de ponts établis avec d’autres SI

---
<!--slide 37-->
#### Des besoins et une hypothèse

* Donner **plus de cohérence et de précision aux métadonnées**
* **Décloisonner les silos** internes de métadonnées
* **Améliorer l’accès aux métadonnées**, faire évoluer la SIV
* Avancer vers **l’interopérabilité** avec d’autres réservoirs

L’hypothèse, devenue aujourd’hui, après plusieurs années de travail, une certitude : **RiC peut aider à faire cela**. 

---
<!-- slide 38 -->
#### Première étape : une preuve de concept (PIAAF)

PIAAF, pour vérifier:
* qu’on peut convertir sans perdre en précision les métadonnées actuelles en RDF/RiC-O, donc passer de jeux de métadonnées "bidimensionnelles" à un graphe orienté
* qu’on peut grâce aux technologies RDF les interconnecter avec des jeux de métadonnées externes
* qu’on peut envisager des interfaces de recherche et de consultation très différentes, montrant le graphe produit


---
<!-- slide 39 -->
#### PIAAF : bilan

Objectifs atteints ! Le travail s’est avéré non trivial, mais il a pu être mené à bien et a produit des jeux de données riches, ayant le niveau de qualité attendu.

Une expérience très encourageante d’implémentation de RiC-O (dans une version très antérieure à la version 0.1, à un niveau très précoce de développement de l’ontologie). Elle a montré que :

   * RiC-O est utilisable pour exprimer la complexité de la description archivistique !
   * On **peut obtenir des résultats intéressants à partir de jeux de métadonnées préexistants, réels, issus des SI actuels**. Cependant divers points d’attention ont été identifiés ; en particulier **il est essentiel de définir et mettre en œuvre une stratégie d’attribution et de gestion d’identifiants pérennes pour les entités à décrire, et, de manière générale, d’améliorer et de contrôler la qualité des métadonnées à convertir**
   * On pouvait imaginer d’aller beaucoup plus loin
   * En ce qui concerne la visualisation du graphe ainsi formé et les nouvelles possibilités de recherche, des perspectives très intéressantes s’ouvraient

.smaller[Voir aussi, dans la documentation de PIAAF :
* la page [Contenu du portail](https://piaaf.demo.logilab.fr/stats/)
*  la page [Réalisation](https://piaaf.demo.logilab.fr/editorial/contexte-technique)]

.smaller[Nota bene : des problèmes de fonctionnement ont récemment été détectés dans PIAAF. Ces problèmes seront réglés prochainement.]

---
<!-- slide 40-->
#### Deuxième étape (en cours) : passer à l’échelle

* **Produire en masse des données RDF** à partir de l’ensemble des métadonnées existantes
   * à partir des fichiers EAD et EAC : se doter d’un outil > **RiC-O Converter**
   * à partir des autres silos de données : base de données par base de données (ex. de Léonore) ; métadonnées des archives numériques
   * à partir des vocabulaires d’indexation : **définir un modèle de données, construire des référentiels conformes à ce modèle** (référentiel des agents, des fonctions, des lieux vus comme des entités géo-historiques...)
   * dans ce processus, **RiC-CM est le socle global de référence**, mais d’autres modèles (comme SKOS) peuvent aussi être utiles
* Définir et mettre en œuvre les **évolutions nécessaires dans le SI** pour gérer, stocker et diffuser ces données, sans oublier :
    * la génération et la gestion des URIs
    * la mise en place de dispositifs de recherche adaptés (lever les verrous SPARQL et RiC-O)
    * la réalisation d’une interface d’exploration et de consultation
* Définir et mettre en œuvre une **stratégie scientifique d’amélioration de la qualité des métadonnées** (dans le cadre du PSCE) :
    * principes
    * outils et méthodes de gestion de la qualité
    * formations  

---
<!-- slide 41 -->

#### RiC-O Converter : où en est-on ?

Les sources et la documentation de l’outil ont été publiées sur [GitHub](https://github.com/ArchivesNationalesFR/rico-converter) en avril 2020.

Les AN disposent aujourd’hui d’un outil fonctionnel et peuvent convertir sans problème en RDF/RiC-O, en moins de 30 min, tous leurs fichiers EAD et EAC-CPF (ce qui produit actuellement 155 millions de triplets RDF environ).
Elles sont en mesure de faire évoluer l’outil.

RiC-O Converter est **sous licence libre** ; il **peut être réutilisé et modifié par toute personne ou projet intéressé** !

Il est accompagné des premiers **tableaux de mappings existants ['EAD vers RiC-O'](https://github.com/ArchivesNationalesFR/rico-converter/blob/master/ricoconverter/ricoconverter-doc/src/main/resources/EAD_to_Ric-O_0.1_documentation.xlsx) et ['EAC-CPF vers RiC-O'](https://github.com/ArchivesNationalesFR/rico-converter/blob/master/ricoconverter/ricoconverter-doc/src/main/resources/EAC_to_Ric-O_0.1_documentation.xlsx)**. Ces mappings peuvent servir de base pour n’importe quel autre projet similaire, même si on n’utilise pas RiC-O Converter pour la sémantisation.

L’enregistrement du webinaire du 25 juin 2020 sera diffusé très prochainement sur la chaîne YouTube des Archives nationales.

---
<!-- slide 42 -->
#### Les leçons du développement et de l’utilisation de RiC-O Converter

Voir diapos 14 à 26 de la [présentation de l’outil faite le 28 janvier](https://f.hypotheses.org/wp-content/blogs.dir/2167/files/2020/02/20200128_4_RiCOConverter.pdf).

---
<!-- slide 43 -->
#### Construire des référentiels : exemple pour les langues utilisées

Nous avons pu passer de ceci :

    <d id="name4">
      <terme>Français/fre</terme>
    </d>

à ceci :

    <rico:Language rdf:about="language/FRAN_RI_100-name4">
      <owl:sameAs rdf:resource="http://id.loc.gov/vocabulary/iso639-2/fre"/>
      <rico:identifier>Ancien identifiant SIA : FRAN_RI_100.xml#name4</rico:identifier>
      <rdfs:label xml:lang="fr">Français/fre</rdfs:label>
      <skos:prefLabel xml:lang="fr">français</skos:prefLabel>
      <skos:prefLabel xml:lang="en">French</skos:prefLabel>
    </rico:Language>
 
---
<!-- slide 44 -->

#### Construire des référentiels : exemple pour les types de documents

Nous avons pu passer de ceci :

    <d id="d3nd89lgo1-7iazj0zqu03g">
      <terme>acte de vente</terme>
      <interdits>
        <i>acte d’achat</i>
        <i>acquisition</i>
        <i>adjudication</i>
      </interdits>
      <d id="d3nd89me2v-1xfyihxuch3po">
        <terme>titre de propriété</terme>
      </d>
    </d>

à ceci (pour commencer) :

    <skos:Concept rdf:about="documentaryFormType/FRAN_RI_001-d3nd89lgo1-7iazj0zqu03g">
      <rdf:type rdf:resource="https://www.ica.org/standards/RiC/ontology#DocumentaryFormType"/>
      <skos:inScheme rdf:resource="documentaryFormTypes"/>
      <skos:prefLabel xml:lang="fr">acte de vente</skos:prefLabel>
      <skos:altLabel xml:lang="fr">acte d’achat</skos:altLabel>
      <skos:altLabel xml:lang="fr">acquisition</skos:altLabel>
      <skos:altLabel xml:lang="fr">adjudication</skos:altLabel>
      <skos:narrower rdf:resource="documentaryFormType/FRAN_RI_001-d3nd89me2v-1xfyihxuch3po"/>
    </skos:Concept>
    <skos:Concept rdf:about="documentaryFormType/FRAN_RI_001-d3nd89me2v-1xfyihxuch3po">
      <rdf:type rdf:resource="https://www.ica.org/standards/RiC/ontology#DocumentaryFormType"/>
      <skos:inScheme rdf:resource="documentaryFormTypes"/>
      <skos:prefLabel xml:lang="fr">titre de propriété</skos:prefLabel>
      <skos:broader rdf:resource="documentaryFormType/FRAN_RI_001-d3nd89lgo1-7iazj0zqu03g"/>
    </skos:Concept>

---
<!-- slide 45 -->
#### Les lieux : les voir comme des entités géo-historiques

Exemple de point de départ :

    <d id="d3ntcnfgcw--pvic5jinawzd">
      <terme>L’Abergement-Clémenciat (Ain)</terme>
      <noticel>
         <forms>
            <f>L’Abergement-Clémenciat</f>
         </forms>
         <geo>C01-101#001-0001#N01-1</geo>
         <reg>Rhône-Alpes</reg>
         <dpt>Ain</dpt>
         <arr>12</arr>
         <canton>Châtillon-sur-Chalaronne</canton>
         <insee>01001</insee>
      </noticel>
    </d>

Nous avons pu obtenir, pour ce lieu, ce graphe RDF/RiC-O :
.reduite3[![graphe RDF pour l’Abergement-Clémenciat](images/RiC-O/Screenshot_2020-06-24-GraphDb-lieux.png)]

---
<!-- slide 46 -->
#### En attendant une application sémantique (un data.archives-nationales.fr ?)

Les référentiels d’indexation, et peut-être celui des producteurs, seront accessibles dès que possible en open data, dans leur format RDF/RiC-O, et probablement aussi en CSV pour les vocabulaires.

---

<!-- slide 47 -->
#### Poursuivre les recherches et développements sur l’interface de consultation 

**Lever deux verrous** :
* impossible de demander à un utilisateur final de connaître SPARQL et sa syntaxe
* impossible de demander à un utilisateur final de connaître RiC-O (or pour interroger des données RDF/RiC-O il faut connaître le vocabulaire et les spécifications RiC-O)

Projet en cours de définition pour améliorer l’éditeur visuel de requêtes SPARQL [Sparnatural](http://blog.sparna.fr/2019/06/13/sparnatural-ecrire-des-requetes-sparql-tout-naturellement/)

---
<!-- slide 48 -->
#### Sparnatural : pour construire très intuitivement sa requête SPARQL sur des données RiC-O...

.reduite[![Copie d’écran Sparnatural sur PC local, requete sur data webinaire](images/RiC-O/sparnatural-local.jpg)]



---


name: exercices
<!-- slide 49 -->
## 4. Exercices

.reduite[![un graphe](images/RiC-O/fondEcran-graphe.png)]

---
<!-- slide 50 -->
### 4.1. Exercice 1 : étudier un petit instrument de recherche en EAD, du point de vue de RiC

Ouvrir le fichier XML/EAD nommé FRAN_IR_054848.xml

Nota :
* il est consultable dans la salle des inventaires virtuelle des AN ; son permalien est le suivant :<br>[https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_054848](https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_054848).
* il est associé à la notice d’autorité (en EAC-CPF) FRAN_NP_005422.xml, dont la vue HTML est consultable via le permalien [https://www.siv.archives-nationales.culture.gouv.fr/siv/NP/FRAN_NP_005422](https://www.siv.archives-nationales.culture.gouv.fr/siv/NP/FRAN_NP_005422).


Quelles **entités** RiC y sont décrites ?

Quelles propriétés ont ces entités ?

Quelles **relations** existent entre ces entités ?

D’après cet unique exemple, quelles questions ou problématiques soulève le "passage à RiC" selon vous ?

Si vous avez un peu en tête la collection de fichiers EAD de votre service d’archives, ou encore ses instruments de recherche ISAD(G), quelles réflexions ce petit exercice vous inspire-t-il ?

---
<!-- slide 51 -->
### 4.2. Exercice 2 : utiliser le logiciel RiC-O Converter

Télécharger la dernière release de RiC-O Converter : [https://github.com/ArchivesNationalesFR/rico-converter/releases/tag/v1.0.0](https://github.com/ArchivesNationalesFR/rico-converter/releases/tag/v1.0.0).

Dézipper le dossier. On y trouve :
* dans le dossier ’documentation’, un site web de documentation. Dans votre navigateur, ouvrir le fichier index.html qui est dans ce dossier. Vous y retrouvez notamment les mappings EAD et EAC vers RiC-O.
* dans le dossier unit-tests, les tests unitaires qui ont permis d’établir les spécifications détaillées du logiciel
* dans le dossier parameters, des fichiers de paramètres, adaptés au cas des AN, mais qui peuvent être modifiés
* dans le dossier input-ead, des fichiers EAD prêts à convertir
* idem dans le dossier input-eac

Dans un premier temps, exécuter la conversion des fichiers EAD (sur un ordinateur sous Windows, double-cliquer sur le fichier ricoconverter.bat et répondre 'convert_ead' à la première question, puis retour chariot à la question suivante).

La conversion prend quelques secondes, le résultat est stocké dans le dossier output-ead-{AAAAMMJJ}. 

---
<!-- slide 52 -->
#### Exercice 2, suite

Dans le dossier RDF de sortie, il y a un fichier RDF par instrument de recherche.
Ouvrir le fichier FRAN_RecordResource_054848.rdf dans votre éditeur XML.

Des questions ?

Faire la même chose pour les 101 fichiers EAC-CPF stockés dans le dossier input-eac.
Dans le dossier RDF de sortie, on trouve 3 sous-dossiers, dont un pour les agents (101 fichiers RDF, un pour chaque entité décrite) et un pour les relations entre agents et les relations de provenance.
Ouvrir le fichier FRAN_Agent_005422.rdf

Des questions ?

---
<!-- slide 53 -->
#### Paramétrer la conversion

Pour cela on utilise les fichiers XML du dossier parameters

Ouvrir le fichier convert_ead.properties

On y trouve des paramètres pour la gestion des fichiers à traiter et des fichiers générés, et des paramètres qui vont définir certaines caractéristiques des fichiers de sortie, comme le segment invariant de l’URI. 

Si l’on veut vraiment modifier le comportement de RiC-O Converter pour l’adapter à un cas d’utilisation d’EAD différent de celui des AN, il faudra modifier les scripts de transformation. Pour cela, il faut être un bon développeur XSLT 2.0, ou trouver un tel développeur !

Les AN vont faire vivre le logiciel :
* produire, tout d’abord, un 2e release, prenant en compte RiC-O v0.2 (début 2021)
* continuer à enrichir le logiciel

On serait heureux de pouvoir étudier toute demande de modification, ou de travailler avec des ingénieurs d’autres institutions.

---
<!-- slide 54 -->
### 4.3. Exercice 3 : utiliser GraphDB Free et SPARQL pour faire des requêtes dans le RDF obtenu précédemment

On va utiliser un des entrepôts (une des bases de données RDF) qui se trouve ici [http://graphdb.sparna.fr/](http://graphdb.sparna.fr/) et qui se nomme **’formationAAF-sept2020’**.

**Cet entrepôt ne sera disponible que le temps de la formation**. 
Mais il est possible de mettre en place un tel entrepôt en local, sur un ordinateur de bureau.
Le logiciel (libre) employé ici est GraphDB Free (voir [https://www.ontotext.com/products/graphdb/](https://www.ontotext.com/products/graphdb/)).

Les données RDF produites pendant l’exercice précédent sont déjà présentes dans l’entrepôt, ainsi que RiC-O (dernière version en date). Ce qui donne un total d’environ 895 000 triplets RDF.

---
<!-- slide 55 -->
#### Exercice 3, suite

Après avoir sélectionné l’entrepôt, on peut pour commencer explorer un peu :
* la hiérarchie des classes disponibles (Explore > Class hierarchy)
* les relations entre les entités décrites dans l’entrepôt (Explore > Class relationships)

.reduite[![Copie d’écran GraphDB](images/RiC-O/GraphDB-screenshot.jpg)]

---
<!-- slide 56 -->
#### Exercice 3, suite : utiliser SPARQL

Ouvrir le fichier de requêtes SPARQL 'SPARQL-RiC-O.txt'.

Choisir une requête, puis la copier/coller dans la fenêtre GraphDB > SPARQL

_Enjoy_  comme on dirait en anglais !

---
layout: true
class: left, top

.footnote[
      [Programme](#programmeJ2)]
---
name:conclusion
<!-- slide 57 -->
## 5. Conclusion de cette journée


.reduite[![un graphe](images/RiC-O/fondEcran-graphe.png)]

---
<!-- slide 58 -->
### Bilan 

Est-ce que cette journée vous a intéressés ?

Des réactions, des suggestions, des questions ?

Si les circonstances le permettent, il devrait y avoir en 2021 un **workshop de 3 jours aux Archives nationales** pour approfondir toutes ces questions.

À retenir :
* _novembre 2020_ : publication de **RiC-CM v0.2 final draft** et de **RiC-O v0.2**, avec appel à commentaires
* _2021_ : publication de **RiC-CM et RiC-O v1.0**
* sur RiC-O, vous pouvez utiliser l’entrepôt GitHub pour suggérer des améliorations ou soumettre des problèmes
* **n’hésitez pas à me contacter** !
---
<!-- slide 59-->
### Éléments de bibliographie

Outre les liens déjà fournis vers les sources et site web de RiC-O, on peut consulter :

- Dans les annexes de l’Abrégé d’archivistique de l’AAF (4e édition), les [annexes du chapitre 6](https://abrege.archivistes.org/Chapitre-VI-Instruments-de-recherche-la-normalisation-au.html). 
- Parmi ces annexes, une [bibliographie](https://abrege.archivistes.org/sites/abrege.archivistes.org/IMG/pdf/chapitre_vi__complement_bibliographique.pdf) qui liste des références utiles.