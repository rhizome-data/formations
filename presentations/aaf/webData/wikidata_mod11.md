---
title: Wikidata - Module 1 Qu'est ce que wikidata ?
tags: formation, wikidata, websem

---

# Wikidata - Module 1 Qu'est ce que wikidata ?

1. Objectifs

**=> (qu'est-ce que wikidata ?)** M1-10
**=> (wikidata, Objectifs)** M1-20
**=> (wikidata, écosystème wiki)** M1-30
**=> (wikidata, Une base de données ouvertes)** M1-40

2.  Principes de fonctionnement

**=> (wikidata, Une page par entité)** M1-50
**=> (wikidata, rechercher une entité)** M1-60
**=> (wikidata, consommer les données des entités)** M1-70
**=> (wikidata, les éléments du modèle de données)** M1-80
**=> (wikidata, récapitulatif du modèle de données)** M1-90
**=> (wikidata, analyser une page wikidata)** M1-100
**=> (wikidata, obtenir de l'aide)** M1-110

3.  Liens avec d'autres projets libres

**=> (wikidata, les liens avec d'autres projets de données ouvertes)** M1-110

---
## Objectifs

**=> (slide : qu'est-ce que wikidata ?)** M1-10

### Qu'est ce que wikidata ?

=> Qu'est-ce que ce module vous apprendra ?

- Ce qu'est Wikidata, comment il s'inscrit dans la galaxie "wiki"
- La différence entre Wikidata, DBpédia et Wikipédia
- Comment trouver des informations dans Wikidata ?
- Comment ajouter ou modifier des informations dans Wikidata ?


---
**=> (slide : wikidata, Objectifs)** M1-20

### Objectifs de wikidata

**Wikidata** est un projet de la Fondation Wikimedia : une base de données libre, collaborative et multilingue, qui collecte des **données structurées** pour alimenter Wikipédia, Wikimedia Commons, les autres projets Wikimédia et toutes formes de réutilisation. ([https://www.wikidata.org/wiki/Wikidata:Introduction/fr Wikidata:Introduction, nov 2016).
Il s'appuie sur les principes du web de données en s'appuyant sur un modèle de données RDF.

Dit autrement : Wikidata est la *base de connaissance*, la *plateforme centrale de gestion de données* pour Wikipédia et pour les autres projets de Wikimédia.

Avec Wikidata, l’accent est mis sur les données structurées : si Wikipédia se veut une encyclopédie, Wikimedia Commons une banque d'images et d'autres fichiers et le Wiktionnaire, un dictionnaire contenant entre autres, définitions et synonymes, Wikidata a quant à lui pour objectif de **fournir des données structurées**.

Ceci rend les données exploitables par les humains comme par les ordinateurs. Les données structurées ouvrent aussi un champ d’opportunités incroyables dont vous prendrez connaissance par la suite.

Qu'est-ce qu'une donnée structurée ? C'est une donnée comprenant des éléments de description. **Ces éléments de description font référence à des catégories qui en précisent la sémantique**.

Par exemple un email est une donnée structurée. Dans un email on a un destinataire, un expéditeur, une date d'envoi ou de réception, un objet, qui sont séparés du corps du message. Cela permet par exemple de chercher ou classer ses emails par destinataire ou par date.

Ce que l'on appelle plein texte est en revanche un ensemble de mots qu'il faut lire pour en comprendre le contexte.
La structuration des données permet, entre autres choses, de faciliter leur exploitation par les machines.


---
**=> (slide : wikidata, écosystème wiki)** M1-30

## Principes de fonctionnement

### La galaxie wiki

Un **Wiki** est un outil de rédaction de contenu généralement ouvert à la contribution. Il dispose de mécanismes favorisant la structuration des contenus contribuant ainsi à leur sémantisation. 

**Wikimédia** est un groupe de projets liés comprenant Wikipédia, Wiktionnaire, Wikisource et autres, qui ont pour objectif d’utiliser la puissance collaborative de l’Internet et du concept de wiki pour créer et partager la connaissance libre sous toutes ses formes.

**Wikipédia** est un projet Wikimédia qui est une encyclopédie du net, mondiale, libre et supportant de nombreuses langues. C'est aussi le plus grand et le plus ancien projet Wikimédia, datant même d'avant la Fondation Wikimedia elle-même.

Vous connaissez peut-être aussi **DBpedia**, qui extrait des données structurées depuis les infobox de Wikipédia et les publie en rdf. On pourrait penser à première vue que Wikidata et DBpedia sont comparables mais ce n'est pas le cas. Ils ont des rôles très différents. Wikidata inverse le processus de DBpedia, au lieu d'extraire des informations des infobox, il *nourrit* les infobox à partir de données structurées saisies sur une page wikidata, et DBpedia utilise ces données pour fournir à son tour d'autres services.

---
**=> (slide : wikidata, Une base de données ouvertes)** M1-40

### Le wiki des données

Ce qui rend **Wikidata** unique, c'est la richesse de ses données. Beaucoup intègrent des informations de *provenance* ou des données *contextuelles* supplémentaires, les données sont connectées à d'autres jeux de données externes ou à des référentiels internationaux. Toutes ces données structurées sont libres et multilingues.

Comme Wikipédia, Wikidata est organisée en pages et c'est ainsi que les données sont structurées. Chaque *sujet* sur lequel Wikidata a des données structurées est appelé *entité* et chaque entité a sa propre page. Le système distingue deux types d'entités, des *éléments* ("*item*") et des *propriétés*.
Les éléments ont un identifiant commençant par la lettre Q suivi d'un nombre (par exemple l'élément "Belgique" a pour identifiant [Q31](https://www.wikidata.org/wiki/Q31)) et les propriétés ont un identifiant commençant par la lettre P suivi d'un nombre (par exemple la propriété "instance de" a pour identifiant [P31](https://www.wikidata.org/wiki/Property:P31)).

On vérifie qu'on a bien une page pour l'élément "Belgique" et une page pour la propriété "instance de".

---
### Principe de fonctionnement : le modèle sujet - prédicat - objet

Le modèle de description de ressources appelé RDF propose de découper les informations que l'on possède sur une chose en phrases simples composées de 3 parties : 

sujet - prédicat - objet

où le sujet dispose d'un identifiant d'entité (ce à propos de quoi j'ai des informations), le prédicat est un attribut disposant d'un identifiant et l'objet contient la valeur de cet attribut

identifiant d'entité - nom de l'attribut - valeur de l'attribut

Chaque sujet est identifié par une URI (Unique Resource Identifier) ce qui permet d'éviter les ambiguïtés


|                    sujet                     |                    prédicat                    |           objet           |
|:--------------------------------------------:|:----------------------------------------------:|:-------------------------:|
|             identifiant d'entité             |               nom de l'attribut                |   valeur de l'attribut    |
| https://www.rhizome-data.fr/formation/topic1 | https://www.logilab.fr/2018/formation/ns#title | "Introduction à wikidata" |

Ici faire un graphique pour montrer la construction des graphes

Pour écrire les triplets RDF on utilise fréquemment la notation abrégée Turtle qui rend la lecture plus simple

Elle permet d'utiliser des raccourcis pour les url des attributs en utilisant le mot clé PREFIX

Ainsi on peut faire référence aux attributs du modèle de description des formations proposées par Logilab sous cette forme

```XML
PREFIX lb: <https://www.logilab.fr/2018/formation/ns#>
PREFIW rd: <https://www.rhizome-data.fr/formation/#>


rd:topic1 lb:title "Introduction à wikidata"
```



| sujet                             | prédicat                                       | objet                     |
|:--------------------------------- | ---------------------------------------------- | ------------------------- |
| identifiant d'entité              | nom de l'attribut                              | valeur de l'attribut      |
| https://rhizome-data.fr/et/topic1 | https://www.logilab.fr/2018/formation/ns#title | "Introduction à wikidata" |


Un catalogue de formation pourrait donc être présenté sous cette forme



| sujet    | titre                          | durée   | niveau        | date de session |
|:-------- | ------------------------------ | ------- | ------------- | --------------- |
| wikidata | Introduction à wikidata        | 1 jour  | débutant      | 2020-05-24      |
| wikidata | Usages avancés de wikidata     | 2 jours | avancé        | 2020-09-23      |
| wikidata | Contribuer à wikidata          | 1 jours | intermédiaire | 2020-06-30      |
| Python   | Introduction à Python          | 1 jour  | débutant      | 2020-07-03      |
| Python   | Calcul scientiques avec Python | 2 jours | avancé        |                 |
| Juniper  | Introduction aux notebook      | 2 jours | avancé        |                 |

Dans ce modèle chaque ligne représente un sujet, chaque colonne un prédicat et chaque cellule un objet

Cependant si on souhaitait faire figurer plusieurs dates pour chaque formation cela nécessiterait de rendre plus complexe ce modèle.

Avec RDF il suffit de rajouter des déclarations à propos d'un sujet et de les lier par la référence au sujet identique.

Pour récupérer la liste des formations qui ont comme sujet wikidata la requête serait

```XML
PREFIX lb: <https://www.logilab.fr/2018/formation/ns#>

SELECT ?formation
WHERE {
    ?formation lb:sujet "wikidata"
}
```

---
**=> (slide : wikidata, Une page par entité)** M1-50

### Principes de fonctionnement : une page

La page de Georges Cuvier : https://www.wikidata.org/wiki/Q171969

Chaque page d'élément contient les parties suivantes :

- un label (p.ex., “Georges Cuvier”),
- une courte description, p.ex.: 
    - “naturaliste, zoologiste, paléontologue, historien des sciences et illustrateur français” 
    - "French naturalist, zoologist and paleontologist (1769–1832)"
- une liste d'alias ("Also known as") (p.ex., “Georges-Léopold-Chrêtien-Frédéric-Dagobert Baron Cuvier”),
- une liste d'énoncés ("Statements") (c'est la partie la plus riche des données),
- une liste de liens vers d'autres sites.

Les trois premières parties sont généralement désignées comme les "termes", ce sont eux qui permettent de trouver et d'afficher les éléments. Un élément peut avoir des termes dans toutes les langues supportées par Wikidata.

Dans les "Statements" qui suivent (nos "énoncés"), on remarque que si l'on survole "instance of" on voit s'afficher "P31" et si l'on survole "human" on affiche l'identifiant de l'élément correspondant (Q5) : Georges Cuvier est donc une "instance de" *human*. On peut soupçonner qu'il en sera de même de toutes les personnes que nous trouverons dans Wikidata et on peut également deviner que le schéma de la base de données Wikidata ressemble à un graphe, dans lequel un énoncé tel que "Georges Cuvier est une instance de human" s'écrit :

Q171969 --P31--> Q5

Parmi la liste de liens vers d'autres sites, on a une section très importante qui est celle des **identifiants**. La page Wikidata recense les identifiants d'une personne (VIAF, ISNI, ORCID, SUDOC, BNF, Bibliothèque du Congrès, ...) pour autant que des contributeurs les ont ajoutés.
(à vérifier : dans certains cas ce sont les sites externes qui créent l'entrée, p.ex. VIAF)

==> éventuellement : ajouter ici un exercice = ajouter un identifiant dans la page d'une personne (à déterminer)


---

### Principes de fonctionnement : une page d'une archive départementale

La page des Archives de la Seine-Maritime : https://www.wikidata.org/wiki/Q2860472

- deux labels (p.ex., “archives départementales de la Seine-Maritime”, ""),
- une courte description, p.ex.: 
    - “archives du département de la Seine-Maritime” 
- une liste d'alias ("Also known as") (p.ex., “Tour des archives (Rouen”),
- une liste d'énoncés ("Statements") (c'est la partie la plus riche des données),
- une typologie indiquant la nature de l'entité (P131)
- une liste de liens vers d'autres sites.


---

### Principes de fonctionnement : la page wikidata de la Belgique

https://www.wikidata.org/wiki/Q31

Sur cette page d'élément, on retrouve bien le label, la description et les alias.

Dans les "Statements" (nos "énoncés") on retrouve également "instance of" et on voit que la Belgique est une "instance de" plusieurs éléments : "Etat souverain", "Etat fédéral", "pays", "puissance coloniale". On y trouve bien d'autres énoncés, comme l'espérance de vie, la date de fondation de la Belgique, son hymne national, ainsi que le fait qu'elle faisait partie du groupe des pays victorieux de la Première Guerre Mondiale.

Cela nous rappelle que wikidata n'est pas une collection de "vérités" sur un élément donné (comme pourrait l'être une encyclopédie universitaire), mais **une collection d'énoncés** (formulés par les contributeurs), qui peuvent évoluer dans le temps.

---

### Principes de fonctionnement : la page wikidata de Kylian MBappé

https://www.wikidata.org/wiki/Q21621995?uselang=fr

Où l'on voit que la page s'enrichit à mesure que les informations changent : clubs dans lesquels KM a joué, buts marqués chaque saison, et même son poids (qui a dû changer entre Monaco et PSG). Cependant l'historique des clubs est considéré comme une info digne d'être conservée (avec son évolution dans le temps) alors que le poids de KM est donné sans date ce qui ne nous permet pas de connaître son évolution ni même de savoir s'il est à jour.

[ou pour le fun montrer la page de Boris Vian avec la liste de ses pseudonymes et comment c'est utile quand on fait une recherche sur google] https://www.wikidata.org/wiki/Q7833

---
### Principes de fonctionnement : la page wikidata de P31 "instance of"

https://www.wikidata.org/wiki/Property:P31

Les pages de propriétés comportent aussi des termes (labels, etc.) mais ne présentent pas d'énoncés ni de liens vers d'autres sites. Par contre elles comportent des "datatype" qui décrivent le type de données qu'elles acceptent, p.ex. le *datatype* de *date de naissance* est *time* (type temporel).

---
**=> (slide : wikidata, rechercher une entité)** M1-60

### La recherche
On peut chercher des entités avec le moteur de recherche. Le résultat donne généralement accès à la page de l'entité avec les différentes déclarations qui ont été rattachés.

Si on fait une recherche sur un type d'entité le moteur de recherche ne propose pas une liste résultats des occurence de ce type d'entité dans la base de données mais un ou plusieurs types d'entités disponibles dans le modèle de données.

par exemple : voiture

https://www.wikidata.org/wiki/Special:Search

On peut aussi de retrouver dans wikidata en consultant une page de wikipedia et en cliquant sur le bouton situé à gauche s'il existe une correspondance entre une page du wiki de wikipedia et celui de wikidata.

Passer de Wikipédia à Wikidata : 

![](https://i.imgur.com/AnTXbDE.jpg)

Faire des recherches dans wikidata nécessite donc l'apprentissange du language de requête SparQL. L'intérêt d'investir dans l'appresntissage de ce language est qu'il est utilisé par l'ensemble des points d'accès proposés par d'autres sites et soutenu par un standard international mainteu par le W3C.

Dans wikidata le prefix wdt: retourne par défaut le résultat privilégié, c'est-à-dire celui qui a le meilleur ranking. En effet il peut y avoir plusieurs occurences d'une propriété déclarée pour une même entité et certaines peuvent être définies comme la valeur préférée.

Par exemple 2 localisations administratives pour une même région.
Ou plusieurs valeurs de population pour une même  ville avec plusieurs dates associées. La valeur retournée par défaut est la plus récente ou la mieux notée.

> Aide : voir la liste des prefixes accessibles depuis l'aide du point d'accès wikidata


> FILTER NOT EXIST = MINUS (sauf exceptions)

Avec le point d'accès SparQL on peut poser des questions à wikidata

On peut poser des questions sur le sujet, ou sur l'objet
Par exemple quel est le lien entre Paris et Rome
wd:Paris ?lien ?wd:Rome .
Ou quell est la population de Paris ?
wd:Paris wdt:population ?pop .


```XML

# Retourne toutes les 10 premières valeurs de chiffres de population 
# de la ville de Paris.

SELECT ?chiffrePopulation
WHERE {
    wd:Q90 p:1082 ?population .
    ?population ps:1082 ?chiffrePopulation .
}
LIMIT 10

# Sélectionne le label français de l'entité Q71 (la ville de Genève)

SELECT ?itemLabel WHERE {
  BIND ( wd:Q71 AS ?item )
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
LIMIT 10

# Sélectionne le label chinois de la ville de Paris
SELECT ?nom
WHERE {
wd:Q90 rdfs:label ?nom .
FILTER ( lang(?nom) = "zh" )
}
LIMIT 10


# les villes de suisse dont le label commence par Aa résultat label français


SELECT ?elementLabel WHERE {
  ?element wdt:P31 wd:Q70208 ; rdfs:label ?elementLabel .
  FILTER ( lang(?elementLabel) = "fr" )
  FILTER ( regex(?elementLabel, "^Aa") )
}
LIMIT 10



# Trouve les personnes qui ont pour père la personnes A 
# et qui ne sont pas de sexe masculin


SELECT ?personneA ?personneB WHERE {
  ?personneA wdt:P22 ?personneB .
  MINUS { ?personneB wdt:P21 wd:Q6581097 }
}
LIMIT 10
```


---
**=> (slide : wikidata, consommer les données des entités)** M1-70

### La consommation

Toute entrée Wikidata peut être récupérée en format JSON brut ou en XML.

https://www.wikidata.org/wiki/Q171969 (page lisible par un humain sur Georges Cuvier)
https://www.wikidata.org/wiki/Special:EntityData/Q171969.json (format JSON)
https://www.wikidata.org/wiki/Special:EntityData/Q171969.ttl (format XML/RDF)


---
**=> (slide : wikidata, les éléments du modèle de données)** M1-80

### Principes de fonctionnement : de quoi est fait wikidata ?

#### Les éléments

Chaque élément doit être notable (par exemple se retrouver dans une entrée d'un site de Wikimédia comme Wikipédia, Wiktionary, Wikibooks, WikiNews, etc. Chaque élément doit être **unique** (une seule entrée par item). Ces items peuvent être **reliés**. 

Chaque élément possède sa propre page − où toutes les données à son sujet sont rassemblées − et un identifiant unique, toujours sous la forme Q###. Bien qu’il soit très utile pour les machines et pour représenter la connaissance dans de nombreuses langues, cet identifiant n’est pas très pratique pour les humains. 

#### Les libellés

Afin d’éviter d’avoir à se souvenir d’identifiants aléatoires comme Q###, on devrait attribuer à chaque élément un nom qui le définit précisément. Ces noms donnés aux éléments sont appelés **libellés** et devraient être ajoutés à toutes les pages d'élément. 

Un libellé est comme le titre d’une page décrivant le sujet de l’élément. Il doit être aussi court que possible, par exemple, Terre et non pas planète Terre.

Les labels n’ont pas besoin d’être uniques puisque la distinction entre homonymes se fait par les descriptions (que l’on verra plus tard).

Utilisez le nom le plus courant (par exemple chat et pas *Felis catus*) et ne mettez de majuscules qu'aux noms propres (comme Londres, Jupiter ou Clinton, mais pas ville, planète ou politicien).
    
#### Les descriptions

Les descriptions servent à distinguer les libellés en fournissant plus de détails sur l’élément. 

La **description** donne une définition courte d'une entrée. Cette description comprends quelques mots, ne constitue pas une phrase et suit quelques règles (minuscules, pas de ponctuation, pas d'articles, pas de phrase).

Il est possible d’avoir plusieurs éléments avec le même libellé, tant que chaque élément a une description différente. 

Soyez concis, les descriptions ne sont pas des phrases.

Essayez d’être le plus précis et le plus neutre possible, évitez les informations qui peuvent changer au court du temps ou être considérées comme controversées ou biaisées.

Les descriptions ne commencent normalement pas par un article comme *le* ou *un*. Rappelez-vous la description de Georges Cuvier : "naturaliste, zoologiste, paléontologue, historien des sciences et illustrateur français".

#### Les alias

Les **alias** donnent des noms alternatifs aux libellés.

Une appellation alternative pour un élément, tel que le surnom d'une personne ou le nom scientifique d'un animal, est appelé un alias sur Wikidata. Ajouter des alias à la page aidera à recenser toutes les appellations et tous les termes de recherche pour Terre, l’élément que vous avez travaillé si dur à améliorer.

#### Les déclarations

Les **déclarations** définissent des **propriétés** avec une ou plusieurs valeurs. Il s'agit donc des **données structurées** que l'on a sur un objet.

Toutes les pages d'éléments ont une section Déclarations qui peut comporter n’importe quel nombre de lignes contenant différentes choses — mots, nombres, même des fichiers images. Cela peut sembler compliqué mais c’est plutôt simple. 

Une déclaration est faite d’une catégorie de données, visible à gauche, et d’une entrée correspondante à cette catégorie, visible à droite. Dans Wikidata, on appelle une *catégorie de données* une **propriété**, tandis que les données qui décrivent un élément pour une propriété spécifique sont nommées **valeurs**. 

#### Les types de propriétés

Les **propriétés** sont **typées**, et aussi définies par des entrées dans wikidata.

Par exemple la propriété [pays de nationalité](https://www.wikidata.org/wiki/Property:P27) s'applique aux être humains.

> Georges Cuvier (Q171969) > pays de naissance (P27) = France (Q142)
 
Par ailleurs, *pays de nationalité* est une sous-propriété de pays.

[==> à partir de la page "pays de nationalité" parcourir les Statements et les traduire sous forme visuelle (graphe), schéma à venir]

#### Focus sur les propriétés

Les propriétés ont habituellement un nom unique et distinctif, par exemple couleur, pays de nationalité ou sœur.

Les propriétés limitent et définissent les types de valeurs pouvant être ajoutées à un élément. Par exemple, la « date de découverte » nécessite une valeur sous forme de date et ne va pas accepter n’importe quelle valeur comme « bleu » ou « France ». Ces contraintes protègent aussi du vandalisme (et des erreurs) sur Wikidata.

Chaque propriété est associée à un type de données. Il en existe actuellement [11](https://www.wikidata.org/wiki/Special:ListDatatypes) :



|       media        |  concept  |        technique        |
|:------------------:|:---------:|:-----------------------:|
| Médias sur Commons |  Elément  |    Notation musicale    |
|        URL         | Quantité  |   Forme géographique    |
|                    |  Chaîne   |  Coordonnées du globe   |
|                    |   Date    |    Texte monolingue     |
|                    | Propriété |   Données tabulaires    |
|                    |   Forme   |   Identifiant externe   |
|                    |   Sens    | Expression mathématique |
|                    |           |         Lexème          |


---
**=> (slide : wikidata, récapitulatif du modèle de données)** M1-90

### Principe de fonctionnement : Récapitulation du modèle

![illustration du modèle](https://i.imgur.com/k57P5Yd.png)

Ressources pour trouver les entités et les propriétés: 

* https://www.wikidata.org/wiki/Wikidata:List_of_properties
* https://www.wikidata.org/wiki/Wikidata:Database_reports/List_of_properties/Top100
* https://www.wikidata.org/w/index.php?title=Special%3AAllPages&from=&to=&namespace=120

=> comment savoir s'il existe une propriété pour xxx
=> quel est l'identifiant de la propriété "creator" ? "location" ? "depicts" ?
=> 

Pour en savoir plus sur le glossaire Wikidata : https://www.wikidata.org/wiki/Wikidata:Glossary/fr

[ajouter quelques exercices de modélisation]


---
**=> (slide : wikidata, analyser une page wikidata)** M1-100

#### Exercice
[à partir d'exemples, analyser les éléments constitutifs d'une page wikidata]

Exemple les administrations françaises

Les administrations françaises peuvent être identifiées comme des administrations publiques (administration publique (Q31728)) situées en France :

```XML

SELECT ?item ?itemLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr,en". }
  ?item (wdt:P31/(wdt:P279*)) wd:Q31728;
    wdt:P17 wd:Q142.
}
ORDER BY ?itemLabel
```

---
## Principes de fonctionnement avancés de wikidata
### Le modèle de données

Les statements ("énoncés") sont faits aussi de claims ("déclarations"), ranks ("classement") & qualifiers ("qualificatifs").

Les qualifiers:properties sont utilisés pour les déclarations plutôt que pour les éléments  par exemple. "la population de Paris est d'1 millon *en 1860*" [note: une déclaration est une arête ("edge"), dans un graphe on a des noeuds ("vertex") et des arêtes, ou lignes ("edges")]. Ils permettent de modifier la portée d'un élément, indiquer la manière dont une valeur a été obtenue, mettre des restrictions sur la portée d'une valeur ou inclure d'autres détails nécessaires à l'interprétation de la valeur. 

Les rangs ("ranks")constituent une mécanique permettant d’annoter plusieurs valeurs pour une déclaration : préféré ("preferred"), normal, obsolète ("deprecated") => utile pour identifier des déclarations périmées.

Les références ("references"): elles permettent d'indiquer la source des déclarations, leur provenance.

Les **liens de sites** incluent une URL et un titre et doivent pointer vers des pages provenant de Wikimédia. Pour chaque langue vous pouvez ajouter un lien par type de site et on peut indiquer la qualité (badge).

---
#### Faire des requêtes

Qualifiers

* p: qui ne pointe pas sur l'objet, mais sur le "nœud de déclaration". Ce nœud est alors le sujet d'autres triplets
* préfixe ps: (pour property -propriété- statement -déclaration-) pointe sur l'objet de la déclaration, 
* préfixe pq: (property -propriété- qualifier -alificatif-) sur les qualificatifs, 
* prov:wasDerivedFrom pointe sur les nœuds de références.

Ex : 
```
wd:Q12418 p:P186 ?statement3.    # Mona Lisa: material used: ?statement3
?statement3 ps:P186 wd:Q287.     # value: wood
?statement3 pq:P518 wd:Q1737943. # qualifier: applies to part: stretcher bar
?statement3 pq:P580 1951.        # qualifier: start time: 1951 (pseudo-syntax)

```

---
#### Faire des requêtes

**ORDER et LIMIT**

ORDER BY quelqueChose trie les résultats selon quelqueChose. quelqueChose peut être n'importe quelle expression– pour l'instant, le seul type d'expression que nous connaissons sont les simples variables (?quelqueChose)

Cette expression peut être caractérisée avec soit ASC() soit DESC()

---

**=> (slide : contrôle d'autorités sur Wikidata)**

[exercice : rechercher les identifiants de xxx
 => les identifiants sont-ils complets ?
 => les identifiants sont-ils à jour ?]

---
**=> (slide : ontologie)**

---
**=> (slide : Wikidata et RIC)**

Utiliser massivement Wikidata signifie aussi pour les institutions qui conservent des archives s’approprier facilement et rapidement les recommandations de base contenues dans le dernier projet en date de norme de description archivistique intitulé Records in Contexts (RiC) (voir [arbido 3/2017](https://arbido.ch/fr/edition-article/2017/metadonn%C3%A9es-donn%C3%A9es-de-qualit%C3%A9/records-in-contexts-vom-baum-zum-netz)), qui propose justement de fusionner les normes de description existantes en utilisant tout le potentiel des données ouvertes liées.
https://www.ica.org/en/records-in-contexts-ontology
https://www.ica.org/en/egad-ric-conceptual-model
https://www.ica.org/en/semantizing-and-visualising-archival-metadata-the-piaaf-french-prototype-online

---
**=> (slide : cas d'usage)**

- un site d'archives veut rendre "découvrables" ses images
- une institution de conservation d'archives veut renseigner la page wikipedia d'un individu pour indiquer qu'elle détient des sources à son sujet
- un service d'archives veut enrichir ses instruments de recherche de liens vers wikidata
- un site sur la biodiversité veut recenser tous les identifiants des naturalistes de ses listes
- une bibliothèque veut enrichir et/ou vérifier ses données (dates, identifiants...)

---
**=> (slide : cas d'usage)**

Les bienfaits de l'open access

[bilan de 3 ans d'ouverture des (méta-)données au METS museum](https://www.metmuseum.org/blogs/collection-insights/2020/met-api-third-anniversary)

Some background: when we launched the Open Access program in 2017, we released a basic set of metadata under a CC0 license. This includes object information (such as title, maker, culture, medium, date, and dimensions), artist information (such as name, nationality, birth and death year, and their role in creating the object), and links to related public domain images. This information is accessible to interested users via a downloadable .CSV file on our website (more about that later).

Here's a list of some of the new improvements to our Open Access API and CSV that we plan to launch by the end of February 2020:

● We will provide links to Wikidata (a free database containing structured, multilingual data to support Wikipedia and other online resources) for artists and works in the collection, which will help connect our works to their semantic database, allowing users to tap in to their wealth of translated content. 

● The Getty Research Institute maintains structured resources intended to provide a standard, controlled vocabulary for use in the research of art and artists. Our Open Access data now contains links to two resources maintained by The Getty Research Institute: artists link to the Union List of Artist Names (ULAN), and our subject keyword tags link to the Art & Architecture Thesaurus (AAT). The Getty's databases provide standardized, controlled vocabulary for art researchers, thereby making it easier to connect with other datasets using the same standards. 

----

**==> Exemples et définitions, à distiller au long de la formation** (ou pas ? éventuellement une section spécifique ? à voir en fonction de la répartition des exercices)



---
**=> (slide : wikidata, obtenir de l'aide)** M1-110
### Principes de fonctionnement : à l'aide !

Pour obtenir de l'aide 2 solutions : 
* Se rendre au [bistro](https://www.wikidata.org/wiki/Wikidata:Bistro) et regarder les questions posées ou en poser une
* visiter le [portail communautaire](https://www.wikidata.org/wiki/Wikidata:Community_portal/fr)

---
**=> (slide : wikidata, les liens avec d'autres projets de données ouvertes)** M1-110
## Liens avec d'autres projets de données ouvertes

### Faire un lien vers une entité wikidata dans wikipedia

Infobox

L'infobox programmée en Lua avec Wikidata représente la nouvelle génération d'infobox. Le modèle d'infobox ne contient qu'une simple ligne comme {{#invoke:Infobox|build|nom=Cycliste}} et la programmation est contenue dans un module programmé en Lua, qui à vrai dire a la même fonction qu'un modèle. Ce type d'infobox peut prendre une partie plus ou moins grande d'informations sur Wikidata. 



référence directe


---

### Faire un lien vers une entité wikidata dans OSM

Les entitiés Wikidata peuvent être reliées à n'importe quel type d'objet OSM par le biais de la clé **wikidata=** . La clé est constituée par l'identifiant de l'entité sous la forme 'Qnnnnn', par exemple Q936 qui correspond à OpenStreetMap. Les objets ne sont pas liés à une langue mais les clés ne sont pas destinés aux humains contrairement aux autres choses dans OSM.

Il n'existe pas de table d'équivalence entre OSM et Wikidata. Une entité Wikidata peut être utilisée pour décrire plusieurs objets OSM, par exemple: une (boundary=administrative) administration locale, un quariter (place=neighborhood), une ville (place=city), et une île (place=island) peuvent avoir le même nom mais être cartographiée de manière différente dans OSM. Certaines entités Wikidata représentes des entités qui sont cartographiées comme une séries de plusieurs objets dans OSM (par exemple des rues longues, des courants)

Une autre manière de lier des objets est d'utiliser la clé wikipedia=* cependant Wikidata représente les données à un niveau plus atomique à l'instar d'une base de données ce qui correspond plus au modèle de données d'OSM. Toutefois le tag wikipedia=* à l'avantage de produire des références lisibles par les êtres-humains.

Côté wikidata, il est possible d'ajouter une propriété coordonnées géographiques sur une entité (P625)

Cependant il n'est actuellement pas possible d'importer des donnés d'OSM dans wikidata car les licences de réutilisation ne sont pas compatibles (OSM est plus restrictif).



Il est possible d'utiliser l'attribut wikidata pour préciser certains attributs (tags) d'OSM : 

* brand:wikidata=* est utilisée pour préciser la marque d'un magasin par exemple "Burger King" Q177054. Cet attribut peut être utilisé en complément du tag brand=Burger King plus lisible pour les humains.
* operator:wikidata=* similaire à la marque mais pour les les franchises.
* species:wikidata=* est la forme machine-readable de species=* utilisé pour préciser l'espèce d'un arbre.
* network:wikidata=* est la forme machine-readable form de network=* utilisé en combinaison avec les réseaux autoroutiers ou cyclables, ou les systèmes de location de vélo.

#### Exemple de complémentarité

La liste des départements français avec les coordonnées géographiques de leur centroïde qui ont un équivalent dans OSM

```XML
#French départements with Name, Wikidata ID, OSM relation ID, and ISO 3166-2 code
SELECT ?itemLabel ?item ?OSM ?code ?coordo
WHERE
{
	?item wdt:P31 wd:Q6465 . #French départements
	?item wdt:P300 ?code . #with ISO 3166-2 code
    ?item wdt:P625 ?coordo.
	OPTIONAL { ?item wdt:P402 ?OSM }. #OSM relation if available
	SERVICE wikibase:label { bd:serviceParam wikibase:language "fr" }
}

```
Avec le moteur de requête Overpass on peut récupérer la liste des noeuds OSM de type département et le numéro de l'entité wikidata correspondante

```JSON
[out:csv("name","wikidata",::id,"ISO3166-2")];
(
  relation["admin_level"="6"]["wikidata"]["ISO3166-2"~"^FR-..$"];
);
out tags;

```


https://www.wikidata.org/wiki/Wikidata:OpenStreetMap



https://www.youtube.com/watch?v=FkqPLzIUr1k

https://wiki.openstreetmap.org/wiki/Sophox

https://osm.wikidata.link/






