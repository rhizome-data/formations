---
title: Wikidata - Module 2
tags: formation, wikidata, websem

---


# Wikidata - Module 2

## Module 2

**=> (slide : comment contribuer à wikidata ?)** M2-10
### Comment contribuer à wikidata ?

Regarder ce qui est déjà disponible et compléter les pages existantes ou en créer de nouvelles de cette catégorie.

Exercice : trouver les archives départementales décrites dans wikidata

Solution : 

```XML
SELECT ?item ?itemLabel
WHERE {
  ?item wdt:P31 wd:Q2860456  #a pour type archives départementales
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]".
    }              
 }
```

---
## Principes de fonctionnement
### Ajouter des références aux déclarations

Il est intéressant de pouvoir sourcer les déclarations ajoutées à la base de données. Il est possible de faire des référence à des éléments contenus dans Wikipédia [numéro de la propriété ?] ou à des sites externes [numéro de la propriété url de référence]

---

### Contribuer à wikidata
#### Décrire un centre de conservation d'archives

Il existe dans le modèle de données de wikidata plusieurs propriétés associées aux institutions d'archives. D'autres sont en cours de discussion.



| propriétés          | types                                                                             |
|:------------------- |:-------------------------------------------------------------------------------------- |
| nom officiel        |                                                                                        |
| nature de l'élément | archives (Q166118), archives départementales (Q2860456), archives communales (Q604177) |
| dirigeant           |  P1037                                                                                      |
|   adresse                  |                                                                                        |
|    site officiel                 |                                                                                        |
|   jours d'ouverture                  |                                                                                        |
|    courriel                 |                                                                                        |
|            numéro ISNI         |                                                                                        |
|  localisation gographique                   |                                                                                        |

[Liste des propriétés](https://www.wikidata.org/wiki/Wikidata:WikiProject_Archival_Description/Data_structure#Institution)

[La liste affichée sur une carte des archives départementales](https://w.wiki/L9F)

```XML
SELECT ?item ?itemLabel ?dirigeantLabel ?nombreAgentLabel ?siteOfficielLabel ?courrielLabel ?telephoneLabel ?coordo
WHERE
{
  ?item wdt:P31 wd:Q2860456.
  ?item wdt:P856 ?siteOfficiel.
  ?item wdt:P968 ?courriel.
  ?item wdt:P1329 ?telephone.
  ?item wdt:P625 ?coordo.
  OPTIONAL{ ?item wdt:P1037 ?dirigeant.}
  OPTIONAL{ ?item wdt:P1128 ?nombreAgent.}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```

La liste des archives départementales avec le type et le nombre de propriétés renseignées

[requete](https://w.wiki/LPi)

---
### Comment contribuer à wikidata ?
#### Les propriétés associées aux collections d'archives



| Propriété               | types                                                                                                   |               RIC |
|:----------------------- |:------------------------------------------------------------------------------------------------------- | -----------------:|
| titre                   | collection (Q2668072), collection publique de biens culturels (Q2982955), patrimoine culturel (Q210272) |              rico:title |
| nature de l'élément     | P31                                                                                                     |              rico: |
| catalogue               | P972                                                                                                    |              ric:describedBy |
| maintenance assurée par | P126                                                                                                    | rico:heldBy |
| comprend                | P527                                                                                                    |    rico:               |
| décrit à l'URL          | P973                                                                                                    | rico:hasDerivedInstantiation                  |

 :  	

[Liste des propriétés](https://www.wikidata.org/wiki/Wikidata:WikiProject_Archival_Description/Data_structure#Collection)

Actuellement il n'est pas possible d'établir un lien entre les entités institutions de conservation d'archives et leurs collections

[source?°
]

---
### Comment contribuer à wikidata ?
#### Les propriétés associées à la description des fonds

| ISA(G) | Propriétés | Entités |
| -------- | -------- | -------- |
| Référence     | numéro d'inventaire |rico:hasFindingAid|
|Intitulé / analyse |titre(P1476)|rico:title|
|Niveau de description niveau de description (P6224)|Ex : fonds d'archives (Q3052382), série (Q3511132), dossier (Q19515368), pièce (Q11723795)||
|Nom du producteur |producteur d'archives (P6241)|rico:level|
|Modalités d'entrée | 	donné par (P1028)||
|Décrit à l’URL |décrit à l'URL (P973)|rico:hasInstentiation|

[Liste des propriétés](https://www.wikidata.org/wiki/Wikidata:WikiProject_Archival_Description/Data_structure#Fonds_d'archives,_S%C3%A9rie,_dossier,_document)


___
### Comment contribuer à wikidata ?
#### Les propriétés associées à la relation *archivé par* (P485)

La propriété archivé par (P485) est le principal moyen dans Wikidata de faire le lien entre une personne (morale ou physique) voire même un bâtiment (ex : cathédrale de la Sainte-Trinité de Laval (Q2942882)) et l'institution dans laquelle sont déposées ses archives. 

C'est aussi le moyen de faire apparaître les informations de wikidata dans les infobox des Wikipedia. 

La propriété va demander une valeur qui va être alors l'élément représentant l'institution.

Fonds Georges Bureau archivé par Archives départementales de seine-Maritime
[Q3102398](https://www.wikidata.org/wiki/Q3102398) > [P485](https://www.wikidata.org/wiki/Property:P485) > [Q2860472](https://www.wikidata.org/wiki/Q2860472) >> [numéro d'inventaire: FR AD76 354J](http://visualisation.archivesdepartementales76.net/accounts/mnesys_ad76/datas/medias/inventaires/FRAD076_IR_J_271J_Pierre_Nouaud.pdf)


| qualificatifs      |
|:------------------- |
| numéro d'inventaire |
| niveau de description                    |
| s'applique à                     |
| date d'archivage                    |
| titre                    |

[Liste des qualificatifs](https://www.wikidata.org/wiki/Wikidata:WikiProject_Archival_Description/Data_structure#Les_qualificatifs)

# Module 2

## Utiliser Open Refine pour aligner des données avec Wikidata

OpenRefine est un outil libre d'extraction de données qui peut être utilisé pour nettoyer des tableaux, et les connecter à des bases de connaissances, dont Wikidata. 

Dans la terminologie d'OpenRefine, **la réconciliation est le processus d'alignement de données textuelles brutes avec des identifiants de bases de connaissances**. Les fonctionnalités natives de réconciliation d'OpenRefine en font un outil polyvalent pour aligner des données tabulaires à de nombreuses bases de données, dont Wikidata.

### Obtenir des identifiants Wikidata. 

Une fois que vous avez réconcilié une colonne avec Wikidata, vous pouvez obtenir les Qids dans une nouvelle colonne, en utilisant l'opération "Ajouter une colonne à partir de cette colonne", avec l'expression GREL suivante: 

```
cell.recon.match.id
```     

### Extension des données

Cette fonctionnalité est disponible à partir d'OpenRefine 2.8

Une fois qu'une des colonnes de votre tableau est réconciliée à Wikidata, il est possible d'extraire des données de Wikidata pour créer de nouvelles colonnes dans votre jeu de données. Sélectionnez les propriétés qui vous intéressent et OpenRefine insèrera leurs valeurs dans de nouvelles colonnes. Si un item a plusieurs valeurs pour une propriété donnée, ces valeurs seront regroupées au sein d'un même "record" dans OpenRefine: de nouvelles lignes seront créées pour les insérer. Le "record mode" d'OpenRefine peut être plus adapté pour transformer ces valeurs a posteriori. 

### Import de données dans Wikidata

Cette fonctionnalité est disponible à partir d'OpenRefine 3.0

OpenRefine permet de transformer des données tabulaires en modifications sur Wikidata. La transformation est régie par un « schéma » − un patron de modification Wikidata qui est appliqué à chaque ligne de votre tableau. Une fois que vous avez créé un schéma, vous pouvez :

* prévisualiser les modifications sir Wikidata et les inspecter manuellement;
* analyser et résoudre les problèmes de qualité détectées automatiquement par l'outil;
* téléverser vos modifications dans Wikidata en vous connectant avec votre propre compte;
* exporter les modifications au format QuickStatements.

https://www.youtube.com/watch?v=0tQPmfb6IFk&list=PL_0jeq3PjvtADzbovAgHNzOFvOlyF6uL1&index=2
https://www.youtube.com/watch?v=q8ffvdeyuNQ&list=PL_0jeq3PjvtADzbovAgHNzOFvOlyF6uL1&index=1

1. On sélectionne un jeu de données contenant des données à enrichir
2. On choisit une colonne à partir de laquelle on souhaite effectuer la réconciliation.
3. On utilise le service de réconciliation par défaut (en) ou on ajoute le service en langue française : https://tools.wmflabs.org/openrefine-wikidata/fr/api.
4. Automatiquement la fonction propse une liste de types avec lequelles les valeurs contenues dans cette colonne peuvent être réconciliées.
5. plusieurs paramétres peuvent être ajustées et d'autres colonnes peuvent être utilisées afin d'améliorer la qualité de la réconciliation
6. Après avoir effectué les recherches, Open Refine propose une liste de correspondances qui peuvent être consultées, choisies ou rejetées. Plusieurs options d'algorithmes de correspondances peuvent être utilisées.
7. De nouvelles colonnes peuvent être ajoutées au jeu de données à partir des déclarations associées aux page des entités avec lesquelles s'effectue la réconciliation
8. Des informations du jeu de données peuvent être ajoutées à wikidata sous la forme de Quick statements.
9. Pour cela il faut définir le schéma d'import en sélectionnant pour l'entité que l'on souhaite créer ou enrichir les déclarations que l'on souhaite renseigner avec les valeurs présentes dans notre jeu de données
10. L'onglet prévisualisation nous permet de voir le résultat de l'import avant de le faire
11. l'onglet problèmes nous informe sur les problèmes potentiels détecté dans notre schéma d'import
12. Il est utile de créer dans le jeu de données des colonnes correspondant aux déclarations que l'on souhaite importer afin de vérifier que ces déclarations n'existent pas déjà (sinon elles généreront des doublons). pour cela je créé une colonne à partir de la valeur initialement réconciliée pour la propriété que je souhaite enrichir avec mes données. Ensuite je créé une facette personnalisée par valeur nulle. Le résultat m'affiche les lignes qui n'ont pas encore de valeur renseignée. Comme cela je suis sûr que je ne vais importer que des déclarations qui n'existent pas encore.
13. Pour importer dans wikidata il faut avoir un compte utilisateur
14. Après l'import on peut vérifier dans l'onglet contributions que nos propositions d'enrichissements ont bien été intégrées.

Exercice : enrichir les pages des entités archives sur wikidata
Sources :
* l'annuaire France Archives 
* [les données du rapport d'activité du réseaux des archives](https://francearchives.fr/fr/article/37978) 


---
varia:outils

https://tools.wmflabs.org/mix-n-match

---
vario:webographie
https://en.wikibooks.org/wiki/SPARQL
https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial




