---
title: Wikidata - Module 3 Usages avancés SPARQL et wikidata
tags: formation, wikidata, websem

---



# Wikidata - Module 3 Usages avancés SPARQL et wikidata

---
**=> (slide : wikidataet RDF introduction)** M3-10

### Wikidata et RDF

Wikidata utilise le modèle de structuration de données RDF pour décrire les relations entre les entités et les propriétés qui les caractérisent.

**RDF** est un modèle de structuration de données permettant d'écrire des énoncés sur un sujet et de mettre en relation ces déclarations grâce à des identifiants.

Pour interroger des données en RDF il existe un language dédié permettant d'écrire des requêtes : **SparQL**. 

Les données sont rendues disponibles via une interface interactive appellée End Point qui permet d'effectuer des recherches programmatiquement ou de manière assistée

Dans wikidata, les résultats des requêtes peuvent être récupérés sous forme de liste grâce à un moteur de requête très puissant **query.wikidata.org**

---
**=> (slide : Comment formuler une requête)** M3-20

## Utilisation du language de requêtes SparQL

### Description d'une requête

Demander des exemples de requête et en écrire quelques unes sur un post-it

[exemple : rechercher des livres sur l'Histoire décrits dans wikidata]
[exemple : Trouver tous les enfants de Jean-Sebastien Bach décrits dans wikidata]

Pour écrir des requêtes il faut commencer par apprendre à écrire ces requêtes en pseudo-language : 

Rechercher des livres sur l'histoire se dit "trouver les entités de wikidata qui sont des instances du type livre et dont le genre est histoire"

"(instance of: book) et (genre: history)"

Selectionne ?livre
où { 
    ?livre a-pour-type livre
    ?livre a-pour-genre histoire
}

**Pour des triplets WDQS de base, les éléments doivent être préfixés avec wd: et les propriétés avec wdt:**

```XML
SELECT ?livre WHERE {
  ?livre wdt:P31 wd:Q571.
  ?livre wdt:P921 wd:Q309.
}
```

Par exemple rechercher les enfant de Jean-Sebastien Bach se dit


```
SELECT ?enfant ?enfantLabel
WHERE
{
# ?enfant qui ont pour père l'élément Jean-Sebastien Bach
  ?enfant wdt:P22 wd:Q1339.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
}
```


[Essayez!](https://query.wikidata.org)

> vous pouvez appuyer sur Ctrl+Espace à n'importe quel point de la question et avoir des suggestions de code qui peuvent être appropriées

Rechercher les enfants de Bach et Maria dont l'occupation était la composition ou le piano avec leurs dates de naissance et de mort.

```XML
SELECT ?enfant ?enfantLabel ?Date_naissance ?Date_mort
WHERE
{
  ?enfant wdt:P22 wd:Q1339;
  # les enfants de Jean Sébastien Bach
         wdt:P25 wd:Q57487;
  # qui sont aussi enfants de Maria Bach       
         wdt:P106 wd:Q36834,
  # et dont l'occupation était compositeur       
                  wd:Q486748.
  # ou pianiste                
  ?enfant wdt:P569 ?Date_naissance.
  # pour lesquels une date de naissance est renseignée
  ?enfant wdt:P570 ?Date_mort.
  # et une date de mort
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
}
```

---
**=> (slide : Le point d'accès à la base de données de wikidata)** M3-30
### Principes de fonctionnement : recherche par requêtes

La puissance de l'outil c'est la possibilité d'interroger la base de données en y effectuant des requêtes.

Pour cela un outil est mis à disposition : le [point d'accès SparQL](https://query.wikidata.org/)

Il permet d'écrire des recherches dans un language de requête et d'obtenir des résultats.

Plusieurs types de restitution sont disponibles en fonction des données interrogées.

Le requêteur fourni par wikidata propose différentes formes de restitution pour les résultats des requêtes : 

* Table : valable pour tout type de requêtes
* Line chart, Bar chart, tree, area Chart, bubble chart : pour les résultats contenant des données chiffrées
* Map : pour les résultats contenant des données géolocalisées

Exemple : 

Localisation de tous les musées parisiens (185) disposant d'une page dans wikidata


```XML

# Tous les musées de Paris avec leur localisation
# Tous les musées et les sous-classes de muséesà Paris avec leurs coordonnées et leur typologie
#defaultView:Map
SELECT DISTINCT ?name ?natureLabel ?nature ?item ?coord ?lat ?lon
WHERE
{
 hint:Query hint:optimizer "None" .
 ?item wdt:P131* wd:Q90 .
 # les entités dont la localisation administrative est Paris
 ?item wdt:P31/wdt:P279* wd:Q33506 .
 ?item wdt:P31 ?nature . 
 # qui sont des instances ous des sous-classes de musée 
 ?item wdt:P625 ?coord . 
 # qui ont des coordonnées géographiques
 ?item p:P625 ?coordinate . 
 # qui ont des coordonnées géographiques
 ?coordinate psv:P625 ?coordinate_node . 
 # dont les coordonnées ont un emsemble de propriétés géographiques
 ?coordinate_node wikibase:geoLatitude ?lat . 
 # de type latitude
 ?coordinate_node wikibase:geoLongitude ?lon . 
 # de type logitude
 SERVICE wikibase:label {
 bd:serviceParam wikibase:language "fr" .
 ?item rdfs:label ?name . 
 ?nature rdfs:label ?natureLabel
 }
}
ORDER BY ASC (?name)
# classés par nom
```

[Voir le résultat](https://w.wiki/KiC)

> note : le commentaire ##defaultView:Map permet de déclencher automatiquement l'affichage des résultats sous forme de carte.

Il propose également un panneau d'aide pour faciliter l'écriture des requêtes, des exemples de requêtes et des liens pour partager les requêtes ou leurs résultats

---


#### Rechercher la liste des entités d'un même type

En utilisant la propriété "est une instance de" et l'identifiant du type d'instance recherché ont peut ontenir une liste de résultats similaires.

Pour récupérer la liste des entités qui sont des instance de l'entité Archives départementales ont peut donc écrire cette requête

```XML

SELECT ?item                      # Liste de l'ensemble des informations à récuperer et à lister
WHERE                             # Mot clef qui indique la déclaration des contraintes
{
  ?item wdt:P31 wd:Q2860456.      # Chaque contrainte est explicitée sous la forme d'un triplet
}

```

---
**=> (slide : interroger des classes ou des sous-classes d'entités)** M3-40
#### Faire des requêtes dans wikidata

Classes et instances

il y a deux propriétés pour "est" dans Wikidata : nature de l'élément (P31) et sous-classe de (P279). *Autant en emporte le vent* est une instance particulière de la classe "film"; la classe "film" est une sous-classe (une classe plus spécifique; une spécialisation) de la classe plus générale "œuvre d'art". 

wdt:P31/wdt:P279*

---
**=> (slide : structure générale d'une requête RDF)** M3-50

#### Structure générale



| Mot clé SParQL | Fonction                                                                                                                                                                                                                             |                      Syntaxe |
|:-------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :----------------------------:|
| SELECT         | Elément initiateur d'une requête, préfixe la déclaration des variables à récupérer sous forme de tableau à la fin de la requête.                                                                                                     | SELECT [variables] ?variable |
| WHERE          | Clôt la définition des variables, initie la description des contraintes à appliquer sur la requête. Est toujours suivi d'accolades pour permettre l'énoncé des contraintes sous forme de triplets, avec un unique triplet par ligne. |      WHERE { [contraintes] } |

---
**=> (slide : Variables)** M3-60
### Variables

Une variable est constituée d'un mot quelconque préfixé d'un point d'interrogation.

Les variables sont définies lors de l'établissement des contraintes.

Elles servent de point d'ancrage pour pouvoir chaîner les contraintes, ainsi que de repêre pour le choix des informations à récuperer lors de la requête.

Une attention particulière doit être portée au nommage des variables pour permettre une compréhension et une relecture de la requête la plus simple possible.

Exemple : ?nom ?nombreAgents ?dateFin ?molécule

---
**=> (slide : Définition des triplets)** M3-70
### Définition des triplets

Les contraintes sur les requêtes s'expriment sous la forme de triplets **SUJET** | **PREDICAT** | **OBJET**

Ce triplet doit toujours être suivi d'un point.

> Si on souhaite combiner 2 contraintes sur un même sujet on peut les relier par un point virgule ce qui dispense de répéter le sujet.

=> (slide : Définition des triplets: explication littérale) M3-70
On peut se les représenter sous forme littérale:
"Je cherche les **SUJET**s qui pour le **PREDICAT** ont pour valeur l'**OBJET**"

Exemple pour récuperer les services d'archives:
"Je cherche les **entités** qui pour la propriété **instance_of(appartenance)** ont pour valeur **archives départementales**"

Traduit en SparQL:
```XML
?item wdt:P31 wd:Q2860456.
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%7D)

**=> (slide : Définition des triplets: Manipulation des entitées)** M3-80
On remarque que l'on récupère directement les entités de wikidata et non pas de valeurs brutes, comme le label de l'entité.

Le premier sujet doit nécessairement être une variable.

Dans cet exemple, on récupère des sujets. 

Si l'on veut plus d'information sur ces sujets, on peut maintenant récupérer les objets qui lui sont liés via des prédicats (propriétés).

---
**=> (slide : Définition des triplets: Manipulation des entitées: Exemple 1)** M3-90
Exemple: Trouver tous les éléments connus de Wikidata qui sont des "archives départementales"

La requête de base s'écrit sous cette forme : 
```XML

SELECT ?item ?itemLabel 
WHERE 
{
  ?item wdt:P31 wd:Q2860456. #toutes les variables item qui ont pour propriété d'ête une instance de l'élément "archives départementales"
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],fr". }
}

```

---
### Définition des triplets: Manipulation des entitées

**=> (slide : Définition des triplets: Manipulation des entitées: Exemple 2)** M3-100
Disons que l'on veut récupérer tous les services d'archives ainsi que l'addresse de leur site web.

Les contraintes seraient sous cette forme:
```XML
?item wdt:P31 wd:Q2860456. #toutes les variables item qui ont pour propriété d'ête une instance de l'élément "archives départementales"
?item wdt:P856 ?website. # et qui ont une propriété site officiel (P856) que l'on stocke dans la variable ?website

```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3Fwebsite%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20wdt%3AP856%20%3Fwebsite.%0A%7D)

---

### Définition des triplets: Manipulation des entitées: Subtilités

**=> (slide : Définition des triplets: Manipulation des entitées: Subtilités)** M3-110
Attention: L'interprétation littérale change pour la seconde ligne étant donné que la variable que l'on récupère est l'objet et non plus le sujet.

Dans ce cas, la seconde contrainte s'exprime sous la forme:
"Pour chaque **entité** définie, chercher la valeur de sa propriété **official_website** et la stocker dans la nouvelle **variable** ?website"

Ici la valeur récupérée est directement la valeur recherchée: l'url du site web. Ce n'est pas toujours le cas.

---
### Définition des triplets: Manipulation des entitées

**=> (slide : Définition des triplets: Manipulation des entitées: Exemple 3)** M3-120
Par exemple si l'on voulait aussi récupérer le nom du territoire administratif, on serait tenté de rédiger la requête:

```XML
?item wdt:P31 wd:Q2860456. #les instances de type Archives départementales
?item wdt:P856 ?website. # qui ont une proprité ste officiel
?item wdt:P131 ?territory. # et une propriété localisation administrative
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3Fwebsite%20%3Fterritory%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20wdt%3AP856%20%3Fwebsite.%0A%20%20%3Fitem%20wdt%3AP131%20%3Fterritory.%0A%7D)

---

### Définition des triplets: Manipulation des entitées: Différence valeur/entités

**=> (slide : Définition des triplets: Manipulation des entitées: Différence valeur/entités)** M3-130
On constate que la nouvelle variable contient l'identifiant d'une entité wikidata et non pas le nom du territoire. Pour récuperer cette valeur, il va falloir descendre au niveau inférieur de la structure de données de wikidata.

Maintenant que nous avons accès a l'entitée, on peut écrire un nouveau triplet qui permet de récuperer le nom du territoire. 


```XML
?item wdt:P31 wd:Q2860456.
?item wdt:P856 ?website.
?item wdt:P131 ?territory.
?territory wdt:P1448 ?territory_name.
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3Fwebsite%20%3Fterritory%20%3Fterritory_name%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20wdt%3AP856%20%3Fwebsite.%0A%20%20%3Fitem%20wdt%3AP131%20%3Fterritory.%0A%20%20%3Fterritory%20wdt%3AP1448%20%3Fterritory_name.%0A%7D)

---
### Définition des triplets: Manipulation des entitées: Label: Duplication de lignes

**=> (slide : Définition des triplets: Manipulation des entitées: Label: Duplication de lignes 1)** M3-140
On peut remarquer en classant les entrées par identifiants que les archives de la Marne occupent maintenant deux lignes dans le tableau. Cela est dû au fait que ce service d'archives est situé sur deux territoires administratifs. C'est le cas également pour les archives de la Corse et celles d'Aude qui ont des déclarations dans plusieurs langues pour la propriété recherchée (localisation administrative) 

La contrainte sur le territoire va donc avoir pour résultat de nous retourner deux entités wikidata qui vont chacune occuper leur propre ligne dans le tableau.

---
### Définition des triplets: Manipulation des entitées: Label: Duplication de lignes

**=> (slide : Définition des triplets: Manipulation des entitées: Label: Duplication de lignes 2)** M3-150
Ce genre de comportement bien pratique peut parfois être gênant. Un exemple avec la requête suivante :

```XML
?item wdt:P31 wd:Q2860456.
?item rdfs:label ?label.
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3Flabel%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20rdfs%3Alabel%20%3Flabel.%0A%7D)

Le résultat présente des doublons pour chaque traduction de libellé que possède wikidata. Ces lignes supplémentaires n'apportent aucune nouvelles informations, et celles-ci necessitent d'être filtrées.

**/// A revoir**
Au contraire cette [requête](https://w.wiki/Lfp) renvoie 99 centres d'archives départementales, l'URL de leur site officiel, leur localisation administrative, leur numéro ISNI, leur numéro de téléphone et leur coordonnées géographiques. 

Comme l'on recherche les entités pour lesquelles toutes ces propriétés sont vraies (une déclaration existe dans chacune des instances de l'élément archives départementales), celles pour lesquelles au moins une des déclarations n'est pas présente ne sont pas retournées dans la liste de résultats.

**(Tu peux voir dans ce tableau qu'au moins une entité (wd:Q2406894) est présente dans deux lignes. Le fait d'être strict sur les contraintes n'empêche pas la duplication dans les résultats. Ca permet seulement de ne pas autoriser les lignes avec des valeurs manquantes : Ok)**


[La bonne requête avec les options qui renvoie 102 résultats](https://w.wiki/K$S)

Maintenant avec cette liste de résultats on veut afficher sur une carte la liste des archives départementales 

(POURQUOI SEULEMENT 100 RESULTATS ???) => A cause du `GROUP BY` sur les services d'archives. 2 services d'archives étaient dupliqués dans les résultats a cause de résultats multiples sur certaines propriétés. Par ex: *archives départementales de la Marne* qui est présente dans 2 lignes pour pouvoir afficher les 2 localisations d'administration. OK

``` XML

# Afficher sur une carte 100 centres d'archives départementaux


#defaultView:Map
SELECT ?item ?itemLabel (SAMPLE(?natureLabel) as ?sampleNL) (SAMPLE(?coord) as ?sampleCoord) (SAMPLE(?lat) as ?slat) (SAMPLE(?lon) as ?slon)
WHERE
{
 hint:Query hint:optimizer "None" .
 # les entités dont le type est archives départementales
 ?item wdt:P31 wd:Q2860456 .
 # qui sont des instances ou des sous-classes de musée
 # si je rajoute cette condition les résultats ne sont plus distincts
 ?item wdt:P31 ?nature .
 # récupère le label de manière explicite pour pouvoir le manipuler
 ?nature rdfs:label ?natureLabel.
 FILTER (langMatches( lang(?natureLabel), "fr" ) ).
 ?item wdt:P625 ?coord .
 # qui ont des coordonnées a
 ?item p:P625 ?coordinate .
 # qui ont des coordonnées géographiques
 ?coordinate psv:P625 ?coordinate_node .
 # dont les coordonnées ont un emsemble de propriétés géographiques
 ?coordinate_node wikibase:geoLatitude ?lat .
 # de type latitude
 ?coordinate_node wikibase:geoLongitude ?lon .
 # de type logitude
 SERVICE wikibase:label {bd:serviceParam wikibase:language "fr" .}
}
GROUP BY ?item ?itemLabel
ORDER BY ASC (?itemLabel)
# classés par nom

```
---
**///**
**=> (slide : Définition des triplets: Filtres: Operateurs)** M3-160
## Filtres

### Opérateurs


| Symboles                  |                    Types | Exemples                                                                                          | Fonction                                                                    |
|:------------------------- | ------------------------:| ------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------- |
| +, -, >, <, >=, <=, `, !` | Opérateurs mathématiques | ?nombre1 + ?nombre2 < 5 / "abd" > "abc" / "2016-01-01"^^xsd:dateTime > "2015-12-31"^^xsd:dateTime | Comparer des types *comparables* pour obtenir une réponse du type VRAI/FAUX |     |
| &&(ET), \|\|(OU)          |      Opérateurs logiques | ?date >= "2016-01-01"^^xsd:dateTime && ?date <= "2016-12-31"^^xsd:dateTime                        | Combiner les résultats des comparaisons                                     |     |
|      [not]in                     |   Opérateur d'appartenance                       |  ?prenom in ("Pierre", "Paul", "Jacque") / ?pays not in ("France", "Allemagne")                                                                                                 |  Permet de filtrer spécifiquement sur certaines valeurs                                                                           |

> A noter que dans l'exemple de comparaison de dates, la valeur littérale de la date doit être *convertie* en valeur de date brute via l'opérateur **^^** suivie du type de donnée de destination. 

L'utilisation de certaines fonctions est aussi possible et parfois nécessaire pour certaines comparaisons.

Par exemple:
```XML
FILTER(STRSTARTS(?label, "Mr. ")).
```

### Utilisations de `FILTER`


| Mot clé SPARQL | Fonction | Syntaxe    |
|:-------------- | -------- | --- |
| FILTER|Contrainte permettant de restreindre les résultats de recherche     | FILTER(COMPARAISON)|

Une condition peut utiliser un opérateur ou des combinaisons d'opérateurs. Celle-ci doit avoir une structure qui permette une réponse franche binaire sous la forme OUI/NON pour chacun des élements de l'ensemble d'entités que l'on veut filtrer.

On a maintenant toutes les cartes en main pour pouvoir récuperer uniquement les noms des services d'archives en français (c'est-à-dire de contraindre la liste des résultats aux labels en français).

```XML
SELECT ?item ?label
WHERE
{
  ?item wdt:P31 wd:Q2860456.
  ?item rdfs:label ?label.
  FILTER (langMatches( lang(?label), "fr" ) )
}
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3Flabel%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20rdfs%3Alabel%20%3Flabel.%0A%20%20FILTER%20%28langMatches%28%20lang%28%3Flabel%29%2C%20%22fr%22%20%29%20%29%0A%7D)

Cette requête fonctionne, mais la structure de wikidata ne nous facilite pas la tâche. Heureusement on peut obtenir le même résultat beaucoup plus simplement en utilisant un service fourni par Wikidata.

```XML
SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
```

Ajouter cette ligne dans nos contraintes va permettre de créer automatiquement, pour chaque variable "x" représentant une entité Wikidata, une variable "xLabel" qui contiendra automatiquement le libellé de l'entité dans le language spécifié.

La nouvelle requête se présente donc sous cette forme:

```XML
SELECT ?item ?itemLabel
WHERE
{
  ?item wdt:P31 wd:Q2860456.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3FitemLabel%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D)

La commodité de cette ligne ne saute pas aux yeux, son avantage commence à être clair quand on veut récupérer les libellés de plusieurs entités et que ces libellés sont disponibles dans plusieurs langues.

Disons que l'on veuille récupérer tous les services d'archives, et afficher leurs libellés en français.
En utilisant les filtres, ça donnerait une requête du style:

```XML
SELECT ?item ?itemLabel ?countryLabel
WHERE
{
  ?item wdt:P31 wd:Q2860456.
  ?item rdfs:label ?itemLabel.
  ?item wdt:P17 ?country.
  ?country rdfs:label ?countryLabel
  FILTER (langMatches( lang(?itemLabel), "fr" ) ).
  FILTER (langMatches( lang(?countryLabel), "fr" ) )
}
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3FitemLabel%20%3FcountryLabel%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20rdfs%3Alabel%20%3FitemLabel.%0A%20%20%3Fitem%20wdt%3AP17%20%3Fcountry.%0A%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryLabel%0A%20%20FILTER%20%28langMatches%28%20lang%28%3FitemLabel%29%2C%20%22fr%22%20%29%20%29.%0A%20%20FILTER%20%28langMatches%28%20lang%28%3FcountryLabel%29%2C%20%22fr%22%20%29%20%29%0A%7D)

Maintenant, grâce au service de wikidata, il suffit d'écrire:

```XML
SELECT ?item ?itemLabel ?countryLabel
WHERE
{
  ?item wdt:P31 wd:Q2860456.
  ?item wdt:P17 ?country.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3FitemLabel%20%3FcountryLabel%0AWHERE%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456.%0A%20%20%3Fitem%20wdt%3AP17%20%3Fcountry.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D)

Pratique non ?

Attention ce n'est valable que pour les traductions des libellés des entités.

D'une manière plus générale, on retrouve cette structure:
```XML
SELECT ?x ?xLabel ?y ?yLabel... 
WHERE
{
  ?x propriété entité.
  ?x propriété y?.
  # ou encore
  # ?y propriété entité.
  # ...
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```

`FILTER` possède une certaine souplesse dans sa structure, ce qui permet d'ajuster le niveau de précision et de manipulation pour filtrer les données.

Par exemple:
```XML
SELECT DISTINCT ?genre ?genreLabel
WHERE {
  ?genre wdt:P279 wd:Q6097 .
  ?genre rdfs:label ?genreLabel.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  FILTER (lang(?genreLabel) = "fr")
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3FitemLabel%20%3Fmother%20%3FmotherLabel%20WHERE%20%7B%0A%20%20VALUES%20%3Fitem%20%7B%20wd%3AQ937%20wd%3AQ1339%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP25%20%3Fmother.%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%7D)

Cette requête nous retourne tout les types de thé sur wikidata qui ont un label en français.

Pour récupérer les types de thé qui n'ont **pas** de traductions en français, on serait tenté de juste inverser la condition:

```XML
SELECT DISTINCT ?genre ?genreLabel
WHERE {
  ?genre wdt:P279 wd:Q6097 .
  ?genre rdfs:label ?genreLabel.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  FILTER (lang(?genreLabel) != "fr")
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fgenre%20%3FgenreLabel%0AWHERE%20%7B%0A%20%20%3Fgenre%20wdt%3AP279%20wd%3AQ6097%20.%0A%20%20%3Fgenre%20rdfs%3Alabel%20%3FgenreLabel.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%20%20FILTER%20%28lang%28%3FgenreLabel%29%20%21%3D%20%22fr%22%29%0A%7D)

Mais on se rends compte que le nombre d'entitées résultante est beaucoup trop important pour que le résultat de notre requête soit cohérent.

En effet, la condition `?genre rdfs:label ?genreLabel.` va récupérer tout les labels pour tout les types de thés. Du coup on aura des lignes multiples pour chaque traduction de type de thé, et la clause `FILTER (lang(?genreLabel) != "fr")` ne va faire que filtrer les lignes dans un autre langage que le français.

Pour avoir un résultat cohérent, il est nécessaire que la récupération des traductions se fasse dans la clause d'un filtre.

On peut se représenter les filtres comme une opération qui va être vérifiée pour **chaque** entitée décrite en dehors du filtre et dont les conditions du filtre font références.

Cela va permettre de les sélectionner individuellement. Les labels ne sont plus récupérés automatiquement pour **toutes** les entitées, mais au contraire ces valeurs vont être récupérées pour chacune d'entre elles **individuellement** et permettre une plus grande libertée dans la construction des contraintes.

```XML
SELECT DISTINCT ?genre ?genreLabel
WHERE {
  ?genre wdt:P279 wd:Q6097 .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  FILTER NOT EXISTS {
    ?genre rdfs:label ?genreLabel
    FILTER (lang(?genreLabel) = "fr")
  }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fgenre%20%3FgenreLabel%0AWHERE%20%7B%0A%20%20%3Fgenre%20wdt%3AP279%20wd%3AQ6097%20.%0A%20%20%3Fgenre%20rdfs%3Alabel%20%3FgenreLabel.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%20%20FILTER%20%28lang%28%3FgenreLabel%29%20%21%3D%20%22fr%22%29%0A%7D)

On arrive donc à cette requête, dont le filtre peut être interprété de la manière suivante:
"Pour **chaque** entité décrite, récuperer **toutes** les traductions de **cette** entitée en français (`FILTER (lang(?genreLabel) = "fr")`), garder cette entitée si il n'existe pas de résultat pour ces conditions (`NOT EXISTS`)"

Mise en pratique:

1) **Utiliser les filtres**: Ecrire une requête SparQL qui retourne toutes les personnes qui ont reçu un prix nobel de la paix et qui sont nés après le premier Janvier 1990.

Indice: Partir de l'entitée "Prix Nobel de la Paix"(Q35637) permet de récuperer toutes les entités qui se sont vu attribuer ce prix.

[Solution](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FbirthDate%0AWHERE%0A%7B%0A%20%20wd%3AQ35637%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20%3Fwinner%20wdt%3AP569%20%3FbirthDate.%0A%20%20FILTER%28%3FbirthDate%20%3E%20%221990-01-01%22%5E%5Exsd%3AdateTime%29%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D)

Explication

selectionne dans la variable ?winner 
* les entités qui ont comme propriété vainqueur (P1346) de l'entité prix nobel de la paix (wd:Q35637)
* et qui sont des entités de type "être humain (P5)"
* et dont la date de naissance est supérieure à 1990 FILTER(?birthDate > "1990-01-01"^^xsd:dateTime)



2) **Manipuler une structure à plusieurs niveaux**: Récupérer maintenant toutes les personnes ayant reçu un prix nobel, quelle que soit sa catégorie, et afficher leur libellé et le libellé de la catégorie qu'elles ont remportée.

Indice: En consultant l'entité "Prix Nobel de la Paix"(Q35637) on peut voir que celle-ci est une sous-classe de l'entité plus générale "Prix Nobel"(Q7191).

[Solution](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D)

Explication

SELECT ?winner ?winnerLabel ?categorieLabel
WHERE
{
  ?categorie wdt:P279 wd:Q7191. #les entités qui sont des sous-classe de l'entité Prix nobel
  ?categorie wdt:P1346 ?winner. # stocke les dans la variable ?winner qui a comme propriété vainqueur d'une sous-classe de prix nobel
  ?winner wdt:P31 wd:Q5. # et qui appartiennent à l'entité "être humain"
  
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }



---
### Formater les données

Il existe differentes manière en SparQL de formater le résultat d'une requête, que ce soit pour la trier, ou ne récupérer qu'une sous-partie du résultat.




| Mot clé SparQL | Fonction                                                                                                                                         | Syntaxe                                                 |
| -------------- |:------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------- |
| ORDER BY       | Permet de trier les résultats de la requête en fonction d'une colonne                                                                            | {CONTRAINTE} ORDER BY (ASC/DESC)(colonne)               |
| LIMIT          | Permet de fixer le nombre maximum de résultats pour la requête                                                                                   | {CONTRAINTE} ORDER BY X LIMIT nombre                    |
| OFFSET         | Permet de sauter les premiers résultats de la recherche à partir du nombre fixé                                                                  | {CONTRAINTE} ORDER BY X OFFSET nombre                   |
| OPTIONAL       | Permet de récupérer une information sur les élements qui le permettent tout en conservant les résultats qui ne contiennent pas cette information | SELECT ?var WHERE {OPTIONAL { ?var property element. }} |
| BIND           | Permet de lier le résultat d'une expression à une variable pour s'y référer                                                                      | BIND(?deathDate - ?bornDate as ?ageInDays)              |
| BOUND          | vérifier si une variable est définie pour un élément en particulier                                                                              | FILTER (!BOUND(?deathDate))                             |
| VALUES         | Permet de selectionner plusieurs entitées ou valeurs à lier avec une même variable                                                               | VALUES ?meal {"lunch" "dinner"} / VALUES (?var1 ?var2) {value1 value2} |


**Syntaxe ORDER BY**

Les mots-clefs `ASC`/`DESC` sont à utiliser comme des fonctions sur la colonne spécifiée pour `ORDER BY`

Ils permettent de déterminer si l'on veut un tri respectivement croissant ou décroissant.

exemple : ORDER BY ASC(?variable)

**Syntaxe OFFSET et LIMIT**

L'ordre des clauses `OFFSET` et `LIMIT` peut être interchangé mais elle doivent se trouver tout à la fin de la requête.

exemples : 
OFFSET 50
LIMIT 10

Exemple d'utilisation:

```XML
SELECT ?winner ?winnerLabel ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
ORDER BY ASC(?winnerLabel)
OFFSET 50
LIMIT 10
```
[tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AORDER%20BY%20ASC%28%3FwinnerLabel%29%0AOFFSET%2050%0ALIMIT%2010)

**Syntaxe OPTIONAL**

Le mot-clef `OPTIONAL` permet de récupérer une propriété sans limiter les résultats aux entrées qui possèdent cette propriété.

Le triplet qui exprime la contrainte est entouré par des accolades et se termine par un point

exemple : la variable vainqueur à une date de mort 
OPTIONAL{?winner wdt:P570 ?deathDate.}


```XML
SELECT ?winner ?winnerLabel ?birthDate ?deathDate ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  ?winner wdt:P569 ?birthDate.
  OPTIONAL{?winner wdt:P570 ?deathDate.}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
ORDER BY DESC(?birthDate)
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FbirthDate%20%3FdeathDate%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20%3Fwinner%20wdt%3AP569%20%3FbirthDate.%0A%20%20OPTIONAL%7B%3Fwinner%20wdt%3AP570%20%3FdeathDate.%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AORDER%20BY%20DESC%28%3FbirthDate%29)

Attention, si plusieurs valeurs peuvent être absentes, il est nécessaires de les récupérer dans des clauses `OPTIONAL` différentes.

Par exemple cette requête:

```XML
SELECT ?winner ?winnerLabel ?birthDate ?deathDate ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  OPTIONAL
  {
    ?winner wdt:P569 ?birthDate.
    ?winner wdt:P570 ?deathDate.
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
ORDER BY DESC(?birthDate)
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FbirthDate%20%3FdeathDate%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20OPTIONAL%0A%20%20%7B%0A%20%20%20%20%3Fwinner%20wdt%3AP569%20%3FbirthDate.%0A%20%20%20%20%3Fwinner%20wdt%3AP570%20%3FdeathDate.%0A%20%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AORDER%20BY%20DESC%28%3FbirthDate%29)

va récupérer soit les entités qui n'ont aucune date de naissance et de mort définie, soit celles qui ont ces deux dates définies. Alors que la requête:

```XML
SELECT ?winner ?winnerLabel ?birthDate ?deathDate ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  OPTIONAL{?winner wdt:P569 ?birthDate.}
  OPTIONAL{?winner wdt:P570 ?deathDate.}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
ORDER BY DESC(?birthDate)
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FbirthDate%20%3FdeathDate%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20OPTIONAL%7B%3Fwinner%20wdt%3AP569%20%3FbirthDate.%7D%0A%20%20OPTIONAL%7B%3Fwinner%20wdt%3AP570%20%3FdeathDate.%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AORDER%20BY%20DESC%28%3FbirthDate%29)

va permettre de récuperer toutes les entités, qu'elles aient ou pas une date de naissance ou de mort renseignée.

**Syntaxe de BIND et BOUND**

Le mot clef `BIND` permet de créer une nouvelle variable à partir d'une expression. Ca peut être pratique pour renommer une colonne, ou récupérer et afficher le résultat d'une opération sur les données. 

`BOUND` quant-à lui permet de vérifier si une variable est définie pour un élément en particulier. C'est très pratique pour filtrer les données.

Par exemple:

```XML
SELECT ?winner ?winnerLabel ?birthDate ?winnerAge ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  ?winner wdt:P569 ?birthDate.
  OPTIONAL{?winner wdt:P570 ?deathDate.} #optionnellement ceux qui ont une date de mort
  BIND(YEAR(NOW()) - YEAR(?birthDate) AS ?winnerAge) (stocke dans la variable ?winnerAge le résultat de la différence entre l'année actuelle et l'année de leur naissance)
  FILTER (!BOUND(?deathDate)) #qui n'ont pas de date de mort (qui sont encore vivante ?)
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
ORDER BY DESC(?winnerAge)
```

[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3FbirthDate%20%3FwinnerAge%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20%3Fwinner%20wdt%3AP569%20%3FbirthDate.%0A%20%20OPTIONAL%7B%3Fwinner%20wdt%3AP570%20%3FdeathDate.%7D%0A%20%20BIND%28YEAR%28NOW%28%29%29%20-%20YEAR%28%3FbirthDate%29%20AS%20%3FwinnerAge%29%0A%20%20FILTER%20%28%21BOUND%28%3FdeathDate%29%29%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AORDER%20BY%20DESC%28%3FwinnerAge%29)

Cette requête récupère toutes les personnes ayant remporté un prix Nobel et étant encore vivantes, et les trie selon leur âge, des plus agées aux plus jeunes.

Remarquez de quelle manière `OPTIONAL` et `BOUND` ont été utilisés pour filtrer les résultats de recherche, et comment les valeurs de la colonne `?winnerAge` ont été crées via l'utilisation de `BIND`.

Enfin, l'utilisation du mot clef `VALUES` permet de fixer la valeur d'une ou plusieurs variables a une ou plusieurs valeurs.

Par exemple, avec une requête qui retournerait le résultat suivant:

| description | date         | amount |
| ----------- | ------------ | ------ |
| "dinner"    | "2011-10-16" | 25.05  |
| "lunch"     | "2011-10-16" | 10.00  |
| "breakfast" | "2011-10-16" | 6.65   |
| "dinner"    | "2011-10-15" | 31.45  |
| "lunch"     | "2011-10-15" | 6.20   |
| "lunch"     | "2011-10-15" | 9.45   |
| "breakfast" | "2011-10-15" | 4.32   |
| "dinner"    | "2011-10-14" | 28.30  |
| "lunch"     | "2011-10-14" | 11.13  |
| "breakfast" | "2011-10-14" | 6.53   |

En utilisant le mot clef `VALUES` on pourrait apposer la condition suivante:

```
VALUES ?description { "lunch" "dinner" }
```

Ce qui mermettrait d'obtenir comme nouveau résultat:

| description | date         | amount |
| ----------- | ------------ | ------ |
| "lunch"     | "2011-10-16" | 10.00  |
| "lunch"     | "2011-10-15" | 6.20   |
| "lunch"     | "2011-10-15" | 9.45   |
| "lunch"     | "2011-10-14" | 11.13  |
| "dinner"    | "2011-10-16" | 25.05  |
| "dinner"    | "2011-10-15" | 31.45  |
| "dinner"    | "2011-10-14" | 28.30  |

On voit ici que `VALUES` à été utilisé dans le but de filtrer les résultats de requêtes sur certaines valeurs.
Inversement, `VALUES` peut permettre d'initialiser une variable a plusieurs valeurs pour mutualiser les contraines appliqués à cette variable.

Par exemple avec la requête suivante:

```XML
SELECT ?item ?itemLabel ?mother ?motherLabel WHERE {
  VALUES ?item { wd:Q937 wd:Q1339 }
  OPTIONAL { ?item wdt:P25 ?mother. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fitem%20%3FitemLabel%20%3Fmother%20%3FmotherLabel%20WHERE%20%7B%0A%20%20VALUES%20%3Fitem%20%7B%20wd%3AQ937%20wd%3AQ1339%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP25%20%3Fmother.%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%7D)

La contrainte optionelle va s'exprimer sur les deux types d'entitées assignées à la variable `?item`.


### Les aggrégations

| Mot clé SparQL | Fonction                                                                                                       |                                                 Syntaxe |
| -------------- | -------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------:|
| DISTINCT       | Permet de supprimer les doublons dans les résultats                                                            |                    SELECT DISTINCT ?var1 ?var2… WHERE … |
| GROUP BY       | Permet de regrouper les valeurs d'une colonne en particulier, et d'appliquer certaines fonctions d'aggrégation | SELECT ?var1 ?var2 … WHERE {CONTRAINTES} GROUP BY ?var2 |


Les aggrégations permettent de rassembler les résultats selon les valeurs d'une ou plusieurs colonnes.

Cela nécessite de travailler sur les colonnes non aggrégées. En effet, après l'aggrégation, la nouvelle ligne unique aura une de ses colonnes qui possedera plusieurs valeurs. On devra donc expliciter via les fonctions d'aggrégation quel valeur garder pour cette ligne.

Par exemple, imaginez que nous voulons récupérer toutes les différentes catégories de prix Nobel que nous avions récupérées avec les requêtes précédentes. On pourrait être tenté d'écrire cette requête:
```XML
SELECT ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0A)

On a bien récupéré toute les catégories, mais malheureusement on a une réponse pour chaque champ de personnes ayant reçu un prix. On peut remédier à ça simplement avec le mot clef `DISTINCT`

```XML
SELECT DISTINCT ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20DISTINCT%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0A)

`DISTINCT` permet d'aggréger facilement les lignes dont **toutes** les valeurs sont identiques. Ca permet d'éviter simplement les doublons si il n'y a pas d'autres manipulations nécessaires.

Le résultat serait le même en utilisant un `GROUP BY`

```XML
SELECT ?priceLabel
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
GROUP BY ?priceLabel
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3FpriceLabel%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AGROUP%20BY%20%3FpriceLabel%0A)

L'avantage du `GROUP BY` est la possibilité d'utiliser certaines fonctions pour récupérer plus d'informations. On peut par exemple chercher combien de personnes ont remporté chaque catégorie de prix.

```XML
SELECT ?priceLabel (COUNT(?winner) as ?winnerNb) #créé une variable ?winnerNb qui est le résultat de la comptabilisation du nombre de résultat stockés dans la variable ?winner
WHERE
{
  ?price wdt:P279 wd:Q7191.
  ?price wdt:P1346 ?winner.
  ?winner wdt:P31 wd:Q5.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
GROUP BY ?priceLabel
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3FpriceLabel%20%28COUNT%28%3Fwinner%29%20as%20%3FwinnerNb%29%0AWHERE%0A%7B%0A%20%20%3Fprice%20wdt%3AP279%20wd%3AQ7191.%0A%20%20%3Fprice%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AGROUP%20BY%20%3FpriceLabel%0A)

### Les fonctions d'aggrégation

Il existe plusieurs fonctions qui permettent de transformer un ensemble de valeurs en une valeur unique, qui pourra ainsi être affichée dans le résultat de l'aggrégation.



| Mot clé                     | Fonction                                                                       | Exemple                                                  |
|:--------------------------- | ------------------------------------------------------------------------------ |:-------------------------------------------------------- |
| COUNT                       | Renvoie le nombre d'élements présents                                          | COUNT(?author) as ?nbAuthors                             |
| SUM, AVG                    | Renvoient respectivement la somme et la moyenne de l'ensemble des élements     | SUM(?nbBooks) as ?totalNbBooks, AVG(?price) as ?avgPrice |
| MIN, MAX                    | Renvoient respectivement la valeur minimale et la valeur maximale des élements | MIN(?temperature) as minTemp                             |
| SAMPLE                      | Renvoie un des élements de l'ensemble au hasard                                | SAMPLE(?ref) as ?sampleRef                               |
| GROUP_CONCAT | Concatène tous les élements                                                    | GROUP_CONCAT(?hashtag) as ?hashtags                      |


Prenons pour exemple la requête suivante pour illustrer les fonctions d'aggrégation:
```XML
SELECT ?firstNameLabel ?lastNameLabel ?bookLabel ?pubDate
WHERE
{
  wd:Q7934 wdt:P735 ?firstName.
  wd:Q7934 wdt:P734 ?lastName.
  ?book wdt:P50 wd:Q7934.
  ?book wdt:P577 ?pubDate.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
ORDER BY ?pubDate
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3FfirstNameLabel%20%3FlastNameLabel%20%3FbookLabel%20%3FpubDate%0AWHERE%0A%7B%0A%20%20wd%3AQ7934%20wdt%3AP735%20%3FfirstName.%0A%20%20wd%3AQ7934%20wdt%3AP734%20%3FlastName.%0A%20%20%3Fbook%20wdt%3AP50%20wd%3AQ7934.%0A%20%20%3Fbook%20wdt%3AP577%20%3FpubDate.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AORDER%20BY%20%3FpubDate)

Le résultat représente le nom et prénom d'un auteur, ainsi que les titres et les dates de parution de ses oeuvres.

Essayons d'agglomérer les résultats de cette requête en fonction de l'auteur.

On va compter le nombres d'oeuvres présentes dans la requête.

```XML
SELECT ?firstNameLabel ?lastNameLabel (COUNT(?book) as ?nbBooks)
WHERE
{
  wd:Q7934 wdt:P735 ?firstName.
  wd:Q7934 wdt:P734 ?lastName.
  ?book wdt:P50 wd:Q7934.
  ?book wdt:P577 ?pubDate.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
GROUP BY ?firstNameLabel ?lastNameLabel
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3FfirstNameLabel%20%3FlastNameLabel%20%28COUNT%28%3Fbook%29%20as%20%3FnbBooks%29%0AWHERE%0A%7B%0A%20%20wd%3AQ7934%20wdt%3AP735%20%3FfirstName.%0A%20%20wd%3AQ7934%20wdt%3AP734%20%3FlastName.%0A%20%20%3Fbook%20wdt%3AP50%20wd%3AQ7934.%0A%20%20%3Fbook%20wdt%3AP577%20%3FpubDate.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AGROUP%20BY%20%3FfirstNameLabel%20%3FlastNameLabel%0AORDER%20BY%20%3FpubDate)

Pour obtenir ce résultat, il ne faut pas oublier de nommer dans le `GROUP BY` **toutes** les colonnes que l'on veut regrouper par valeurs. Vu que la variable `?book` n'est pas présente dans le `GROUP BY`, on **doit** appliquer a cette variable, dans le résultat de la requête, une fonction d'aggrégation. En l'occurence `COUNT`.

Il ne faut pas oublier que le résultat de la fonction d'aggrégation **doit** être assigné a une nouvelle variable de la manière suivante:

```XML
(COUNT(?var) as ?newVar)
```

> A noter que l'on peut utiliser la clause `DISTINCT` dans une fonction `COUNT` pour éviter de compter les doublons potentiels.

```XML
SELECT ?priceLabel (COUNT(DISTINCT ?winner) as ?winnerNb)
```

Les autres fonctions d'aggrégation s'utilisent de la même manière, il faut juste adapter la fonction selon le résultat désiré.

A noter que `MIN` et `MAX` peuvent êtres utilisés sur de nombreux types de données et permettent d'avoir un résultat reproductible. En effet la fonction `SAMPLE` n'assure pas que la valeur récupérée soit toujours la même.

Exercice:
A partir de la [Page Wikidata des Oscars](https://www.wikidata.org/wiki/Q19020), écrivez une requête SparQL qui permette de récupérer les 5 catégories d'Oscars qui ont été remportés par le plus de personnes, en affichant le nom de la catégorie et le nombre de personnes qui l'ont remporté. Afficher le résultat dans l'ordre décroissant.

[Solution](https://query.wikidata.org/#SELECT%20%3FawardCategoryLabel%20%28COUNT%28DISTINCT%20%3Fwinner%29%20as%20%3FwinnerNb%29%0AWHERE%0A%7B%0A%20%20wd%3AQ19020%20wdt%3AP527%20%3FawardCategory.%0A%20%20%3FawardCategory%20wdt%3AP1346%20%3Fwinner.%0A%20%20%3Fwinner%20wdt%3AP31%20wd%3AQ5.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AGROUP%20BY%20%3FawardCategoryLabel%0AORDER%20BY%20DESC%28%3FwinnerNb%29%0ALIMIT%205)

Je ne comprends pas cette requête :)

wdt:P527 : comprend

```XML
SELECT ?awardCategoryLabel (COUNT(DISTINCT ?winner) as ?winnerNb) #nombre de personnes

WHERE
{
  wd:Q19020 wdt:P527 ?awardCategory. #vainqueur aux oscars comprend la variable ?awardCategory
                                     # => On stocke ici tout les éléments liés à l'entitée des [oscars](wd:Q19020) via la propriétée [comprend](wdt:P527) dans la variable ?awardCategory.
                                     # Autrement dit on récupère toute les entitées qui sont des instances des oscars. Soit les catégories des oscars dans ce cas.
  ?awardCategory wdt:P1346 ?winner. #awardCategory est une vainqueur stocké dans la variable ?winner
                                    # => Stocker dans la variable ?winner toutes les entitées qui respectent la propriétée [vainqueur](wdt:P1346) sur l'ensemble des entités stockés dans la variable ?awardCategory
  ?winner wdt:P31 wd:Q5. #qui est de type être humain
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
GROUP BY ?awardCategoryLabel #regroupe les catégories d'Oscars
ORDER BY DESC(?winnerNb) #ordonnée par le nombre de personnes
LIMIT 5 # limité aux 5 premiers résultats

# Ne pas oublier qu'un triplet comprenant une variable peut permettre de stocker dans cette variable les objets ou les sujets de(s) entité(s) déja définie(s) !
```


## Notions avancées

### Les sous-requêtes

Il peut arriver que les requêtes SparQL deviennent très longues. 

Pour améliorer la lisibilitée des requêtes, on peut les diviser en sous requêtes. Cela permet de les relire plus facilement, et de les tester indépendament. 

D'une manière générale, les sous-requêtes suivent le format suivant:

```XML
# Requête principale
SELECT ...
WHERE
{
  {
    # La sous-requête s'écrit içi dans le format classique
  }
}
```

Toutes les variables retournées par la sous-requête sont utilisables dans la requête principale.

Par exemple, voici une requête composée de sous requêtes. Celle-ci récupère tout les écrivains qui respectent certains critères **indépendants** et les classe suivant un score et selon le nombre d'oeuvres de science-fiction qu'ils ont écrites.

```XML
SELECT ?author ?authorLabel (MAX(?nbBooks) as ?maxNbBooks) (SUM(?maxScore) as ?sumScore)
WHERE
{
  # Sélectionne tout les écrivain qui ont pour occupation `écrivain de science fiction`
  {
    SELECT ?author (COUNT(DISTINCT(?book)) as ?nbBooks) (MAX(?score) as ?maxScore)
    WHERE
    {
      ?author wdt:P31 wd:Q5.
      ?author wdt:P106 wd:Q18844224.
      ?book wdt:P50 ?author.
      ?book wdt:P136 wd:Q24925.
      BIND(2 as ?score).
    }
    GROUP BY ?author ?authorLabel
  }
  UNION
  # Sélectionne tout les écrivain qui ont reçu le prix Hugo.
  {
    SELECT ?author (COUNT(DISTINCT(?book)) as ?nbBooks) (MAX(?score) as ?maxScore)
    WHERE
    {
      ?author wdt:P31 wd:Q5.
      ?author wdt:P166 wd:Q255032.
      ?book wdt:P50 ?author.
      ?book wdt:P136 wd:Q24925.
      BIND(4 as ?score).
    }
    GROUP BY ?author ?authorLabel
  }
  UNION
  # Sélectionne tout les écrivain qui ont pour genre `science-fiction`
  {
    SELECT ?author (COUNT(DISTINCT(?book)) as ?nbBooks) (MAX(?score) as ?maxScore)
    WHERE
    {
      ?author wdt:P31 wd:Q5.
      ?author wdt:P136 wd:Q24925.
      ?book wdt:P50 ?author.
      ?book wdt:P136 wd:Q24925.
      BIND(1 as ?score).
    }
    GROUP BY ?author ?authorLabel
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
GROUP BY ?author ?authorLabel
ORDER BY DESC(?sumScore) DESC(?maxNbBooks)
```
[Tester la requête](https://huit.re/oscarRequete)

Le mot clef `UNION` permet de rassembler des ensembles de résultats si ils sont cohérents entre eux.

On entends par *cohérents* que les variables doivent avoir le même nom, et représenter des entitées du même type.

L'utilisation de sous-requêtes dans ce cas est différent de l'utilisation du mot clef `OPTIONAL`. Ici les auteurs sont sélectionnés de manière stricte sur une contrainte, puis tout les résultats sont agglomérés entre eux. (expliciter la différence)

Les sous requêtes sont aussi utilisées ici pour donner un score à chacun des résultats de requêtes grâce à la fonction `BIND`. 

En effet, disons que nous voulions privilégier dans l'ordre des résultats les auteurs qui sont présents dans le maximum de sous-requêtes. Le fait d'assigner une nouvelle variable `?score` a chaque sous-requête va permettre d'indiquer le respect des contraintes par l'entité dans le résultat final. On peut jouer subjectivement sur la valeur du score pour privilégier la présence dans une sous-requête par rapport à une autre.

Ensuite l'utilisation de la fonction `SUM` dans la requête principale permet de se retrouver avec un score global pour chacun des auteurs en fonction des réponses des sous-requêtes.

Par exemple, pour un auteur qui aurait remporté un prix Hugo et qui aurait pour occupation 'écrivain de science fiction':

| author    | nbBooks | maxScore |
|:--------- | ------- | -------- |
| ecrivain1 | 20      | 4        |
| ecrivain1 | 20      | 2        |

On aurait cette entrée dans l'union des résultats des sous requêtes.

Après utilisation du `GROUP BY` dans la requête principale:

| author    | nbBooks | sumScore |
|:--------- | ------- | -------- |
| ecrivain1 | 20      | 6        |

On aura donc le score final sumScore qui permettra de classer les entitées selon l'importance que l'on donne au respect ou non de certaines contraintes.

La valeur de la variable sumScore peut être interprétée de la manière suivante:

| valeur | construction | signification                                                                  |
| ------ | ------------ | ------------------------------------------------------------------------------ |
| 7      | 4 + 2 + 1    | Prix Hugo + occupation "ecrivain de science fiction" + genre "science-fiction" |
| 6      | 4 + 2        | Prix Hugo + occupation "ecrivain de science fiction"                           |
| 5      | 4 + 1        | Prix Hugo + genre "science-fiction"                                            |
| 4      | 4            | Prix Hugo                                                                      |
| 3      | 2 + 1        | occupation "ecrivain de science fiction" + genre "science-fiction"             |
| 2      | 2            | occupation "ecrivain de science fiction"                                       |
| 1      | 1            | genre "science-fiction"                                                        |

Le choix des valeurs des scores permet d'avoir des valeurs uniques pour toutes les combinaisons possibles. 
Cela permet de savoir selon la valeur du score final quelles contraintes ont été respectées par l'entité sans ambiguïté.
C'est un détail intéréssant donné à titre d'indication, qui n'est pas à retenir dans le cadre de cette formation.

### Les qualificateurs

Il peut arriver que la donnée qui vous intéresse soit un peu plus complexe à récupérer que dans les exemples vus précédemment.

Reprenons par exemple la page wikidata des [Oscars de la meilleure actrice](https://www.wikidata.org/wiki/Q103618)

On peut voir que pour chaque personne ayant remporté un Oscar, on a à disposition la date d'obtention ainsi qu'une indication du film pour lequel cet Oscar a été délivré.

Comment accéder à cette donnée ?

En effet, avec la requête suivante:
```XML
SELECT ?winner
WHERE
{
  wd:Q103618 wdt:P1346 ?winner.
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%0AWHERE%0A%7B%0A%20%20wd%3AQ103618%20wdt%3AP1346%20%3Fwinner.%0A%7D)

On récupère directement les entités des personnes ayant remporté ce prix.

Pour récupérer les informations annexes, il faut ajouter une étape, et accéder d'abord aux métadonnées relatives aux gagnants.

Pour y accéder, on ne passe plus par le préfixe `wdt:` mais par le préfixe `p:`

```XML
SELECT ?winner
WHERE
{
  wd:Q103618 p:P1346 ?winner.
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%0AWHERE%0A%7B%0A%20%20wd%3AQ103618%20p%3AP1346%20%3Fwinner.%0A%7D)

Si on clique sur une des entités retournées par cette requête, on constate qu'on est resté sur la page des Oscars de la meilleure actrice, et que l'entité correspond à la section de la propriété spécifiée.

La manipulation de cette section se fait de cette manière:

`entité_parent p:propriété_generale  section`
`section ps:propriété_generale entité_enfant`
`section pq:propriété_qualifieur entité_qualifieur(ou valeur directe)`

Pour donner un exemple plus concret, si on cherche à récupérer toutes les gagnantes des Oscars de la meilleure actrice, avec si disponible la date d'obtention, le film et le rôle :

```XML
SELECT ?winner ?winnerLabel ?date ?filmLabel ?roleLabel
WHERE
{
  wd:Q103618 p:P1346 ?section.
  ?section ps:P1346 ?winner.
  OPTIONAL{?section pq:P585 ?date}
  OPTIONAL{?section pq:P1686 ?film}
  OPTIONAL{?section pq:P453 ?role}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3Fdate%20%3FfilmLabel%20%3FroleLabel%0AWHERE%0A%7B%0A%20%20wd%3AQ103618%20p%3AP1346%20%3Fsection.%0A%20%20%3Fsection%20ps%3AP1346%20%3Fwinner.%0A%20%20OPTIONAL%7B%3Fsection%20pq%3AP585%20%3Fdate%7D%0A%20%20OPTIONAL%7B%3Fsection%20pq%3AP1686%20%3Ffilm%7D%0A%20%20OPTIONAL%7B%3Fsection%20pq%3AP453%20%3Frole%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D)

Exercice:
A partir de la page des [Oscars](https://www.wikidata.org/wiki/Q19020) écrire une requête qui récupère tous les films, le nombre d'Oscars qu'ils ont permis de remporter ainsi que la date à laquelle ils ont été remis. Les classer par ordre décroissant.

[Solution](https://query.wikidata.org/#SELECT%20%3Ffilm%20%3FfilmLabel%20%3Fdate%20%28COUNT%28%3Fwinner%29%20as%20%3FwinnerNb%29%0AWHERE%0A%7B%0A%20%20%3FawardCategory%20wdt%3AP31%20wd%3AQ19020.%0A%20%20%3FawardCategory%20p%3AP1346%20%3Fsection.%0A%20%20%3Fsection%20ps%3AP1346%20%3Fwinner.%0A%20%20OPTIONAL%7B%3Fsection%20pq%3AP585%20%3Fdate%7D%0A%20%20%3Fsection%20pq%3AP1686%20%3Ffilm.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AGROUP%20BY%20%3Ffilm%20%3FfilmLabel%20%3Fdate%0AORDER%20BY%20DESC%28%3FwinnerNb%29)

### Les références

Les références permettent de spécifier les sources des informations récupérées sur wikidata si elles sont présentes. **Il est recommandé de toujours préciser ses sources quand il est question de participer au site**.

Pour accéder aux références, le principe ressemble beaucoup à l'accès aux qualificateurs.

Par exemple, pour accéder aux sources dans les exemples d'entités utilisés précedement:

```XML
SELECT ?winner ?winnerLabel ?source
WHERE
{
  wd:Q103618 p:P1346 ?node.
  ?node ps:P1346 ?winner.
  OPTIONAL{?node prov:wasDerivedFrom ?refNode.
           ?refNode pr:P854 ?source.}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Fwinner%20%3FwinnerLabel%20%3Fsource%0AWHERE%0A%7B%0A%20%20wd%3AQ103618%20p%3AP1346%20%3Fnode.%0A%20%20%3Fnode%20ps%3AP1346%20%3Fwinner.%0A%20%20OPTIONAL%7B%3Fnode%20prov%3AwasDerivedFrom%20%3FrefNode.%0A%20%20%20%20%20%20%20%20%20%20%20%3FrefNode%20pr%3AP854%20%3Fsource.%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0A)

On remarque qu'on doit en premier lieu accéder à la section possédant la référence avec le préfixe `p:` puis extraire le noeud de références avec la propriété `prov:wasDerivedFrom`.
On peut ensuite accéder à tous les types de références en utilisant le préfixe `pr:`

Exercice: Récupérer toutes les espèces de corvidés(Q43365) présentes dans wikidata et afficher leur nom, une image parmi celles disponibles pour chaque espèce, la carte de leur répartition géographique, le nom de leur taxon, leur statut UICN, ainsi que la référence de la dernière date de consultation (P813) pour la section du taxon.

Attention, il est nécessaire d'utiliser `GROUP BY `ainsi que `SAMPLE` pour n'avoir qu'une ligne par espèce.

[Solution](https://query.wikidata.org/#SELECT%20%3Fcorvids%20%3FcorvidsLabel%20%28SAMPLE%28%3Fimage%29%20as%20%3Fs_image%29%20%3Fmap%20%3Ftaxon%20%3FUICNLabel%20%28SAMPLE%28%3FsourceStatement%29%20as%20%3Fs_sourceStatement%29%0AWHERE%0A%7B%0A%20%20%3Fcorvids%20wdt%3AP171%20wd%3AQ43365.%0A%20%20%3Fcorvids%20wdt%3AP18%20%3Fimage.%0A%20%20%3Fcorvids%20p%3AP225%20%3FtaxonNode.%0A%20%20OPTIONAL%7B%3FtaxonNode%20ps%3AP225%20%3Ftaxon.%7D%0A%20%20OPTIONAL%7B%3FtaxonNode%20prov%3AwasDerivedFrom%20%3Fsource.%0A%20%20%20%20%20%20%20%20%20%20%20%3Fsource%20pr%3AP813%20%3FsourceStatement.%7D%0A%20%20%3Fcorvids%20wdt%3AP141%20%3FUICN.%0A%20%20%3Fcorvids%20wdt%3AP181%20%3Fmap.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0AGROUP%20BY%20%3Fcorvids%20%3FcorvidsLabel%20%3Ftaxon%20%3FUICNLabel%20%3FrefNode%20%3Fmap)

## Les visualisations

### Graphes

L'outil de requête de wikidata permet la représentation des données sous différentes formes. Ce qui peut être très intéressant pour analyser les données ou les présenter sous une forme plus intuitive.

Prenons par exemple cette requête.

```XML
SELECT ?element ?elementLabel ?atomNb ?discovDate
WHERE
{
  ?element wdt:P31 wd:Q11344.
  ?element wdt:P1086 ?atomNb.
  ?element wdt:P575 ?discovDate.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
}
```
[Tester la requête](https://query.wikidata.org/#SELECT%20%3Felement%20%3FelementLabel%20%3FatomNb%20%3FdiscovDate%0AWHERE%0A%7B%0A%20%20%3Felement%20wdt%3AP31%20wd%3AQ11344.%0A%20%20%3Felement%20wdt%3AP1086%20%3FatomNb.%0A%20%20%3Felement%20wdt%3AP575%20%3FdiscovDate.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%7D%0A)

Celle-ci nous renvoie tous les éléments du tableau periodique présents sur wikidata, accompagnés de leur numéro atomique et la date de leur découverte.

Observées sous forme de tableau, ces données ne paraissent pas, au premier abord, faire ressortir de relations entre elles. 

Représentée sous forme de tableau, la repartition des numéros atomiques en fonction des dates de découvertes permet de mettre l'accent sur un point intéréssant. (Je n'arrive pas à utiliser l'affichage automatique pour créer un graphe avec le GraphBuilder, on pourra eventuellement leur faire faire à la main pour leur montrer l'edition de graphes)

On peut voir sur ce graphe que la répartition est au départ aléatoire. Par contre la fin du graphique prend une allure linéaire. En regardant plus en détail, le changement intervient vers les années 1930/40. En faisant quelques recherches on se rend compte que cela coïncide avec les grandes découvertes sur la radioactivité et l'atome.

On peut donc observer le moment ou l'humanité a cessé de **découvrir** les éléments du tableau periodique, et ou elle a commencé à les **créer**.

La visualisation en graphe permet en un coup d'oeil de remarquer les tendances, corrélations ou autres dans l'ensemble de notre jeu de données.

Cela peut donner de précieuses pistes d'interprétation.

### Cartes

Wikidata permet aussi de visualiser des données géographiques sous forme de cartes.

Il suffit qu'une entité ait une propriété qui permette de récuperer des coordonnées géographique (ex:P625) pour que le service de requêtes de wikidata puisse automatiquement nous afficher une carte avec les entités réparties selon leurs coordonnées.

```XML
# Afficher sur une carte 99 centres d'archives départementaux

#defaultView:Map
SELECT ?item ?itemLabel (SAMPLE(?natureLabel) as ?sampleNL) (SAMPLE(?coord) as ?sampleCoord)
WHERE
{
 hint:Query hint:optimizer "None" .
 # les entités dont le type est archives départementales
 ?item wdt:P31 wd:Q2860456 .
 # qui sont des instances ou des sous-classes de musée
 # si je rajoute cette condition les résultats ne sont plus distincts
 ?item wdt:P31 ?nature .
 # récupère le label de manière explicite pour pouvoir le manipuler
 ?nature rdfs:label ?natureLabel.
 FILTER (langMatches( lang(?natureLabel), "fr" ) ).
 ?item wdt:P625 ?coord .
 SERVICE wikibase:label {bd:serviceParam wikibase:language "fr" .}
}
GROUP BY ?item ?itemLabel
ORDER BY ASC (?itemLabel)
# classés par nom
```
[Tester la requête](https://query.wikidata.org/#%23%20Afficher%20sur%20une%20carte%2099%20centres%20d%27archives%20d%C3%A9partementaux%0A%0A%23defaultView%3AMap%0ASELECT%20%3Fitem%20%3FitemLabel%20%28SAMPLE%28%3FnatureLabel%29%20as%20%3FsampleNL%29%20%28SAMPLE%28%3Fcoord%29%20as%20%3FsampleCoord%29%0AWHERE%0A%7B%0A%20hint%3AQuery%20hint%3Aoptimizer%20%22None%22%20.%0A%20%23%20les%20entit%C3%A9s%20dont%20le%20type%20est%20archives%20d%C3%A9partementales%0A%20%3Fitem%20wdt%3AP31%20wd%3AQ2860456%20.%0A%20%23%20qui%20sont%20des%20instances%20ou%20des%20sous-classes%20de%20mus%C3%A9e%0A%20%23%20si%20je%20rajoute%20cette%20condition%20les%20r%C3%A9sultats%20ne%20sont%20plus%20distincts%0A%20%3Fitem%20wdt%3AP31%20%3Fnature%20.%0A%20%23%20r%C3%A9cup%C3%A8re%20le%20label%20de%20mani%C3%A8re%20explicite%20pour%20pouvoir%20le%20manipuler%0A%20%3Fnature%20rdfs%3Alabel%20%3FnatureLabel.%0A%20FILTER%20%28langMatches%28%20lang%28%3FnatureLabel%29%2C%20%22fr%22%20%29%20%29.%0A%20%3Fitem%20wdt%3AP625%20%3Fcoord%20.%0A%20SERVICE%20wikibase%3Alabel%20%7Bbd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22%20.%7D%0A%7D%0AGROUP%20BY%20%3Fitem%20%3FitemLabel%0AORDER%20BY%20ASC%20%28%3FitemLabel%29%0A%23%20class%C3%A9s%20par%20nom)

La requête permet de visualiser la carte par défaut. C'est possible grâce au commentaire `#defaultView:Map`. 

Les données ont été filtrées pour n'avoir qu'une seule entrée pour chaque service d'archives. 

Noubliez pas d'aggréger vos données pour éviter les doublons !

### Arbres

Dans le cas ou vous voudriez récupérer un grand nombre d'objets différents, et pouvoir les visualiser par catégories, c'est possible grâce à WikiData !

```XML
#defaultView:TreeMap
SELECT ?sub_arch ?sub_archLabel ?sub2_arch ?sub2_archLabel ?result ?resultLabel
WHERE
{
  ?sub_arch wdt:P279 wd:Q166118.
  ?sub2_arch wdt:P279 ?sub_arch.
  ?result wdt:P31 ?sub2_arch
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE], fr". }
}
```
[Tester la requête](https://query.wikidata.org/#%23defaultView%3ATreeMap%0ASELECT%20%3Fsub_arch%20%3Fsub_archLabel%20%3Fsub2_arch%20%3Fsub2_archLabel%20%3Fresult%20%3FresultLabel%0AWHERE%0A%7B%0A%20%20%3Fsub_arch%20wdt%3AP279%20wd%3AQ166118.%0A%20%20%3Fsub2_arch%20wdt%3AP279%20%3Fsub_arch.%0A%20%20%3Fresult%20wdt%3AP31%20%3Fsub2_arch%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2C%20fr%22.%20%7D%0A%7D)

Cette requête renvoie toutes les catégories de l'entité [archives](https://www.wikidata.org/wiki/Q166118) puis descend encore d'un niveau en récupérant toutes les sous-catégories de ces éléments. Enfin, après avoir récupéré toutes ces catégories et sous-catégories, elle récupère toutes les entités qui ont pour instance ces sous-catégories.

On peut, de cette manière, explorer et scanner la structure de wikidata. Cette représentation en tableau n'est malheureusement pas naturelle à lire et analyser pour un être humain.

C'est là tout l'avantage de la représentation en Arbre, qui permet de rassembler les entités qui ont des valeurs communes.

[Exemple de requêtes pour récupérer pas mal des informations recherchées](https://query.wikidata.org/#SELECT%20DISTINCT%20%0A%3Fgenre%20%3FgenreLabel%20%0A%28GROUP_CONCAT%28DISTINCT%20%3ForiginLabel%3B%20separator%3D%22%2C%20%22%29%20as%20%3Forigins%29%20%0A%28GROUP_CONCAT%28DISTINCT%20%3Fcategory%3B%20separator%3D%22%2C%20%22%29%20as%20%3Fcategories%29%20%0A%28GROUP_CONCAT%28DISTINCT%20%3Fcategory2Label%3B%20separator%3D%22%2C%20%22%29%20as%20%3Fcategories2%29%0AWHERE%20%7B%0A%20%20%3Fgenre%20wdt%3AP279%20wd%3AQ6097.%0A%20%20%3Fgenre%20rdfs%3Alabel%20%3FgenreLabel.%0A%20%20FILTER%28langmatches%28lang%28%3FgenreLabel%29%2C%20%27fr%27%29%29.%0A%20%20OPTIONAL%7B%3Fgenre%20wdt%3AP495%20%3Forigin.%0A%20%20%20%20%20%20%20%20%20%20%20%3Forigin%20rdfs%3Alabel%20%3ForiginLabel.%0A%20%20%20%20%20%20%20%20%20%20%20FILTER%28langmatches%28lang%28%3ForiginLabel%29%2C%20%27fr%27%29%29%7D%0A%20%20OPTIONAL%7B%3Fgenre%20wdt%3AP373%20%3Fcategory.%7D%0A%20%20OPTIONAL%7B%3Fgenre%20wdt%3AP279%20%3Fcategory2.%0A%20%20%20%20%20%20%20%20%20%20%20%3Fcategory2%20rdfs%3Alabel%20%3Fcategory2Label.%0A%20%20%20%20%20%20%20%20%20%20%20FILTER%28langmatches%28lang%28%3Fcategory2Label%29%2C%20%27fr%27%29%29%7D%0A%7D%0AGROUP%20BY%20%3Fgenre%20%3FgenreLabel)

Les (sous)-types de thé
[Les sous-types de thé qui n'ont pas de label français](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fgenre%20%3FgenreLabel%0AWHERE%20%7B%0A%20%20%3Fgenre%20wdt%3AP279%20wd%3AQ6097%20.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%20%20FILTER%20NOT%20EXISTS%20%7B%0A%20%20%20%20%3Fgenre%20rdfs%3Alabel%20%3FgenreLabel%0A%20%20%20%20FILTER%20%28lang%28%3FgenreLabel%29%20%3D%20%22fr%22%29%0A%20%20%7D%0A%7D)
Les sous-type de thé qui ne sont pas des tisanes
Les sous-type de thé qui sont utilisés comme médecine traditionnelle
Le nombre de sous-type de thé par pays
Le nombre de catégorie de thé
Le nombre de thé par catégorie
