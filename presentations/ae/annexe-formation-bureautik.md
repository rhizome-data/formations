name : Annexe

### Paramétrer l'outil Resip

**Identification des formats de fichier** :

La moulinette ReSIP procède à une identification du format des fichiers traités en utilisant
l’application Siegfried ainsi que les fichiers de signatures DROID publiés par The National
Archives (UK)

Pour mettre à jour des fichiers, il suffit de :

 * télécharger ces fichiers sur le site de The National Archives (UK), à l’adresse suivante : https://www.nationalarchives.gov.uk/aboutapps/pronom/droid-signature-files.htm;
 * substituer, dans le répertoire config du répertoire ReSIP, les fichiers existants par les fichiers plus récents précédemment téléchargés

---
### Paramétrer l'outil Resip

**configuration du bordereau SEDA**

Dans le fichier ResipPreferences.properties on peut spécifier les propriétés qui aliment le manifeste.xml exporté dans le SIP

* exportContext.globalMetadata.comment : le commentaire associé au bordereau
* exportContext.globalMetadata.messageIdentifier : le titre du bordereau
* exportContext.general.keptMetadataList : la liste des éléments SEDA exportés dans le manifeste
* exportContext.general.managementMetadataXmlData : les valeurs des métadonnées de gestion (notamment les libellés des services versant et  producteur)
* exportContext.globalMetadata.archivalAgencyIdentifier : l'identifiant du service archives
* exportContext.globalMetadata.archivalAgencyOrganizationDescriptiveMetadataXmlData : la fiche XML-EAC du service archives (?)
* exportContext.globalMetadata.transferringAgencyOrganizationDescriptiveMetadataXmlData: la fiche XML-EAC du service versant (?)
* exportContext.globalMetadata.archivalAgreement : l'identifiant du contrat de versement associé
* exportContext.globalMetadata.codeListVersionsXmlData : les identifiants des listes de codes associés avec le manifeste

---
### Utiliser l'interface graphique

En mode graphique il est possible d'importer une arborescence de dossiers (contenant des fichiers) et d'éditer le futur bordereau d'export

En cliquant sur un dossier (une unité d'archives) la fenêtre de droite présente les métadonnées de base permettant de la caractériser

Il est possible d'ajouter des métadonnées de manière libre en cliquant sur le bouton "éditer l'ArchvieUnit"

En cliquant sur le bouton "ajouter" il est possible d'ajouter des éléments SEDA de manière assistée

Le champ « Profil d’unité archivistique »(ArchiveUnitProfile) est préfixé par un [A], les métadonnées de description avec un [C], les métadonnées de gestion avec un [M].

> Attention : les métadonnées répétables sont suivies d’un astérisque.

---
### Le panneau de visualisation de la liste des objets

Ce panneau permet de visualiser la liste des objets. Il permet également de sélectionner un de ces objets pour voir ses métadonnées, le visualiser ou changer le fichier correspondant.

Un bouton "ouvrir l'objet" permet de visualiser l'objet avec l'application associé sur l'ordinateur utilisé

Un bouton "changer l'objet" permet de substituer un fichier à celui importé dans l'arborescence.

---
### Import d’une arborescence de fichiers

Paramétrage de l’import : cliquer sur l’action « Fichier » puis sur la sous-action « Préférences »

Le clic sur la sous-action « Importer depuis un SIP » permet de :

* définir les paramètres d’import des messageries
* définir les fichiers susceptibles d’être présents dans la structure arborescente de fichiers et qui devront être exclus du processus d’import
      * en déclarant le nom des fichiers qui doivent être exclus de l’import (ex: Thumbs.db)
      * en déclarant les catégories de fichiers à exclure sur la base d’expressions régulières
            * pour exclure tous les fichiers dont l’extension est « .odg », il convient d’indiquer
            ```.*\.odg```
            * pour exclure tous les fichiers commençant par « manifest », il convient d’indiquer
            ``` manifest.*```
            * pour exclure tous les fichiers commençant par « manifest » et dont l’extension est « .xml », il convient d’indiquer ```manifest.*xml```;
            * pour exclure tous les fichiers ayant un chiffre dans le titre, il convient d’indiquer
            ```.*[0-9].*```

---
### Paramétres avancés de l'import SIP

Pour les unités archivistiques correspondant aux fichiers, le nom du fichier comme titre (champ Title du standard SEDA), la date de dernière modification du fichier comme date (champ TransactedDate du standard SEDA). Le niveau de description (champ DescriptionLevel du standard SEDA) est incrémenté avec la valeur « Item ».

Pour les unités archivistiques correspondant à des répertoires, l’intitulé de ceux-ci comme titre (champ Title du standard SEDA), la date de la plus ancienne et de la plus récente unité archivistique contenue dans le répertoire (champs StartDate et EndDate du standard SEDA). Le niveau de description (champ DescriptionLevel du standard SEDA) est incrémenté avec la valeur « RecordGrp ».

---
### Métadonnées de l'en-tête du manifeste

Les métadonnées de l’en-tête du manifeste (intitulé et identifiant du SIP, contrat d’entrées, versions des référentiels utilisés, identifiant du service d’archives et identifiant du service de transfert) peuvent être renseignées dans un fichier nommé __GlobalMetadata.xml qui se présente comme suit :

```
<Comment>SIP complexe pour ReSIP</Comment>
<MessageIdentifier>MessageIdentifier0</MessageIdentifier>
<ArchivalAgreement>IC-000001</ArchivalAgreement>
<CodeListVersions>Identifiant des listes de code </CodeListVersions>
<ArchivalAgency>
      <Identifier>Identifier4</Identifier>
</ArchivalAgency>
<TransferringAgency>
      <Identifier>Identifier5</Identifier>
</TransferringAgency>
```
> Ce fichier doit être enregistré dans le répertoire racine destiné à être importé dans la moulinette ReSIP

---
### Métadonnées de gestion du bloc ManagementMetadata

Les métadonnées du bloc ManagementMetadata du manifeste (profil d’archivage, niveau de service, mode d’entrée, statut juridique, règles de gestion applicables à l’ensemble du SIP, service producteur de l’entrée, service versant de l’entrée) peuvent être renseignées dans un fichier nommé __ManagementMetadata.xml qui se présente comme suit :

```
<ManagementMetadata>
      <ArchivalProfile>PR-000001</ArchivalProfile>
      <ServiceLevel></ServiceLevel>
      <AcquisitionInformation>Versement</AcquisitionInformation>
      <LegalStatus>Public Archive</LegalStatus>
      <OriginatingAgencyIdentifier>ABCDEFG</OriginatingAgencyIdentifier>
      <SubmissionAgencyIdentifier>ABCDEFG</SubmissionAgencyIdentifier>
      <AccessRule>
            <Rule>ACC-00001</Rule>
            <StartDate>2017-01-01</StartDate>
      </AccessRule>
</ManagementMetadata>
```

> Ce fichier doit être enregistré dans le répertoire racine destiné à être importé dans la moulinette ReSIP

---
### Métadonnées d'une unité archivistique

Les métadonnées d’une unité archivistique (métadonnées de gestion comme de description) peuvent être renseignées dans un fichier nommé __ArchiveUnitMetadata.xml qui se présente comme suit :

```
<ArchiveUnitProfile>AUP-000001</ArchiveUnitProfile>
<Management>
      <AppraisalRule>
            <Rule>APP-00001</Rule>
            <StartDate>1960-01-01</StartDate>
            <FinalAction>Keep</FinalAction>
      </AppraisalRule>
      <AccessRule>
            <Rule>ACC-00001</Rule>
            <Rule>ACC-00002</Rule>
            <StartDate>2010-05-14</StartDate>
      </AccessRule>
</Management>
<Content>
      <DescriptionLevel>File</DescriptionLevel>
      <Title>Titre de mon dossier</Title>
      <StartDate>1988-01-01</StartDate>
      <EndDate>2018-12-31</EndDate>
</Content>
```

> Ce fichier doit être enregistré dans un répertoire correspondant à l’unité archivistique

---
### Métadonnées d’un objet binaire

Les métadonnées d’un objet binaire (métadonnées techniques) peuvent être renseignées dans un fichier nommé __« usage » [BinaryMaster, Dissemination,Thumbnail, TextContent]_« version »_BinaryDataObjectMetadata.xml qui se présente comme suit :

```
<DataObjectVersion>BinaryMaster_1</DataObjectVersion>
<Uri>content/ID8.txt</Uri>
<MessageDigest algorithm="SHA-512">8e393c3a82...</MessageDigest>
<Size>30</Size>
<FormatIdentification>
      <FormatLitteral>Plain Text File</FormatLitteral>
      <MimeType>text/plain</MimeType>
      <FormatId>x-fmt/111</FormatId>
</FormatIdentification>
<FileInfo>
      <Filename>BinaryMaster.txt</Filename>
      <LastModified>2016-10-18T19:03:30Z</LastModified>
</FileInfo>
```

> Ce fichier doit :
* être enregistré dans un répertoire correspondant à l’unité archivistique, au même niveau que l’objet binaire qu’il décrit ;
* avoir le même préfixe (usage_version_) que celui de l’objet décrit (cf. copie d’écran ci- dessous)

---
### Groupe d’objets techniques contenant plusieurs objets

Il est possible d’importer plusieurs objets, qu’ils soient physiques ou binaires, représentant une
même unité archivistique et constituant ensemble un groupe d’objets

Pour créer un groupe d’objets, il suffit de :

* créer un répertoire correspondant à l’unité archivistique
* enregistrer tous les objets, qu’ils soient physiques ou binaires, représentant l’unité archivistique, en les préfixant comme suit : __ « usage » « version » _ nom du fichier.

---
### Import d’un arbre de positionnement ou d’un plan de classement sous forme de fichier .csv

Le fichier d’import prend la forme d’un fichier au format .csv composé de cinq colonnes :

* id : identifiant unique attribué à l’unité archivistique par l’utilisateur ;
* nom : intitulé de l’unité archivistique ;
* observations : champ libre ;
* cote : identifiant métier propre à l’unité parente (suffixe) ;
* série : identifiant métier de l’unité archivistique « parente » (préfixe).

> Attention :

* l’ordre des colonnes ne doit pas être modifié ;
* une première ligne d’en-tête donnant le nom des colonnes doit être présente, chaque ligne décrivant ensuite une unité archivistique ;
* le séparateur entre les colonnes doit être le « ; ».

---
### Import depuis un fichier .csv décrivant une structure arborescente d’archives et/ ou de fichiers

Le fichier d’import prend la forme d’un fichier au format .csv composé de x colonnes :

* Id : identifiant unique de l’unité archivistique, définie par l’utilisateur (colonne facultative) ;
* ParentId : identifiant unique de l’unité archivistique parente, définie par l’utilisateur (colonne facultative) ;
* File : chemin relatif à partir de l’emplacement où est enregistré le fichier .csv (colonne obligatoire) ;
* DescriptionLevel : niveau de description de l’unité archivistique (colonne obligatoire) ;
* Title : intitulé de l’unité archivistique (colonne obligatoire) ;
* toute colonne correspondant à un champ du standard SEDA : (colonnes facultatives).

---
### Import depuis un fichier .csv décrivant une structure arborescente d’archives et/ ou de fichiers

> pour les colonnes correspondant à des champs du standard SEDA,l’intitulé doit correspondre à celui du champ dans le standard SEDA, précédé de «  Management. » s’il s’agit d’une métadonnée de gestion (ex. « Management.AccessRule.Rule » pour une règle de communicabilité) ou de « Content » s’il s’agit d’une métadonnée descriptive (ex.« Content.DocumentType »).

> quand le schéma XML du standard SEDA propose une structure complexe de balises (par exemple pour décrire un auteur via l’objet XML <Writer> qui contient plusieurs balises XML comme FullName ou BirthName), il convient d’intituler la colonne de la manière suivante : Content.Writer.FullName ou Content.Writer.BirthName

> quand un champ ou un objet XML est multivalué dans le standard SEDA (et qu’il est possible d’en décrire plusieurs dans le bordereau comme c’est le cas pour l’objet Writer par exemple), il convient de numéroter la colonne de la manière suivante : Content.Writer.0.FullName, Content.Writer.1.FullName

> Attention : le fichier .csv à importer doit se trouver dans le même répertoire que le répertoire correspondant à la racine de la structure arborescente de fichiers à importer.

---
### Traitement des structures arborescentes d’archives importées dans la moulinette ReSIP

* tri alphabétique des différents niveaux de la structure arborescente d’archives;
* recherche d’unités archivistiques et d’objets dans la structure arborescente d’archives;
* détection de doublons;
* récupération de statistiques sur les objets;
* réorganisation de l’arborescence;
* traitement des métadonnées des unités archivistiques;
* traitement des objets, tant physiques que binaires, et de leurs métadonnées ;
* vérification de la conformité de la structure arborescente d’archives par rapport au SEDA 2.1;
* sauvegarde du contexte de travail en cours de traitement;
* purge de l’espace de travail.

---
### Recherche d’objets

Afin d’effectuer des recherches d’objets dans une structure arborescente d’archives importée, il convient, dans le menu de la moulinette ReSIP, de :

* cliquer sur l’action « Traiter » puis sur la sous-action « Chercher des objets »
* choisir, dans la fenêtre de dialogue qui s’est ouverte, le type de recherche souhaité :
      * une recherche par catégorie de formats
      * une recherche par liste de formats (PRONOM)
      * une recherche sur la taille des objets, sur la base d’un intervalle défini

---
### Recherche de doublons

* recherches de doublons sur la base de l’empreinte du fichier (algorithme utilisé par défaut : SHA-512) ;
* recherche de doublons sur la base du nom du fichier

---
### Réorganiser l’arborescence

L'arorescence importée peut être retravaillée dans ReSip par :

* la création et l’ajout de nouvelles unités archivistiques;
* le déplacement d’unités archivistiques;
* la suppression d’unités archivistiques;
* le rattachement d’une unité archivistique à une deuxième unité archivistique parente

---
### Vérification de la conformité de la structure arborescente d’archives par rapport au SEDA 2.1

Afin de vérifier la conformité d’une structure arborescente d’archives et de sa description par rapport au schéma par défaut proposé par le SEDA 2.1., il convient, dans la moulinette ReSIP, de cliquer sur l’action « Traiter » puis sur la sous-action « Vérifier la conformité SEDA 2.1. »

---
### Vérification par rapport à un profil d’archivage spécifique

Le clic sur la sous-action « Vérifier la conformité à un profil SEDA 2.1 », ouvre l’explorateur de l’utilisateur et permet à celui-ci de sélectionner un fichier correspondant à un profil d’archivage – au format xsd ou rng – et de l’importer dans la moulinette ReSIP en cliquant sur le
bouton d’action « Ouvrir ».

> Attention : la moulinette ReSIP génère systématiquement des groupes d’objets. Le profil d’archivage doit prendre en compte cette structuration. Par ailleurs, il est conseillé de faire l’export du manifeste en mode arborescent et disposer d’un profil d’archivage arborescent également.

---
### Export des données

Il est possible d’exporter, depuis la moulinette ReSIP, une structure arborescente d’archives sous trois formes différentes :

* un SIP conforme au SEDA 2.1
* un manifeste conforme au SEDA 2.1
* une structure arborescente de fichiers
      * une structure arborescente de fichiers accompagnée d’un fichier de métadonnées au format .csv
      * un fichier de métadonnées au format .csv

---
### Préparer un fichier CSV

La solution logicielle Vitam permet d’importer deux référentiels correspondant à des fichiers CSV :

* le référentiel des règles de gestion,
* le référentiel des services agents

Un référentiel au format CSV doit contenir :

* une ligne contenant les titres des champs acceptés. Dans la solution logicielle Vitam, les titres sont imposés :
      * « Identifier », « Name » et « Description » pour le référentiel des services agents,
      * « RuleId », « RuleType », « RuleValue », « RuleDescription », « RuleDuration » et « RuleMeasurement » pour le référentiel des règles de gestion ;
      * une à plusieurs lignes contenant les valeurs correspondant aux attendus du titre de colonne.
