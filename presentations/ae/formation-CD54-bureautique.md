
class: center, top
background-image: url(images/logo-rhizome-data.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.footnote[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data]

<!--slide 1-->
### Réaliser des versements bureautiques

---
name: programme


layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat

.footnote[
      [Programme](#programme)]

---
<!--slide 2-->
### Objectifs

#### Réaliser des versements manuels (ou semi-automatisés) d'arborescences
3 scenarii :

* Paul du bureau du contentieux de la DIMMO change de service et veut archiver ses dossiers clos
* Christine, secrétaire du bureau de la coordination administrative veut archiver un dossier d'opération immobilière clos
* Oussem, chef du service veut archiver les dossiers clos des 5 dernière années

--
<!--slide 3-->


1. [Analyser](#analyser) la production à partir d'une grille de d'analyse
2. [Décrire](#decrire) une arborescence et produire un rapport d'audit
3. [Réaliser](#realiser) des bordereaux
4. [Verser et ou éliminer](#sae) dans un SAE

---
<!--slide 4-->
### Analyser la production en amont

- Quelle grille d'analyse de la production ?
- Comment enrichir un tableau de gestion pour la production électronique ?
- Comment décrire une arborescence et produire un rapport d'audit ?

---
name: analyser
<!--slide 5-->

### Analyser la production courante

L'objectif est d'identifier :
* les éléments à conserver,
* les règles de tri,
* les éléments à supprimer à l'issue de la DUA,
* les règles de communicabilité,
* les éléments liés à la pérennisation.


---
<!--slide 6-->

### Analyser la production courante

Mais aussi :
* les règles de nommage à implémenter,
* les formats de fichiers à convertir,
* les données personnelles à décrire,
* la valeur probante à conserver,
* les arborescence à réorganiser.


---
<!--slide 7-->

### Analyser la production courante
#### Exemple de grille de collecte

Préciser le type de mode de production

![tableur environnement de création](images/grilleCollecte_1.png)

---
<!--slide 8-->

### Analyser la production courante
#### Exemple de grille de collecte

Décrire les métadonnées disponibles

![tableur environnement de création](images/grilleCollecte_2.png)

---
<!--slide 9-->

### Analyser la production courante
#### Exemple de grille de collecte

Identifier la localisation des objets données

![tableur environnement de création](images/grilleCollecte_3.png)

---
<!--slide 10-->

### Analyser la production courante
#### Exemple de grille de collecte

Caractériser les métadonnées de gestion archivistiques

![tableur environnement de création](images/grilleCollecte_4.png)

---

<!--slide 11-->


### Analyser la production courante
#### Exemple de grille de collecte

Caractériser les usages associés

![tableur environnement de création](images/grilleCollecte_5.png)

---

<!--slide 12-->

### Analyser la production courante
#### Remplir la grille de collecte


[Formulaire](https://airtable.com/shrYoeudCMuJhgkjx)

---
<!--slide 13-->

### Analyser la production courante
#### Visualiser la production
Par exemple avec WindirStat

![arborescence](images/k4dirStatDimmo.png)

---
<!--slide 14-->
### Analyser la production courante
#### Visualiser la production
Par exemple avec WindirStat

![treemap](images/k4dirStatDimmoTreemap.png)


---
name: decrire
<!--slide 15-->

### Décrire une arborescence

En accompagnement à la production courante ou dans l'optique d'un versement, la description d'une arborescence bureautique peut-être effectuée à l'aide d'outils numériques : [voir le billet du SIAF](https://siaf.hypotheses.org/1033)

- Archifiltre
- Octave
- Resip

![Comparatif outils](images/comparatifOutilsAnalyse.png)

---
<!--slide 16-->
### Décrire une arborescence
#### Maîtriser l'outil Archifiltre

Outil créé dans le cadre du programme « entrepreneurs d’intérêt général » (porté par la DINSIC) et porté par les archivistes du Ministère des Affaires sociales

objectifs de l'outil :

* proposer à tout utilisateur de fichiers bureautiques un outil de visualisation d’arborescences complètes afin de pouvoir les appréhender rapidement en vue de les décrire, les organiser, les trier et aussi les enrichir en apportant de la contextualisation et de la qualification aux documents ;
* réaliser des audits sur la production bureautique

Le Wiki d'ArchiFiltre : https://github.com/SocialGouv/archifiltre/wiki/Wiki-Archifiltre

---
<!--slide 17-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

L'outil est téléchargeable https://archifiltre.fabrique.social.gouv.fr/

Une fois téléchargé il se lance comme n'importe quelle application

![accueilArchifiltre](images/accueil-Archifiltre.png)

---
<!--slide 18-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

On peut commencer alors par un simple glissé-déposé

![Archifiltre après dépot d'un répertoire](images/archifiltreLaunch.png)

---
<!--slide 19-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

Nommer son répertoire et renseigner une description et les mots clés

![Archifiltre renseigner les informations sur les répertoires du dossier](images/Archifiltre3.png)

![Archifiltre renseigner les informations sur les répertoires du dossier](images/Archifiltre4.png)

Enregistrer

---
<!--slide 20-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

Visualiser la production et les modifications apportées grâce à l'export au format CSV

L'export CSV sera une annexe du rapport d'audit

<!--![Archifiltre l'export CSV](images/Archifiltre5.png)-->


![Archifiltre l'export CSV](images/Archifiltre7.png)


---
<!--slide 21-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

Faire un rapport d'audit

![Archifiltre rapport d'audit](images/Archifiltre5.png)


---
<!--slide 22-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

Le rapport d'audit permet d'avoir des éléments sur :

- La volumétrie
- les types de fichiers
- la présence ou non de doublons

![Archifiltre rapport d'audit](images/Archifiltre6.png)

Le rapport élaboré à partir d'Archifiltre permet d'avoir une matrice pour rédiger le rapport final à remettre au service producteur

---
<!--slide 23-->
### Décrire une arborescence
#### Utiliser l'outil Archifiltre

Le rapport d'audit final pourrait contenir en plus des éléments fournis par Archifiltre les informations suivantes :

- Fichiers à renommer
- Temps de travail des archivistes sur la production documentaire avant intégration dans un SAE
- Intérêt ou non de réaliser un profil SEDA
- Part des documents à conserver ou non
- Bilan qualitatif de la production en conclusion et liste des actions à mener

---
<!--slide 24-->
### Décrire une arborescence
#### Archifiltre: cas d'usages

* Cas 1 – Une arborescence mal pensée

--
<!--slide 25-->

Que faire ? Analysez le répertoire en ayant connaissance de l’organigramme et des grandes fonctions de la structure

--

* Cas 2 – Des doublons de répertoires au sein de l’arborescence

--
<!--slide 26-->

Que faire ?

Où sont les fichiers définitifs ? Est-ce qu’on retrouve l’intégralité des fichiers dans la plus grosse arborescence ? Quelle arborescence fait référence ?

--
<!--slide 27-->
* Cas 3 – Identifier les dossiers clos

--
<!--slide 28-->

Que faire ? Analyser à l'aide de la visualisation chronologique

---
<!--slide 29-->
### Décrire une arborescence
#### L'outil Archifiltre et les autres outils à disposition

L'export ReSIP permet de créer un paquet à intégrer dans ReSIP.

![Archifiltre ReSIP](images/Archifiltre8.png)

---
<!--slide 30-->
### Décrire une arborescence
#### l'outil Archifiltre et les autres outils à disposition

Il n'y a pas de moyen d'intégrer les éléments saisis ou modifiés avec Archifiltre vers Octave

![Archifiltre lien Octave ](images/Archifiltre9.png)

---
<!--slide 31-->
### Décrire une arborescence
#### Maîtriser l'outil Resip

objectifs de l'outil :

* importer des structures arborescentes d’archives et les fichiers qui les représentent ;
* enrichir les métadonnées de description et de gestion des unités archivistiques (ArchiveUnits) ainsi que les métadonnées techniques des fichiers (BinaryObjects)
* exporter les structures arborescentes d’archives et les fichiers qui les représentent sous une forme importable dans la solution logicielle Vitam ou sous la forme d’une arborescence de fichiers.

---
<!--slide 32-->
### Décrire une arborescence
#### Utiliser l'outil Resip

L'outil est disponible pour les systèmes d'exploitation Linux et windows et nécessite la présence d'un environnement d'exécution java (JRE)

Le code peut être exécuté de 3 manières différentes :

* saisie d'un ligne de commande :
* glisser-déposer une arborescence de dossiers
* lancement d'une interface graphique

---
<!--slide 33-->
### Décrire une arborescence
#### Utiliser l'outil Resip en ligne de commande


* saisie d'un ligne de commande :
    * commande :
        * java -jar target/resip-2.0.0-SNAPSHOT-shaded.jar -d /home/pascal/Nextcloud/01-docs-admin/ -g dossieradminRD.zip -h -i -n -x -v ERROR -w /home/pascal/.Resip/

---
<!--slide 34-->
### Décrire une arborescence
#### Utiliser l'outil Resip en glisser-déposer

* [sous environnement windows] glisser-déposer une arborescence de dossiers sur le fichier resip.bat ![capture d'écran](images/dragdroprsipbat.jpg)

---
<!--slide 35-->
### Décrire une arborescence
#### Utiliser l'outil Resip avec l'interface graphique

* lancement d'une interface graphique
      * sous windows : L’interface graphique de la moulinette ReSIP peut être ouverte en double-cliquant sur le fichier exécutable portable Resip.exe
      * sous linux : java -jar target/resip-2.0.0-SNAPSHOT-shaded.jar   

> Attention :

* l’opération ne pourra pas s’exécuter si le fichier nommé « exportContext.config » est absent du répertoire nommé « config » ;
* l’en-tête du manifeste reprend les options et métadonnées déclarées dans le fichier nommé « exportContext.config ».
Les valeurs des champs de l’en-tête peuvent être directement modifiées dans ce fichier.

---
<!--slide 36-->
### Décrire une arborescence
#### Utiliser l'outil Resip : décrire des unités d'archives

![décrire avec Resip](images/ResipDescription.png)

---
<!--slide 37-->
### Décrire une arborescence
#### Utiliser l'outil Resip : décrire des objets données

![décrire avec Resip](images/ResipDescriptionObjet.png)

---

<!--slide 38-->
### Décrire une arborescence
#### Utiliser l'outil Resip : configurer l'export

![configurer Resip](images/ResipExportMD.png)

---


<!--slide 39-->
### Décrire une arborescence
#### Maîtriser l'outil Octave

Octave est une application à installer sur un poste individuel.

Elle a été créée à partir d'un logiciel nommé Docuteam-Packer

Elle permet de décrire des arborescences mais également de produire des paquets à verser ou des bordereaux au format pdf.

---
<!--slide 40-->
### Décrire une arborescence
#### Utiliser l'outil Octave

Au lancement de l'application, on peut importer des SIP ou des dossiers

![accueil Octave](images/AccueilOctave.png)

---
<!--slide 41-->
### Décrire une arborescence
#### Utiliser l'outil Octave

Plusieurs champs de description sont disponibles pour décrire des dossiers ou des fichiers

![Décrire avec Octave](images/AccueilOctaveDescription.png)


---
<!--slide 42-->
### Décrire une arborescence
#### Utiliser l'outil Octave

On peut prévisualiser le contenu de certains fichiers (PDF, bureautique, images)

![prévisu Octave](images/AccueilOctavePrevisu.png)

---
<!--slide 43-->
### Décrire une arborescence
#### Utiliser l'outil Octave

3 usages :

* créer un paquet vide et ajouter des données manuellement
* importer une arborescence afin de la décrire et de la transformer en SIP
* créer un modèle et l'utiliser pour créer d'autres SIP

---
<!--slide 44-->
### Décrire une arborescence
#### Utiliser l'outil Octave

![import arbo](images/OctaveImportArbo.png)

![Créer depuis modèle](images/OctaveCreerModele.png)

---
<!--slide 45-->
### Décrire une arborescence
#### Utiliser l'outil Octave


La fenêtre de description des objets

![Decrire avec Octave](images/OctaveDecrire.png)

---
<!--slide 46-->
### Décrire une arborescence
#### Utiliser l'outil Octave

Octave permet d'effectuer des opérations de renommage en masse

On peut également ajouter des préfixes et/ou des suffixes aux noms de fichiers.

Le nom original de ceux-ci est conservé dans les métadonnées de traçabilité du SIP.

Le renommage donne aussi la possibilité de fixer une longueur maximale pour les noms de fichier.

Les préfixe, suffixe et longueur maximale autorisée pour les noms de fichiers sont configurables

---
<!--slide 47-->
### Décrire une arborescence
#### Utiliser l'outil Octave : niveaux de description

Il est possible de rattacher des niveaux de description aux éléments de l’arborescence.

Ces niveaux apparaîtront dans les métadonnées de description

![Decrire avec Octave](images/OctaveNiveauDesc.png)

---
<!--slide 48-->
### Décrire une arborescence
#### Utiliser l'outil Octave : dédoublonner
Dédoublonnage
La fonctionnalité de dédoublonnage permet d’identifier et d’éliminer des fichiers aux contenus identiques.

L’identification des doublons est basée sur l’empreinte des fichiers : des fichiers identiques dont les noms sont différents seront donc tout de même identifiés comme doublons.

![Decrire avec Octave](images/OctaveDoublon.png)

Cette fenêtre fait apparaître les exemplaires multiples des fichiers et permet de cocher dans la case de gauche les fichiers à éliminer.

Une fois les cases cochées, cliquer sur le bouton « Supprimer » pour supprimer les fichiers sélectionnés et revenir à l’arborescence.


---
<!--slide 49-->
### Décrire une arborescence
#### Utiliser l'outil Octave : export


OCTAVE permet d'exporter plusieurs types de documents liés à la traçabilité et aux traitements effectués sur le paquet :

* rapports et bordereaux (récolement, élimination, restitution)
* export de métadonnées (EAD, csv)
* export SIP SEDA (V1.0 ou v2.1)


---
<!--slide 50-->
### Décrire une arborescence
#### Synthèse des interactions

![import/Export](images/interactionOutilsBureautique.png)

---
name:realiser
<!--slide 51-->


### Réaliser des bordereaux

* papier avec Octave
* numérique avec Octave ou Vitam

---
<!--slide 52-->

### Guide mise en oeuvre

#### Groupe 1 Audit et Évaluation DUC

1. définir l'objectif de l'audit 
1.1 donner des préconisations aux services
1.2 auditer un vrac en vue d'un traitement


1.1 outil utilisé : archifiltre (windirstat (croque l'arborescence) et alldup (fait la liste des doublons)

**Quelles préconisations ?**:
   

* Introduction (à partir d'aucun outil)
* Nom du producteur
* Nom du service auditeur
* Date de l'audit
* Espaces audités

---

#### Groupe 1 Audit et Évaluation DUC

**Chiffres clés** (Archifiltre, Windirstat)

- les éléments de volumétrie global (dossiers et fichiers) ? ou des dossiers ? 
- structuration du niveau de profondeur, nombre de fichier/dossier à la racine
- statistiques globales sur les formats (reprendre audit graphique archifiltre)
- longueur des caractères
- dossiers les plus volumineux à la racine
- dates extrêmes 

---
#### Groupe 1 Audit et Évaluation DUC

**Préconisations** (Archifiltre, Alldup)

Gestion des risques :

point de vigilance : sécurité des espaces 

- formats utilisés (formats exotiques, formats non reconnus)
- longueur des caractères

**Optimisation de l'arborescence**

- harmoniser / organiser le plan de nommage
- purger les doublons (propriétaire du fichier)
- nombre de dossiers divers, poubelle, brouillons, à trier + dossiers archives

---
#### Groupe 1 Audit et Évaluation DUC

**Gestion de l'archivage**

- fréquence d'utilisation (date de dernière modification)
- fichiers zip
- volumétrie à verser ou à éliminer

---

#### Groupe 2 DUA, Trier et Éliminer 

Savoir si on travaille sur des archives intermédiaires ou définitives (définir le périmètre)

Prérequis : S'appuyer sur un tableau de gestion préexistant et des correspondants formés dans les services

Récupérer l'audit de la phase 1

Récupérer paquet dans Octave et Archifiltre

Contrôle des informations, par sondage? (arborescence, nommage, dates, formats de fichier)

Identifier les doublons (Octave) et identifier fichiers à éliminer immédiatement (tag trash)

Caractériser les sorts finaux ( renseigner métadonnées dont DUA des documents par encore éliminables mais à terme)

Identifier les éliminables (tag trash dans Octave)

Bordereaux élimination (pour signature) 

Mise en œuvre de l'élimination : qui ? Comment ? Quelles modalités ? Traces ?

Réorganiser la production : voir groupe 1

---

#### Groupe 3 Traiter et Verser Sort Final -> conserver

**Étape 1** : création des listes d'autorité (référentiels) dans le dossier Configuration d'Octave

- liste des communes de Meurthe-et-Moselle (pour indexation géographique)

- liste des services versants et des services producteurs (2 colonnes : code + libellé)

- liste des formats de fichiers utilisés (base PRONOM des Archives nationales de Grande-Bretagne)

--> normaliser ces listes

---

#### Groupe 3 Traiter et Verser Sort Final -> conserver

**Étape 2** : importer arborescence propre de fichiers dans Octave

**Étape 3** : au niveau haut de l'arborescence, on note les métadonnées générales :

- élaborer un titre courant pour l'instrument de recherche
- dates extrêmes --> repasser dans archifiltre pour trouver les dates extrêmes
- description du producteur
- description du contenu
- langue utilisée
- communicabilité
- documents contenant des données personnelles
- descripteurs de niveau haut
- identité du rédacteur de l'instrument de recherche
- identité du responsable du service d'archives
- code identifiant du service d'archives

---

#### Groupe 3 Traiter et Verser Sort Final -> conserver

**Étape 4** : au niveau intermédiaire, on précise:

- niveau de description (titres de niveau intermédiaire)
- descripteurs au niveau des titres intermédiaires
- description du contenu et de la finalité des sous-dossiers

**Étape 5** : au niveau bas, intégrer :

- une indexation de mots matière, de typologies documentaire (ces deux listes sont automatiquement paramétrés dans Octave d'après le thésaurus W), de noms d'organisme et de descripteurs géographiques (liste d'autorité).
- vérifier que tous les formats de fichier sont pérennes (si non, faire migrer les données vers un format pérenne) [peut se faire automatique en fonction du paramétrage du SAE lors du versement dans le SAE]

**Étape 6** : création d'un paquet SIP en SEDA (version 1.0 ou 2.1 en fonction du SAE choisi)

**Étape 7** : versement du SIP dans le SAE pour archivage définitif

---

### Annexe
Le [paramétrage de Resip](annexe-formation-bureautik.html)
