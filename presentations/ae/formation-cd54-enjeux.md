class: center, top
background-image: url(images/logo-rhizome-data.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.footnote[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data]

### Les enjeux techniques de la gestion des archives

---
name: programme

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat

---
name: programme

### Programme

- [culture](#culture) de la donnée
- [cartographie](#cartographie) du patrimoine de données
- [dématérialisation](#dematerialisation) et archivage
- [services](#service) de la donnée

---

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programme)]

---
### La culture de la donnée ?

Historiquement considérée comme une commodité, la donnée est révélée comme un actif précieux.

Mais cet actif n’est pas un objet statique : la donnée est transformée, effacée, archivée ... et connaît ainsi un « **cycle de vie** ». La maitrise de la **qualité** et de l’utilisation de ces données massives, durant tout leur cycle de vie, consiste à protéger et à **valoriser** un patrimoine stratégique.

![Cycle de vie de la donnée](images/cycledevie-donnees.png)

---

### La culture de la donnée

Pour faire face à ce défi , les collectivités locales font par de leurs déficit de ressources...

![dimensionnement des équipes](images/dimensionequipe.png)

... et de leur manque de formation

![fromation des équipes](images/formationdonnees.png)

.credit[[Etude de la FNCCR 2019](http://www.fnccr.asso.fr/article/rapport-final-etude-cycle-de-la-donnee-et-transformation-des-si/)]

---
### La culture de la donnée

![langage commun](images/formationData.jpg)



---
name: cartographie

### Pourquoi cartographier les données ?

![cartoFlux](images/cartoFlux.jpg)

---

### Pour qui et pourquoi cartographier les données ?

![Stratégie orientée data](images/strategieData.png)

---

### Les outils pour cartographier les données

#### Le répertoire des informations publiques (RIP)
##### Qu'est-ce qu'un répertoire des informations publiques ?

Une obligation légale, régie par l'article L322-6 du code des relations entre le public et l’administration :
- Les administrations qui produisent ou détiennent des informations publiques tiennent à la disposition des usagers un répertoire des principaux documents dans lesquels ces informations figurent.
- Elles publient chaque année une version mise à jour de ce répertoire.

==> répertoire qui recense les documents administratifs communicables contenant des informations publiques

---

### Les outils pour cartographier les données
#### RIP : base de données

Le répertoire des informations publiques du Ministère des Finances :
- un répertoire sous forme de base de données

![RIPMEF](images/RIPMEF.png)

https://www.economie.gouv.fr/cedef/repertoire-des-informations-publiques

---

### Les outils pour cartographier les données
#### RIP : fichier csv

Le RIP de la commune de Saint-Claude : un fichier CSV
![RipSaint-Claude](images/rip-saint-claude.png)

https://www.data.gouv.fr/en/datasets/registre-des-informations-publiques-1/


---
### Les outils pour cartographier les données
#### RIP : portail open data

Un début de RIP pour les données publiques : le catalogue open data
mais il manque un jeu de données qui serait le catalogue des données ouvertes...

![opendata grand Nancy](images/opendataNancy.png)

---
<!-- slide 12 -->

### Les tableaux de gestion des archives

#### A quoi sert un tableau de gestion ?

#### Etablir un état de la production en relation avec les missions du producteur en intégrant les règles du cycle de vie des documents : 

Que doit-on garder ? Que doit-on détruire ? Que et quand doit-on verser aux Archives et Au bout de combien de temps ? Pourquoi ?

> **Cartographie par les règles de tri et les typologies**

![cartoFlux](images/tableaudegestionDelphineFournier.png)

---

### Les outils pour cartographier les données
#### Tableau des activités de gestion

« procédure fructueuses » produit  dans  le  cadre  de  l’activité  « marchés publics »,  qui est  l’un  des  aspects  de  la  « Gestion  de la commande publique »  qui  elle-même  fait partie de la « Gestion des ressources »

![organigramme DIFAJE](images/orgCmdPub.png)

Le tableau de gestion permet de décrire les typologies documentaires, le contexte de production, la durée d’utilisation et le sort final réservé aux documents

---
### Les outils pour cartographier les données
#### Tableau de gestion : exemple

.reduite[![Tableau de gestion CmdPub](images/TdGCmdPubcd54.png)]

---
### Les outils pour cartographier les données
#### Et dans l'univers électronique ?

Rajout d'information sur les **supports de gestion** (logiciels, base de données), de **stockage** (localisation, typologie) et les **formats** de fichiers (typologie, pérennité)

|Type de dossier, pièces principales|Support de stockage|Format de conservation|
| ------------- |:-------------:|-----|
|**Gestion des marchés publics** <br>Procédures fructueuses <br>Acte d'engagement|Serveur NAS (IP)| PDF/A 1    |
|Profil acheteur|Base de données AWS,Oracle SQL 10| SIARD -> CSV       |

---
### Les outils pour cartographier les données

#### Tableau de gestion : exemple


.reduite[![Tableau de gestion Patrimoine immobilier](images/TDG-elect54.png)]

---
### Les outils pour cartographier les données
#### Et dans l'univers électronique ?
Rajout d'information sur les **formats** de fichiers (typologie) augmentés des **délais de communicabilité** et  des éléments permettant la **préservation de l'information numérique (PIN)**

|Type de dossier, pièces principales|Format de conservation|PIN|Communicabilité|
| ------------- |:-------------:|-----|---------|
|**Dossiers relatifs aux biens immobiliers loués par et pour le Département** <br>État annuel des mises à disposition de locaux à titre gracieux ou quasi gracieux, pour un usage ponctuel ou discontinu|PDF ?| [.reduite2[![Pronom PDF](images/PDF-pronom.png)]](http://www.nationalarchives.gov.uk/PRONOM/Format/proFormatSearch.aspx?status=detailReport&id=1266)   | Librement communicable |

---
### Les outils pour cartographier les données : enjeux
#### Gestion de la production numérique

Dans l'univers éléctronique la localisation devient **virtuelle**, la **granularité** plus fine et l'**authenticité** est garantie par les **métadonnées**

![Gestion RM](images/gestionRM.png)

---
### Les outils pour cartographier les données : enjeux
#### Gestion de la production numérique

C'est le contexte de production qui doit être **explicité** et décrit de manière **portable**

![la métadonnée par la métaphore du vin](images/metaVin.png)

---
### Les outils pour cartographier les données : enjeux
#### Cartographie de la production des données

Les cartographies des **processus** et des **applications** doivent être complétées par une **cartographie des flux et des traitements de données**

![carte du monde babylon](images/babylonianworldmap15.jpg)

---

### Les outils pour cartographier les données : enjeux
#### Cartographier pour dématérialiser

La **description des processus** est un préalable aux projets de dématérialisation.

C'est une bonne occasion pour faire **améliorer** l'enchaînement des activités et **automatiser les tâches** à faible valeur ajoutée (complétude, validité, notification)

![impact démat](images/Dematerialisation-tous-flux-objectifs-impacts-organisation-F.jpg)

---

### Les outils pour cartographier les données : enjeux
#### Cartographie de la gestion par projet

Les activités **matricielles** (projets) transcendent les organigrammes.

La description de la production documentaire doit donc s'appuyer sur les **fonctions** et les **relations**

![Fluxus](images/FLUXUS6.jpg)

---

### Les outils pour cartographier les données : enjeux
#### Connaissance de la production

Une bonne connaissance de la production permet de **capitaliser** sur les activités réalisées (modèles de rapports, documents) et de **valoriser l'action administrative** (intranet, expositions, valorisation)

![gestion de la connaissance](images/knowledge-management-tribune.png)

---

### Les outils pour cartographier les données : enjeux
#### Valorisation : patrimoine et activités

Le **traitement et le classement** des sources administratives permet aux historien.nes, journalistes ou graphistes de **remettre en perspective**.

Les tableaux de bord et rapports d'activité permettent d'**évaluer les impacts des politiques publiques** et de mieux répondre aux **attentes des usager.e.s**.

.pull-left[![rapport cmd pub](images/rapportCmdPubcd54.png)]
.pull-right[![longwy passé sidérurgique](images/WO2083.jpg)]

---

### Synthèse des enjeux de la cartographie

#### Convergence des besoins métiers
* décrire pour archiver et mettre en oeuvre les règles de tri
* décrire pour protéger les données personnelles
* décrire pour mettre à disposition les données (opendata)
* décrire pour urbaniser le système d'information

#### Convergence des pratiques
* renseigner les règles de communicabilité
* localiser les sources et les destinations
* sécuriser et partager l'information

---

### Questions ?

.reduite[![questions](images/questions.jpg)]

---
name: dematerialisation

### Dématérialisation et archivage
### Automatiser des versements

A partir des tableaux de gestion devenus **modèles de flux**, il est possible d'**automatiser la capture** des (méta)-données essentielles en dessinant des **modèles de bordereaux** : les profils

![Schéma SEDA](images/allInOneSeda.png)

---

### Dématérialisation et archivage
#### Automatiser des versements

Ces profils peuvent venir enrichir les connecteurs applicatifs existant ou normaliser la production dans le cadre de l'urbanisation du système d'information

![gare de triage](images/gareMetz.jpg)

---

### Dématérialisation et archivage
### Urbaniser le cycle de vie des données

Des outils de type ESB (un **bus pour optimiser** les transports des données) font leur apparition des les systèmes d'informations publics pour conjuguer les exigences de **dématérialisation, interconnexion et ouverture**

![plu](images/pluNancy.jpg)

---

### Dématérialisation et archivage
#### Cartographier les traitements de données

Depuis la mise en place du RGPD, la collecte et la conservation des données doivent faire l'objet de description et de supervision. Les finalités de traitement doivent être explicités (consentement) et les données doivent être supprimées à l'issue de leurs finalités (traitement ou conservation)

![rgpd cnil](images/rgpd-cnil.jpg)

---

### Dématérialisation et archivage
#### RGPD et archivage

Ces traitements permettent d'identifier des versions différentes des mêmes données et d'y associer les règles de communicabilité et de sort final appropriées.

![ouvrir des données](images/donneesOuvertes.jpg)

---
### Dématérialisation et archivage
#### Open data et archivage

Les flux d'alimentation open data et archviage peuvent être mutualisés et répondrent à des finalités complémentaires.

![convergence open data et archivage](images/archivage-electronique-open-data-schema-1024x630.png)

---
### Dématérialisation et archivage

### Marchés publics et Patrimoine : source de données

**Marchés publics** :

* **Open data** : L’arrêté du 14 avril 2017 relatif aux données essentielles dans la commande publique définit le cadre de l’ouverture des données des marchés publics. Ainsi, au 1er octobre, **les données de tous les marchés supérieurs à 25 000 euros** devront être publiées en Open Data sur le profil d’acheteur.
* **Archivage** : les marchés notifiés doivent être conservés 10 ans (offre retenue) ou 5 ans (candidatures non retenues). Un tri peut être effectué pour conserver les dossiers les plus intéressants.

Les données essentielles annuelles des marchés publics sont des jeux de données interressant à conserver sur le long terme pour réaliser des statistiques et pour avoir une vision de l'ensemble des actions de la commande publique.

**Patrimoine** :

* **Open data** : la liste du patrimoine bâti et non bâti ainsi que les cessions immobilières sont des données publiques. Les données de consommation énergétique sont à intégrer dans les **bilans carbone** qui doivent être publiés sur le site de l'ADEME.
* **Archivage** : les dossiers de gestion du domaine public, d'acquisition ou de cession ainsi que les déclarations d'utilité publique doivent être versées aux Archives départementales.

Les listes sont des jeux de données utiles sur le long terme pour permettre des comparaisons et pour réaliser des statistiques

---
name: service

### Services de la donnée

#### Mutualiser permet de mieux répondre aux attentes et contraintes réglementaires

![collectivisation](images/collectivisation.png)

---
### Services de la donnée

Combiner les aspects liés à la **gestion technique** de l'information avec ceux associés à la **gestion des contenus** autour des **enjeux du pilotage** de l'information permet de bâtir une stratégie de **gouvernance partagée**

![gestion gouvernance](images/gestiontransverseinformation.jpg)

---
### Services de la donnée

Les partenaires institutionnels enntre eux, les collectivités et leurs usagers, les acteurs publics et leurs prestataires de services s'échangent de plus en plus d'informations. Ce mouvement résulte à la fois de la capacité de **circulation** de l'information numérique, dans l'**accroissement** de la place des prestataires privés dans la réalisation des missions de sevice public mais également d'une demande de **transparence et de partage** de la société civile

![contexte](images/cerveauContexte.jpg)

---
### Services de la donnée

A moyens constants (voire en diminution) comment faire face à la nécessité de répondre à ces besoins et de développer des compétences numériques liées aux données ?

![ça fait peur](images/peur.jpg)

---
### Services de la donnée

![stock vrac](images/stock-archives.jpg)

---
### Services de la donnée
#### Renforcer les compétences et les missions associées à la gestion des données

![métiers data](images/metiersData.jpg)

---
### Services de la donnée : dream team

![dream team](images/dreamteam.png)

---

### Service de la donnée : marchés publics

![cycle marché](images/cycleMP.png)

---

### Merci pour votre attention

#### Questions ?

[Support en PDF](https://rhizome-data.gitlab.io/formations/presentations/export-pdf/formation-CD54-enjeux.pdf)

Contacts : 
* [Delphine Jamet : delphine@rhizome-data.fr](mailto:delphine@rhizome-data.fr)
* [Pascal Romain : pascal@rhizome-data.fr](mailto:pascal@rhizome-data.fr)
