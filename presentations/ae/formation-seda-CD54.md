class: center, top
background-image: url(images/logo-rhizome-data.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.footnote[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data]

### Standard d'échange de données pour l'archivage (SEDA)

---
name: programme

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat

### Programme

- comprendre l'[intérêt](#pourquoi) du SEDA
- comprendre la [structuration](#structuration)
- comprendre la nature et la composition des [messages](#messages)
- comprendre la [gestion](#gestion) des métadonnées
- maîtriser la [création](#creation) de profil SEDA
- Comprendre les prérequis pour l'[automatisation]() des versements

---

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programme)]

---
name: pourquoi

<!-- slide 2-->

### Pourquoi le SEDA ?

Faciliter les **processus d'archivage** électronique en les **normalisant** et en permettant leur **interopérabilité**.

.reduite[![Interopérabilité](images/desertB.jpg)]

---
<!-- slide 3-->
### Pourquoi le SEDA ?
Il est **obligatoire d'encapsuler les éléments du bordereau de versement électronique** dans un message au **format SEDA** mais il n'est pas toujours nécessaire de rédiger un profil pour réaliser un versement d'archives électroniques

Les **profils** sont adaptés aux versements dits **sériels** souvent en provenance d'**applicatifs métiers** ou de GED et moins à la **production** de type **bureautique**.

.center[![types versement](images/typeVersement.png)]

---
<!-- slide 4-->
### SEDA : usages

**Bordereau SEDA** : description du contenu d'un versement électronique

**Profil SEDA** : description d'un modèle de versement électronique, permet aux machines de valider la conformité d'un versement

**Message SEDA** : traces des processus d'archivage

Ces données (ou documents) sont composés de textes ou de codes et de balises qui explicitent leur sémantique

.center[![exemple SEDA](images/exSEDA.png)]

---
<!-- slide 5-->
### Historique du SEDA ?

La rédaction de cette grammaire est à l'initiative du Service interministériel des Archives de France :

- **mars 2006**, publication de la **version 0.1** suivie d'une instruction DITN/RES/2006/001 au 8 mars 2006
- janvier 2010 publication de la version 0.2
- septembre **2012** publication de la **version 1.0**
- **2014** : parution de la norme Afnor **NF 44-022 MEDONA** : Modélisation des échanges de données pour l'archivage
- décembre **2015** publication de la **version du standard 2.0**, conforme à la norme MEDONA
- **2017** : parution de la norme **ISO 20614 DEPIP** : Information et documentation -- Protocole d'échange de données pour l'**interopérabilité et la préservation**
- **juin 2018** publication de la révision 2.1 : Interface web de consultation des schémas de la [**version 2.1**](https://francearchives.fr/seda/api_v2-1/seda-2.1-main.html)

![Chronologie du SEDA](images/chronoSEDA.png)

---
<!-- slide 6-->
### Pourquoi le SEDA ?

> Le standard d'échange de données pour l'archivage **modélise les différentes transactions** qui peuvent avoir lieu **entre des acteurs** dans le cadre de l'**archivage de données**.

> Il précise les **messages** échangés ainsi que les **métadonnées** à utiliser pour **décrire, gérer et pérenniser** l’information.

![illustration du pourquoi du SEDA](images/allInOneSeda.png)

---
<!-- slide 7-->
### Pourquoi le SEDA ?

Il vise à :

- proposer un **cadre d'interopérabilité** pour la dématérialisation des processus d'archivage
- **structurer les messages** renforcant la valeur probante des archives
- **relier des unités de description** au sein du processus de versement

![schem Seda ](images/schemaSeda1.png)

---
name: structuration

<!-- slide 8-->

### Composition du SEDA

Le SEDA permet de **composer des messages entre les acteurs** des processus d'archivage.

Ces messages sont composés d'**en-têtes** et d'un **corps** qui rassemble les métadonnées permettant la **description** (technique, archivistique et administrative) des éléments contenus dans les échanges.

Les messages échangés reflètent les **activités** mettant en relation les **acteurs** des **processus d'archivage**. Leur conservation contribue à la **tracabilité des relations** entre producteurs et services d'archives.  

.center[![exemple transfert](images/archiveTransfertUnitArchives.png)]

---
<!-- slide 9-->
### Composition du SEDA

Le bordereau est l'acte "signé" du **transfert de responsabilité**

.center[![exemple transfert](images/archiveTransfert.png)]

---
<!-- slide 10-->
### Composition du SEDA
#### Renforcer la valeur probante des échanges


- les messages d'accusé (réception, validation) sont les **preuves** de la transaction
- les demandes de communication et d'élimination peuvent être signées par un **service de contrôle**

![accusé de réception](images/msgAck_seda.png)

---
<!-- slide 11-->
### Composition du SEDA
#### Favoriser l'automatisation et la normalisation des bordereaux

Une référence à un profil SEDA dans un bordereau permet de **valider sa composition** lors du versement

Cette référence fait partie des **métadonnées administratives**

**ArchivalProfile** : Profil d'archivage applicable aux ArchiveUnit

```XML

<Contains>
      <ArchivalAgencyArchiveIdentifier>saemref-test-ext-000000217
      <ArchivalProfile>saeCD54/000011477
      <DescriptionLanguage listVersionID="edition 2009">fr
      <DescriptionLevel listVersionID="edition 2009">collection
      <Name>l'unité documentaire chapeau du versement
</Contains>      

```

---
<!-- slide 12-->

### Composition du SEDA
#### Transférer : la structuration des messages


![illustration de la structuration du message](images/msgTransfert_seda.png)

---
<!-- slide 13-->

### Composition du SEDA
#### Transférer : la structuration des messages

- **encapsuler** dans un bordereau le contenu du versement
- décrire les **acteurs impliqués** (producteur, versant, archives)
- décrire les **propriétés techniques** des objets données
- **décrire le contenu** des unités de description

.reduite2[![illustration de la structuration du message](images/msgTransfert_seda.png)]

---

<!-- slide 14-->

### Composition du SEDA

#### Les acteurs du SEDA

.pull-left[
![Les 5 acteurs du SEDA](images/acteursSEDA2.png)
]

.pull-right[
Les acteurs sont au nombre de cinq :

- le service producteur,
- le service versant,
- le service d'archives,
- le service de contrôle,
- le demandeur d'archives.

Ils peuvent avoir plusieurs rôles

]

---

<!-- slide 15-->

### Valider les échanges entre les acteurs

Les messages XML (Request / Reply / Notification / Acknowledgement) doivent être conformes à des schémas de validation (RNG ou XSD)


![traçabilité des échanges](images/structureMsg.png)

---

<!-- slide 16-->

### Composition du SEDA

#### Les transactions du SEDA

.pull-left[

![Les transactions dans le SEDA](images/transactionsSEDA.png)
]

.pull-right[
**Les transactions** sont au nombre de six :

- la demande de transfert,
- le transfert,
- la modification,
- l'élimination,
- la communication,
- la restitution.

]


---
<!-- slide 17-->
### Composition du SEDA

#### Que faut-il pour écrire un bordereau SEDA ?

Outils ?

Documentation ?

Interlocuteur.rice.s ?
--
<!-- slide 18-->

**Outils**: éditeur de texte, éditeur XML, Resip, Sherpa, As@lae, etc..

**Documentation** : dictionnaire des balises ? Tableau de gestion ? Fonds à verser ? procédures

**Interlocuteur.rice.s** : métier, informatique, DPO (?), archiviste

---

<!-- slide 19-->

### Composition du SEDA
#### Les éléments de l'en-tête du bordereau

```XML

<?xml version='1.0' encoding='UTF-8'?>
<ArchiveTransfer xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:pr="info:lc/xmlns/premis-v2" xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="fr:gouv:culture:archivesdefrance:seda:v2.1 seda2.1/seda-2.1-main.xsd" xml:id="ID1">

```

La déclaration du format de fichier  :
```XML
<?xml version='1.0' encoding='UTF-8'?>
```

Le type de message rédigé :
```XML
<ArchiveTransfert xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
```

La localisation du schéma permettant la validation des messages :
```XML
<xsi:schemaLocation="fr:gouv:culture:archivesdefrance:seda:v2.1 seda2.1/seda-2.1-main.xsd">
```

---
<!-- slide 20-->
### Composition du SEDA
#### Les éléments de l'en-tête du bordereau

```XML

<Comment>Mon premier versement</Comment>
<Date>2019-11-06T11:27:08.701</Date>
<MessageIdentifier>ark:/12157/df45erDf34</MessageIdentifier>

```

La déclaration du nom du versement  :
```XML
<Comment>Mon premier versement</Comment>
```

La date du transfert :
```XML
<Date>2019-11-06T11:27:08.701</Date>
```

L'identifiant du message :

```XML
<MessageIdentifier>ark:/12157/df45erDf34</MessageIdentifier>
```

---
<!-- slide 21-->

### Composition du SEDA
#### Les éléments de description des acteurs

```XML

<ArchivalAgency>
      <Identifier>
            DADCD54 (Direction des Archives Départementales de la Meurthe-et-Moselle)
      </Identifier>
    </ArchivalAgency>
    <TransferringAgency>
      <Identifier>
            DMPCD54 (Direction des marchés Publics du département de la Meurthe-et-Moselle)
      </Identifier>
</TransferringAgency>

```

La déclaration du service archives  :
```XML
<ArchivalAgency>
      <Identifier>
```

La déclaration du service versant :
```XML
<TransferringAgency>
      <Identifier>
```

---
<!-- slide 22-->
### Composition du SEDA
#### Les éléments de description des règles de gestion

Ces listes de codes permettent de contraindre la saisie et de valider la conformité des bordereaux.

```XML

<CodeListVersions>
    <ReplyCodeListVersion>La liste des codes de réponse</ReplyCodeListVersion>
    <MessageDigestAlgorithmCodeListVersion>La liste des codes de Hashage</MessageDigestAlgorithmCodeListVersion>
    <MimeTypeCodeListVersion>La liste des codes de type MIME</MimeTypeCodeListVersion>
    <EncodingCodeListVersion>La liste des codes d'encodage</EncodingCodeListVersion>
    <FileFormatCodeListVersion>La liste des codes de format de fichiers</FileFormatCodeListVersion>
    <CompressionAlgorithmCodeListVersion>La liste des algorithmes de compression</CompressionAlgorithmCodeListVersion>
    <DataObjectVersionCodeListVersion>La liste des codes de version des objets-données</DataObjectVersionCodeListVersion>
    <StorageRuleCodeListVersion>La liste des codes des règles de stockage</StorageRuleCodeListVersion>
    <AppraisalRuleCodeListVersion>La liste des codes de durée d'utilité administrative</AppraisalRuleCodeListVersion>
    <AccessRuleCodeListVersion>La liste des codes de communicabilité</AccessRuleCodeListVersion>
    <DisseminationRuleCodeListVersion>La liste des codes des règles de diffusion</DisseminationRuleCodeListVersion>
    <ReuseRuleCodeListVersion>La liste des codes de réutilisation</ReuseRuleCodeListVersion>
    <ClassificationRuleCodeListVersion>La liste des code de classification</ClassificationRuleCodeListVersion>
    <AuthorizationReasonCodeListVersion>La liste des codes d'autorisation</AuthorizationReasonCodeListVersion>
    <RelationshipCodeListVersion>La liste des codes de relation</RelationshipCodeListVersion>
  </CodeListVersions>

```

---
<!-- slide 23-->

### Composition du SEDA
#### La référence éventuelle à un profil ou à un contrat de versement

Ces références permettent de garantir **l'homogénéité** des données contenues dans les bordereaux et le **respect du contrat** établi avec les services versants.

```XML
<ArchivalProfile>Profil-marchesPublics-cd54.xml</ArchivalProfile>
<ArchivalAgreement>contrat-versement.pdf</ArchivalAgreement>

```
![illustration contrat](images/contratLagaffe.jpg)

---
<!-- slide 24-->

### Composition du SEDA
#### La description du contenu physique du bordereau

```XML
<BinaryDataObject id="ID10">
      <DataObjectVersion>BinaryMaster_1</DataObjectVersion>
      <Uri>content/ID10.pdf</Uri>
      <MessageDigest algorithm="SHA-512">abbe690f96801d9af3febe265c7d89ec16911e6cfd0b6e7978b5ec1ae1fd7f67873f60e59385ee76f23896b44d84e3b63d4dfc3f3713ab309c5deeab7f858f00</MessageDigest>
      <Size>78740</Size>
      <FormatIdentification>
      <FormatLitteral>Acrobat PDF 1.4 - Portable Document Format</FormatLitteral>
      <MimeType>application/pdf</MimeType>
      <FormatId>fmt/18</FormatId>
      </FormatIdentification>
      <FileInfo>
      <Filename>AE_SAE_RD.pdf</Filename>
      <LastModified>2019-09-26T16:04:38.088794</LastModified>
      </FileInfo>
</BinaryDataObject>
```
La déclaration du type d'objet :
```XML
<BinaryDataObject id="Archives1">
<PhysicalDataObject id="CDROM1">
<DataObjectGroup id="GROUP1">      
```

---
<!-- slide 25-->
### Composition du SEDA
#### La description des objets données


```XML
 <ArchiveUnit id="7">
      <Content>
            <DescriptionLevel>Item</DescriptionLevel>
            <Title>AMO_SAE_DQE_2019_VF.doc</Title>
            <Description>Devis quantitatif estimatif vierge</Description>
            <TransactedDate>2020-01-28T00:00:00</TransactedDate>
            <StartDate>2019-09-19T00:00:00</StartDate>
            <EndDate>2019-09-19T00:00:00</EndDate>
            <Tag>DCE</Tag>
            <Keyword>
              <KeywordContent>dossier de consultation des entreprises</KeywordContent>
              <KeywordReference>/ark:/67717/T3-95</KeywordReference>
              <KeywordType>subject</KeywordType>
            </Keyword>
      </Content>
      <DataObjectReference>
            <DataObjectGroupReferenceId>ID53</DataObjectGroupReferenceId>
      </DataObjectReference>
</ArchiveUnit>
```

---
<!-- slide 26-->
### Composition du SEDA
#### Les différérences entre le SEDA v1 et le SEDA v2

Une structuration **arborescente** versus une structuration orientée **données**

![le schéma SEDA V1](images/Diff_seda1-seda2.png)

SEDA v1 : modèle hérité de la norme de description ISAD(G) et de la DTD EAD.

SEDA v2 : modèle aligné sur les normes de description orientées ressources (RDA, FFBR, RIC)

---
<!-- slide 27-->

### Composition du SEDA v2
#### Un modèle de relation orienté autour des Objets-données

![le schéma du modèle SEDA v2](images/schemaSeda2.png)

---
<!-- slide 28-->
### Composition du SEDA

#### Transfert (@ArchiveTransfert) en SEDA v1

![le schéma SEDA V1](images/seda1.png)

---
<!-- slide 29-->
### Composition du SEDA
#### Transfert (@ArchiveTransfert) en SEDA v2

![Les échanges SEDA V2](images/seda2.png)

---
<!-- slide 30-->

### Structure d'un profil ou bordereau (SEDA v2)

#### Les éléments obligatoires du transfert

![les éléments obligatoires](images/sedaMinTransfert.png)

---
name: messages

<!-- slide 31-->

### Les messages SEDA

#### Les types de messages lors d'un transfert

.reduite[![Les échanges SEDA V2](images/transferer.png)]

---
<!-- slide 32-->

### Les messages SEDA
#### Les types de messages lors d'une communication

![Les échanges SEDA V2](images/communiquer.png)

---
<!-- slide 33-->

### Les messages SEDA
#### Les types de messages lors d'une modification

![Les échanges SEDA V2](images/modifier.png)

---

<!-- slide 34-->

### Les messages SEDA
#### Les types de messages lors d'une élimination

![Les échanges SEDA V2](images/eliminer.png)

---

<!-- slide 35-->

### Les messages SEDA
#### Les types de messages lors d'une restitution

![Les échanges SEDA V2](images/restituer.png)

---

<!-- slide 36-->


### Les messages SEDA
#### Les demandes d'autorisation

##### Les types de messages lors d'une demande d'autorisation au service producteur

![Les échanges SEDA V2](images/demande-service-prod.png)

---

<!-- slide 37-->


### Les messages SEDA
#### Les différents messages lors d'une demande d'autorisation au service de contrôle

![Les échanges SEDA V2](images/demande-service-controle.png)

---

<!-- slide 38-->

### Les messages SEDA
#### Acquitter / réceptionner : la structuration des messages

.pull-left[
![illustration de la structuration du message d'acquitement](images/msgTransfertReply_seda.png)
]

.pull-right[

- dématérialiser le bordereau de versement
- tracer le transfert de responsabilité
- décrire les évènements du cycle de vie
  ]

---

<!-- slide 39-->


### Les messages SEDA
##### Demander : la structuration des messages

.pull-left[
![illustration de la structuration du message de demande](images/msgDemandeCom_seda.png)
]

.pull-right[

- Demander une communication, une destruction ou une restitution
- Faire intervenir un service de contrôle
- générer des traces des réponses et des évènements
  ]

---
name: gestion

<!-- slide 40-->

### Les métadonnées

**définition**

> Une métadonnée est une donnée servant à **définir ou décrire** une autre donnée. **Porteuse d'information** sur le **contexte**, le **sens** et la **finalité** de la ressource informationnelle portée par la **donnée brute**.

![explication des métadonnées par un métaphore sur le vin](images/vinContexte.PNG)

---

<!-- slide 41-->


### Les métadonnées
#### Typologies de métadonnées dans le SEDA

Le SEDA utilise quatre sortes de **métadonnées** :

![illustration métadonnées seda](images/metadataSeda.png)

---

<!-- slide 42-->

### Les métadonnées
#### Les métadonnées de transport du SEDA

Les   métadonnées   de   transport   ont   pour   objet   d’encadrer   le   transport   des   Objets   de   données   depuis l’opérateur de versement jusqu’au service d’archives.

![illustration métadonnées transport](images/metadataSedaTransport.png)

???
empreinte des fichiers, inventaires des Objets de données transmis, identité de l’émetteur et du récepteur,
contrat applicable aux Objets de données transférés
Ces   métadonnées   constituent   un   bloc   à   part   entière   sous   une   forme   de   liste,   pour   tous   les éléments concernés par le transport.  Elles sont uniques par objet d’archives ou par lot d’archives.

Référence à l’accord de service (Archival Agreement)
 le choix des référentiels et des modèles à utiliser lors des transactions
  la liste des acteurs, leurs rôles et responsabilités dans ces transactions
  les niveaux de service, les règles d’accessibilité et les règles de sort final
Liste des objets de données

---

<!-- slide 43-->

### Les métadonnées
#### Les métadonnées de gestion du SEDA

Les métadonnées de gestion ont pour objet de rassembler l'ensemble des informations utiles à la gestion dans le temps de l'objet archivé : durées de conservation, règles d'accès,

Ces métadonnées peuvent être communes à tous les objets transférés ou spécifiques à chacun d'entre eux.

.reduite[![illustration métadonnées gestion seda](images/seda2-metadonnees-gestion.png)]

???
AccessRule = Gestion de la communicabilité = Bloc    de    métadonnées    de    gestion    permettant    de    gérer    la communicabilité.   Utilisable   dans   la   zone   des   métadonnées   de gestion  du  paquet  d’Objet   de   données et   dans   la   zone   des métadonnées de description, dans les niveaux de description.

PreventInheritance= Gestion  de  l’héritage  dans  le  nœud courant==> Métadonnée permettant d’ignorer toutes les règles de gestion héritées des nœuds parents dans le nœud courant.

RefNonRuleId= Arrêt de l’héritage dans le nœud courant==>Métadonnée permettant de retirer de l’héritage une règle de gestion définie dans le nœud courant.

Rule = Règle de gestion de la communicabilité ==> Référence  à  une  règle  de  communicabilité  à  appliquer.  Cette  dernière est  présente  dans  un  référentiel  sur  lequel  les  différents  acteurs  de l’échange se sont mis d’accord.

StartDate = Date de départ du calcul ==> Date permettant de calculer le terme de l’application de la règle de gestion  définie  dans  le  bloc  de  métadonnée  de  gestion  permettant  de gérer la communicabilité.

---

<!-- slide 44-->

### Les métadonnées
#### Les métadonnées techniques

![illustration métadonnées techniques](images/metadataSedaTechnique.png)

???
informations de représentation, informations d’intégrité et informations d’identification

---

<!-- slide 45-->

### Les métadonnées
#### Les métadonnées de description

Les Métadonnées descriptives regroupent l'ensemble des informations descriptives des objets de données. Les informations de pérennisation font référence aux objets de données listés et décrits dans le BinaryDataObjectType et le PhysicalDataObjectType.

.reduite[ ![illustration métadonnées description](images/metadonnees-description.png)]

???
archiveUnit : Information qui   permet  de gérer à la fois la hiérarchie intellectuelle, tout en contenant les métadonnées de description et de gestion propres à chaque niveau de description archivistique.

---
name: creation

<!-- slide 46-->


### Comment faire un profil SEDA ?

**Dire ce qui est toujours vrai et encadrer ce qui est possible**


1. Saisir les valeurs connues au travers de l'analyse de la production (tableau de gestion)
2. définir les cardinalités des champs pour les éléments
   1. toujours et unique
   2. toujours et multiple
   3. optionnel et unique
   4. optionnel et multiple

.center[![cardinalité](images/cardinalite.png)]

---

<!-- slide 47-->

### Comment le SEDA ?

Pour produire les fichiers xml nécessaires aux processus ou à la modélisation des paquets d'information, plusieurs outils sont disponibles :

- éditeur xml : production de bordereaux de versement ou de profils

- SHERPA : application en ligne de génération de profils

- Resip publié par le projet Vitam



.reduite2[[![éditeur XML](images/editeurXML.png)](https://jsonformatter.org/xml-editor)]
.reduite2[[![application SHERPA](images/sherpa.png)](https://francearchives.fr/sherpa/)]
.reduite2[ [![Resip](images/resip.png)](https://github.com/ProgrammeVitam/sedatools)]

---

<!-- slide 48-->

### Présentation de SHERPA

SHERPA a été conçu pour remplacer AGAPE et fournir **un lieu centralisé de production de profils SEDA**. Il implémente l'ensemble des champs du SEDA 2.0 et **permet d'exporter des profils pour les intégrer dans d'autres outils**. Il permet de choisir parmi **une liste définie de vocabulaires les listes de codes associés aux profils**.

.reduite[![illustration de sherpa](https://francearchives.fr/sherpa/data/3dcc0f05c46935e8ee6e6e4e558d943a/images/sherpa_cinematique.png)]

---

<!-- slide 49-->

### Présentation de SHERPA
#### Accueil

![page d'accueil authentifié](images/sherpaAccueil.png)

---

<!-- slide 50-->

### Présentation de SHERPA
#### Création de notice d'autorité

![page création notice EAC](images/sherpaNoticeAutorite.png)

---

<!-- slide 51-->

### Présentation de SHERPA
#### Création d'unité d'archives

![page création unité d'archives](images/sherpaUniteArchives.png)

---

<!-- slide 52-->

### Présentation de SHERPA
#### Création de profil

![page création profil](images/sherpaProfil.png)

---

<!-- slide 53-->

## Automatisation des versements

#### 	Principes :

- Des flux réguliers

- Des flux structurés

#### Etude des appplications :

- **Etudier** et comprendre le flux : modèle d'export du flux à archiver

  - XML or not XML ?
  - SEDA or not SEDA ?

- **Modéliser **

  - Plan de classement des documents à archiver
  - Les métadonnées
  - Les étapes du flux vers le SAE

- **Mapper** l'export avec un profil  = fournir un profil à l'éditeur pour modéliser son export ?

---

<!-- slide 54-->

### Automatisation des versements
#### Export automatisé des applications métiers

![illustration ESB](images/connecteurApplicatifSeda.png)

---

<!-- slide 55-->

### Automatisation des versements : Gouvernance

- Estimation du coût : devis de l'éditeur

- Prioriser

  - Valeur historique
  - Valeur probante
  - DUA
  - Valeur d'usage

- Arbitrer

- Définir le [contrat de versement](https://francearchives.fr/file/b97acb9a1af450921cad61dbf5a34ae2910a8b9b/BDR_04_Contrat_de_versement.pdf)

  - Définir la périodicité des versements d'un flux automatiser

  - Validation automatique ou non ? (signature électronique ou non)

  - Taille maximale par versement

  - Taille maximale par période de versement



---

<!-- slide 56-->

### Liens avec d'autres schémas de description

![illustration des liens](images/webLinkedData.png)

---

<!-- slide 57-->

#### Les métadonnées de description : détail

* description : Zone de description générale de l'unité d'archives (1.n).
* DescriptionLanguage : langue de la description (0.1) = SKOS
* DescriptionLevel : niveau de description (0.1) = SKOS
* DocumentType : typologie documentaire au sens diplomatique du terme = SKOS
* OriginatingAgency : service producteur (0.1) = EAC
* Keyword : mot-clé ou descripteur contrôlé (0.n) = SKOS
* Writer : rédacteur du document (0.n)

.reduite[![les éléments de la description](images/velo.jpg)]

---

<!-- slide 58-->

### La description des évènements

Depuis le SEDA 2.0, il est possible de décrire des évènements suivant 3 significations

.reduite[![les évènements SEDA](images/seda2Event.png)]

---

<!-- slide 59-->

#### Premis :Event

Les métadonnées de Event sont inspirées de [PREMIS](http://www.loc.gov/standards/premis/).

.reduite[![premis event](images/premisEvent.png)]

???
* Pour lister les événements métier intervenus sur une unité d’archives (Content (métadonnées descriptives)).
* Pour lister les événements survenus lors de la gestion du cycle de vie des unités d’archives ou lors de la génération des DataObjectPackage échangés (LogBook (métadonnées de gestion)).
* Pour consigner les traces d’un journal des opérations sur l’ensemble du message (LogBook (métadonnées de gestion)). Correspond à la transmission des événements tracés dans le journal des événements au sens de la NFZ 42-013

---

<!-- slide 60-->

### Ressources

Le support de présentation en [PDF](../export-pdf/formation-seda-CD54.pdf)

[Description du standard](https://francearchives.fr/seda/documentation/SEDA_description_standard_v2_1.pdf)

Le [dictionnaire des balises](https://redirect.francearchives.fr/seda/Dictionnaire_SEDA2.1.pdf) du SEDA 2.1 avec des exemples d'utilisation.

Interface web de [consultation des schémas de la version 2.1](https://francearchives.fr/seda/documentation.html) sous forme SVG.

Les [documents de référence](https://francearchives.fr/fr/article/91524885) pour l'archivage électronique

---

<!-- slide 61-->

## Profils SEDA : Exercices

![illustration Pastell](images/exercice.png)

---

<!-- slide 62-->

### Profils simples

- Les marchés publics : création d'un profil de versement d'un marché public à partir de la documentation du SIAF
  - 	Créer des unités administratives
  - 	Créer le profil
  - Créer la notice d'autorité

- La gestion du Patrimoine

---

<!-- slide 63-->

### Profils plus complexes

* Les marchés publics : création d'un profil de versement pour l'ensemble des marchés notifiés et exécutés dans une année budgétaire
* La gestion du Patrimoine : dossier de bâtiment et l'ensemble des actions d'intervention clôturée dans une année budgétaire.
