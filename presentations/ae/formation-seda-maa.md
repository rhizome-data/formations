class: center, top
background-image: url(images/logo-rhizome-data.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.footnote[
      Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data pour le compte de l'Association des Archivistes Français]

### Standard d'échange de données pour l'archivage (SEDA)

---
name: programme

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat

---
### Objectifs

- Connaître la dernière version du SEDA et savoir générer un profil.
- Comprendre les pré-requis de la mise en oeuvre et du déploiement d’un SAEI.
- Savoir définir un projet de versement d’archivage électronique dans un SAEI.

---
### SEDA et profil

- Comprendre la [structuration](#structuration) des messages.
- Comprendre la [gestion](#gestion) des métadonnées.
- Comprendre la notion de [profil](#profil) SEDA, unités d'archives et objets-données.
- Maîtriser la [création](#creation) de profil SEDA.

---

layout: true
class: left, top
background-image: url(images/logo-rhizome-data-s.jpg)
background-position: right top
background-repeat: no-repeat


.footnote[
      [Programme](#programme)]

---

### Pourquoi le SEDA ?

Faciliter les échanges entre les systèmes d'information en les **normalisant** et en permettant leur **interopérabilité**.

.reduite[![Interopérabilité](images/desertB.jpg)]

---

### Chronologie du SEDA ?

La rédaction de cette grammaire est à l'initiative du Service interministériel des Archives de France :

- **mars 2006**, publication de la **version 0.1** suivie d'une instruction DITN/RES/2006/001 au 8 mars 2006
- janvier 2010 publication de la version 0.2
- septembre **2012** publication de la **version 1.0**
- **2014** : parution de la norme Afnor **NF 44-022 MEDONA** : Modélisation des échanges de données pour l'archivage
- décembre **2015** publication de la **version du standard 2.0**, conforme à la norme MEDONA
- **2017** : parution de la norme **ISO 20614 DEPIP** : Information et documentation -- Protocole d'échange de données pour l'**interopérabilité et la préservation**
- **juin 2018** publication de la révision 2.1 : Interface web de consultation des schémas de la [**version 2.1**](https://francearchives.fr/seda/api_v2-1/seda-2.1-main.html)

![Chronologie du SEDA](images/chronoSEDA.png)

---

### Comment le SEDA ?

> Le standard d'échange de données pour l'archivage **modélise les différentes transactions** qui peuvent avoir lieu **entre des acteurs** dans le cadre de l'**archivage de données**.

> Il précise les messages échangés ainsi que les métadonnées à utiliser pour **décrire, gérer et pérenniser** l’information.

---

### Ca sert à quoi le SEDA ?

Il vise à :

- proposer un **cadre d'interopérabilité** pour la dématérialisation des processus d'archivage
- **structurer les messages** renforcant la valeur probante des archives
- **relier des unités de description** au sein du processus de versement

![illustration du pourquoi du SEDA](images/allInOneSeda.png)

---
name: structuration

### Les acteurs du SEDA

.pull-left[
![Les 5 acteurs du SEDA](images/acteursSEDA2.png)
]
.pull-right[
Les acteurs sont au nombre de cinq :

- le service producteur,
- le service versant,
- le service d'archives,
- le service de contrôle,
- le demandeur d'archives.

> Ils peuvent avoir plusieurs rôles en même temps
> ]

---


### Les transactions du SEDA

.pull-left[

![Les transactions dans le SEDA](images/transactionsSEDA.png)
]

.pull-right[
**Les transactions** sont au nombre de six :

- la demande de transfert,
- le transfert,
- la modification,
- l'élimination,
- la communication,
- la restitution.

]

---


### Structurer les messages

.pull-left[

![traçabilité des échanges](images/structureMsg.png)
]

.pull-right[

- types d'acteurs
  - service producteur (OriginatingAgency)
  - service versant (TransferingAgency)
  - service d'archives (ArchivalAgency)
  - service de contrôle (AuthorizationControlAuthority)
- représentation
  - messages XML (Request / Reply / Notification / Acknowledgement)
  - schémas de validation (RNG ou XSD)
    ]

---

### Les messages SEDA

#### Les types de messages lors d'un transfert

.reduite[![Les échanges SEDA V2](images/transferer.png)]

---

#### Les types de messages lors d'une communication

![Les échanges SEDA V2](images/communiquer.png)

---

#### Les types de messages lors d'une modification

![Les échanges SEDA V2](images/modifier.png)

---

#### Les types de messages lorsd'une élimination

![Les échanges SEDA V2](images/eliminer.png)

---

#### Les types de messages lors d'une restitution

![Les échanges SEDA V2](images/restituer.png)

---

#### Les demandes d'autorisation

##### Les types de messages lors d'une demande d'autorisation au service producteur

![Les échanges SEDA V2](images/demande-service-prod.png)

---

##### Les différents messages lors d'une demande d'autorisation au service de contrôle

![Les échanges SEDA V2](images/demande-service-controle.png)

---

#### Renforcer la valeur probante des échanges

- le bordereau est l'acte "signé" du **transfert de responsabilité**
- les messages d'accusé (réception, validation) sont les **preuves** de la transaction
- les demandes de communication et d'élimination peuvent être signées par un **service de contrôle**

![accusé de réception](images/msgAck_seda.png)

---

##### Transférer : la structuration des messages

- **encapsuler** dans un bordereau le contenu du versement
- décrire les **acteurs impliqués** (producteur, versant, archives)
- décrire les **propriétés techniques** des objets données
- **décrire le contenu** des unités de description

![illustration de la structuration du message](images/msgTransfert_seda.png)

---

#### Acquitter / réceptionner : la structuration des messages

- dématérialiser le bordereau de versement
- tracer le transfert de responsabilité
- décrire les évènements du cycle de vie

![illustration de la structuration du message d'acquitement](images/msgTransfertReply_seda.png)

---

##### Demander : la structuration des messages

- Demander une communication, une destruction ou une restitution
- Faire intervenir un service de contrôle
- générer des traces des réponses et des évènements

![illustration de la structuration du message de demande](images/msgDemandeCom_seda.png)

---

### Les différérences entre le SEDA v1 et le SEDA v2

Une structuration **arborescente** versus une structuration orientée **données**

* SEDA v1 : modèle hérité de la norme de description ISAD(G) et de la DTD EAD.
* SEDA v2 : modèle aligné sur les normes de description orientées ressources (RDA, FFBR, RIC)

---

#### Modèle conceptuel du transfert en SEDA v1

![le schéma SEDA V1](images/schemaSeda1.png)

---

#### Modèle conceptuel du transfert en SEDA v2

![le schéma SEDA V2](images/schemaSeda2.png)

---
name: gestion

### Les métadonnées

**définition**

> Une métadonnée est une donnée servant à **définir ou décrire** une autre donnée. **Porteuse d'information** sur le **contexte**, le **sens** et la **finalité** de la ressource informationnelle portée par la **donnée brute**.

![explication des métadonnées par un métaphore sur le vin](images/vinContexte.PNG)

---

#### Transfert (@ArchiveTransfert) en SEDA v1

Les métadonnées de description suivent la logique hiérarchique de description.

![le schéma SEDA V1](images/seda1.png)

---

#### Transfert (@ArchiveTransfert) en SEDA v2

Les métadonnées sont organisées par tyopologies et reliées aux objets par référence.

![Les échanges SEDA V2](images/seda2.png)

---

### Structure d'un profil (SEDA v2)

#### Les éléments obligatoires du transfert

![les éléments obligatoires](images/sedaMinTransfert.png)

---

### Typologies de métadonnées

Le SEDA utilise quatre sortes de **métadonnées** :

![illustration métadonnées seda](images/metadataSeda.png)

---

#### Les métadonnées de transport

Les   métadonnées   de   transport   ont   pour   objet   d’encadrer   le   transport   des   Objets   de   données   depuis l’opérateur de versement jusqu’au service d’archives.

![illustration métadonnées transport](images/metadataSedaTransport.png)

???
empreinte des fichiers, inventaires des Objets de données transmis, identité de l’émetteur et du récepteur, contrat applicable aux Objets de données transférés
Ces   métadonnées   constituent   un   bloc   à   part   entière   sous   une   forme   de   liste,   pour   tous   les éléments concernés par le transport.  Elles sont uniques par objet d’archives ou par lot d’archives.

Référence à l’accord de service (Archival Agreement)
le choix des référentiels et des modèles à utiliser lors des transactions
la liste des acteurs, leurs rôles et responsabilités dans ces transactions
les niveaux de service, les règles d’accessibilité et les règles de sort final
Liste des objets de données

---

#### Les métadonnées de gestion

Les métadonnées de gestion ont pour objet de rassembler l'ensemble des informations utiles à la gestion dans le temps de l'objet archivé : durées de conservation, règles d'accès,

Ces métadonnées peuvent être communes à tous les objets transférés ou spécifiques à chacun d'entre eux.

Elles peuvent êtres héritées ou pas et servir à définir des conditions de restriction d'accès.

.reduite[![illustration métadonnées gestion seda](images/seda2-metadonnees-gestion.png)

]

???
AccessRule = Gestion de la communicabilité = Bloc de métadonnées de gestion permettant de gérer la communicabilité. Utilisable dans la zone des métadonnées de gestion du paquet d’Objet de données et dans la zone des métadonnées de description, dans les niveaux de description.

PreventInheritance= Gestion de l’héritage dans le nœud courant==> Métadonnée permettant d’ignorer toutes les règles de gestion héritées des nœuds parents dans le nœud courant.

RefNonRuleId= Arrêt de l’héritage dans le nœud courant==>Métadonnée permettant de retirer de l’héritage une règle de gestion définie dans le nœud courant.

Rule = Règle de gestion de la communicabilité ==> Référence à une règle de communicabilité à appliquer.  Cette  dernière est  présente  dans un  référentiel sur lequel les différents acteurs de l’échange se sont mis d’accord.

StartDate = Date de départ du calcul ==> Date permettant de calculer le terme de l’application de la règle de gestion définie dans le bloc de métadonnée de gestion permettant de gérer la communicabilité.

---

#### Les métadonnées techniques

![illustration métadonnées techniques](images/metadataSedaTechnique.png)

???
informations de représentation, informations d’intégrité et informations d’identification

---

#### Les métadonnées de description

Les Métadonnées descriptives regroupent l'ensemble des informations descriptives des objets de données. Les informations de pérennisation font référence aux objets de données listés et décrits dans le BinaryDataObjectType et le PhysicalDataObjectType.

.reduite[ ![illustration métadonnées description](images/metadonnees-description.png)]

???
archiveUnit : Information qui permet de gérer à la fois la hiérarchie intellectuelle, tout en contenant les métadonnées de description et de gestion propres à chaque niveau de description archivistique.

---

#### Les métadonnées de description : détail

* description : Zone de description générale de l'unité d'archives (1.n).
* DescriptionLanguage : langue de la description (0.1)
* DescriptionLevel : niveau de description (0.1)
* DocumentType : typologie documentaire au sens diplomatique du terme
* OriginatingAgency : service producteur (0.1)
* Keyword : mot-clé ou descripteur contrôlé (0.n)
* Writer : rédacteur du document (0.n)

.reduite[![les éléments de la description](images/velo.jpg)]

---

### La description des évènements

Depuis le SEDA 2.0, il est possible de décrire des évènements suivant 3 significations

.reduite[![les évènements SEDA](images/seda2Event.png)]

---

#### Premis :Event

Les métadonnées de Event sont inspirées de [PREMIS](http://www.loc.gov/standards/premis/).

.reduite[![premis event](images/premisEvent.png)]

???
* Pour lister les événements métier intervenus sur une unité d’archives (Content (métadonnées descriptives)).
* Pour lister les événements survenus lors de la gestion du cycle de vie des unités d’archives ou lors de la génération des DataObjectPackage échangés (
LogBook (métadonnées de gestion)).
* Pour consigner les traces d’un journal des opérations sur l’ensemble du message (LogBook (métadonnées de gestion)). Correspond 
à la transmission des événements tracés dans le journal des événements au sens de la NFZ 42-013

---

### Liens avec d'autres schéma de description

![illustration des liens](images/webLinkedData.png)

---
name: profil

### Comment le SEDA ?

1. Saisir les valeurs connues au travers de l'analyse de la production (tableau de gestion)
2. définir les cardinalités des champs pour les éléments
   1. toujours et unique
   2. toujours et multiple
   3. optionnel et unique
   4. optionnel et multiple

---

### Comment le SEDA ?

Pour produire les fichiers xml nécessaire aux processus ou à la modélisation des paquets d'information, plusieurs outils sont disponibles :

- éditeur xml : production de bordereaux de versement ou de profils
- SHERPA : application en ligne de génération de profils
- SAEM : application en ligne de génération de profils et de génération de bordereaux SEDA

.reduite2[[![éditeur XML](images/editeurXML.png)](https://jsonformatter.org/xml-editor)] .reduite2[[![application SHERPA](images/sherpa.png)](https://francearchives.fr/sherpa/)] .reduite2[[![éditeur XML](images/referentielSAEM.png)](https://www.saemgirondin.fr/)]

---

### Présentation de SHERPA

SHERPA a été conçu pour remplacer AGAPE et fournir un lieu centralisé de production de profils SEDA. Il implémente l'ensemble des champs du SEDA 2.0 et permet d'exporter des profils pour les intégrer dans d'autres outils. Il permet de choisir parmi une liste définie de vocabulaires les listes de codes associés aux profils.

.reduite[![illustration de sherpa](https://francearchives.fr/sherpa/data/3dcc0f05c46935e8ee6e6e4e558d943a/images/sherpa_cinematique.png)]

---

### Présentation de SHERPA : Accueil

![page d'accueil authentifié](images/sherpaAccueil.png)

---

### Présentation de SHERPA : Création de notice d'autorité

![page création notice EAC](images/sherpaNoticeAutorite.png)

---

### Présentation de SHERPA : Création d'unité d'archives

![page création unité d'archives](images/sherpaUniteArchives.png)

---

### Présentation de SHERPA : Création de profil

![page création profil](images/sherpaProfil.png)

---

## Exercices

![illustration exercice](images/exercice.png)

---

### Profils simples dans SHERPA

- Les fonctions supports des ministères

  - Créer des unités d'archives

  - Créer le profil

  - Créer les notices d'autorité (Services producteurs / services archives)

  - Exporter le profil

---

### Comment implémenter le SEDA : Export depuis les applications métiers

![export simple](images/ExportApplicatifSeda.png)

---

### Comment implémenter le SEDA : Export urbanisé

![export simple](images/connecteurApplicatifSeda.png)

---

## Exercices

![illustration exercice](images/exercice.png)

---

#### profil de versement d'Etudes

- Direction de l’administration pénitentiaire
 / Sous-direction des métiers et de l'organisation des services (Me)
 / Bureau des statistiques et des études (Me5)

[Tableau de gestion](doc/Me5-Justice.odt)

- Créer des unités d'archives
- Créer le profil
- Créer les notices d'autorité (Bureau des statistiques et des études / service mission MJ)
- Exporter le profil

---

### Comprendre les pré-requis de la mise en oeuvre et du déploiement d’un SAEI.

Qui fait quoi ?

![sae oais](images/sae-oais.png)

---
### Comprendre les pré-requis de la mise en oeuvre et du déploiement d’un SAEI.

**Gouvernance** : Définir les rôles et responsabilité entre AA et OA :

![oais](images/systeme_archivage_electronique_2.jpg)

---

### Mise en oeuvre : identifier les rôles et responsabilités

* Identification des acteurs.
* Formalisation des contrats de service.

L’AA (autorité d’archivage) est responsable de l'ensemble des prestations rendues par le service d’archivage électronique conformément à la politique d’archivage dont elle est à l’origine. L’ensemble des prestations peut être décliné en plusieurs niveaux de sécurité et de service.

L’OA (opérateur d’archivage) est quant à lui responsable des moyens mis en œuvre pour satisfaire les exigences définies dans la PA. L’AA doit posséder un droit de contrôle sur l’OA.

![parcours combattant](images/acteurs_ProcessusSAE_Adullact.png)

---

#### Detour par la question de la pérennisation

![la fonction pérennisation](images/fonctionPerennisation.jpg)

---

#### Comprendre les pré-requis de la mise en oeuvre et du déploiement d’un SAEI.

**Pré-requis** : Définir les contrats de versement

* Identifier les évènements déclencheurs de la prise en charge : archivage par procédure métier.
* Définir les responsabilités et l'offre d'accompagnement avec les services producteurs.
* Etablir des [contrats de versement](https://francearchives.fr/file/b97acb9a1af450921cad61dbf5a34ae2910a8b9b/BDR_04_Contrat_de_versement.pdf).

![contrat](images/contrat.jpg)

---

#### contrat de versement

Il vise à définir les règles pour le versement des archives par producteur.

Il précise notamment :

 * Les règles de gestion du cycle de vie des archives (durée d’utilité administrative, sort final, communicabilité) et de description des archives,
 * les modalités pratiques de transfert des archives.

Le Contrat de versement est une déclinaison d’un Contrat de service spécifique au versement d' une typologie d’archives particulière ou à un ensemble de versements ayant des caractéristiques communes. Il est donc conclu entre les mêmes acteurs.

Il précise également les consitions d'accès aux archives versées notamment au regard de la protection des données personnelles.

---

#### Comprendre les pré-requis de la mise en oeuvre et du déploiement d’un SAEI.

**Déploiement** : Définir l'environnement technique et l'organisation associée :

* Espaces de stockage et sécurité : redondance et gestion des habilitations
* PCI / PRI : définir les engagements de l'opérateur d'archivage en matière de continuité de service et de reprise en cas de sinitre.
* homologuer le service:  identification des risques et des mesures permettant leur réduction. Homologation RGS du service.

![salle archives](images/salle-archives.jpg) ![salle serveur](images/salle-serveur.jpg)

---

#### Documentation du cadre de la mise en oeuvre

![cadre stratégique](images/cadreDocSae.jpg)

---

#### Savoir définir un projet de versement d’archivage électronique dans un SAEI.

Les grandes étapes :

* Cartographie de la production : tableau de gestion et cartographie des flux de données
* Rédaction des profils de versement
* Définition des modalités de versement (contrat de versement)
* Définition de la procédure de communication
* Définition de la procédure d'élimination
* Formation et accompagnement des producteurs

---

#### Avec VITAM par exemple

![vitam architecture](images/SchemaSAEVitam.jpg)

---

#### Qui fait quoi où avec VITAM ?

![fonctionnalités VITAM](images/vitamfonctions.png)

---

#### Cartographie de la production : tableau de gestion et cartographie des flux de données

L'identification, parmi la masse des données produites, de celles à forte **valeur juridique, stratégique et/ou patrimoniale** et dont la durée de conservation peut être très longue.

![cartographie production](images/cartoFlux.jpg)

---

#### Cartographie de la production : tableau de gestion et cartographie des flux de données

Elle s'appuie sur la définition d'une politique d'accès à l'information conforme aux dispositions prévues par la législation en vigueur.

![cartographie production](images/Couv294.jpg)

---

#### Cartographie de la production : tableau de gestion et cartographie des flux de données

Elle rend possible la destruction de façon contrôlée des données devenues inutiles afin de réduire les **coûts de stockage** et de garantir le **retrait de service** des applications qui ne sont plus utilisées.

![elimination production](images/stock-archives.jpg)

---

#### définition de la procédure de communication

* faire la distinction entre DUC et DUA en terme de droit d'en connaître
* établir les contrats de service avec les communautés de (ré-)utilisateurs
 
![communication](images/1959-1-moyen.jpg)

---

#### communication : loi sur les archives et loi pour une république numérique

![communication](images/donneesOuvertes.jpg)

---

#### formation et accompagnement des producteurs

* S'appuyer sur les démarches de mise en conformité RGPD
* Valoriser la complémentarité entre Open data et Archives
* Contribuer à une démarche de la culture de la donnée 

![données ouvertes et archives](images/archivage-electronique-open-data-schema.jpg)

.credit[schema produit par Naoned System]

---

#### formation et accompagnement des producteurs

![expert data](images/metiersData.jpg)

---

### Outillage

* faire des profils : [SHERPA](https://francearchives.fr/sherpa/)
* visualiser des arborescences : [Archifiltre](https://archifiltre.github.io/)
* traiter des vracs numériques : [OCTAVE](https://francearchives.fr/fr/article/88482499)
* exporter des bases de données : [SiardSuite](https://www.bar.admin.ch/bar/fr/home/archivage/outils-et-instruments/siard-suite.html)

![les outils](images/outillage.jpg)

---

### Présentation des outils

Archifiltre : L’objectif d’ArchiFiltre est de proposer à tout utilisateur de fichiers bureautiques un outil de visualisation d’arborescences complètes afin de pouvoir les appréhender rapidement en vue de les décrire, les organiser, les trier et aussi les enrichir en apportant de la contextualisation et de la qualification aux documents.

Octave : Outil de Constitution et de Traitement Automatisé des Versements Électroniques (OCTAVE) permet à l’archiviste de traiter après import des arborescences bureautiques (dédoublonnage, tri, fusion, classement, renommage), d’enrichir les métadonnées et de constituer des SIP aux formats SEDA 1 et SEDA 2.1 accompagné d’un manifeste xml, tout en pouvant produire bordereau de récolement et d’élimination

Resip : L’application ReSIP, construite au-dessus de la bibliothèque sedalib, permet de construire et manipuler des structures arborescentes d’archives, d’en éditer les métadonnées, de les importer et exporter sous la forme de SIP, sous la forme de hiérarchie disque ou encore sous forme csv pour les plans de classement. Elle peut prendre en compte des fichiers bureautiques ou des conteneurs de messagerie.


---

#### Resip

* d’importer des structures arborescentes d’archives et les fichiers qui les représentent;
* d’enrichir les métadonnées de description et de gestion des unités archivistiques(ArchiveUnits) ainsi que les métadonnées techniques des fichiers (BinaryObjects);
* d’exporter les structures arborescentes d’archives et les fichiers qui les représentent sous uneforme importable dans la solution logicielle Vitam ou sous la forme d’une arborescence defichiers.


#### Méthodologie projet

Version Alpha

![SAE beta](images/trotinette.jpg)

---
#### Méthodologie projet

Version Beta

![SAE beta](images/mobylette.jpg)

---

#### MVP

![la valeur ajoutée](images/canvasvierge.png)

---

#### MVP : la cible

![la valeur ajoutée](images/leancanvas4.png)

---
#### MVP : le premier pas

![la valeur ajoutée](images/leancanvas1.png)

---
### Bibliographie

[Description du standard](https://francearchives.fr/seda/documentation/SEDA_description_standard_v2_1.pdf)

[Les schémas SEDA](https://francearchives.fr/seda/documentation.html) : 
Interface web de consultation des schémas de la version 2.0 sous forme SVG

[Le dictionnaire des balises du SEDA 2.1](https://francearchives.fr/seda/Dictionnaire_SEDA2.1.pdf)

Les [documents de référence](https://francearchives.fr/fr/article/91524885) pour l'archivage électronique