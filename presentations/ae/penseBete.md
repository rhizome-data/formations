### outils annexes

* le sommaire, qui offre un accès  par  la  hiérarchie  des  activités  ;  
* l’index  des  activités,  des  catégories  documentaires, des  unités  et  instances  productrices  ;
* la  liste  des  types  de  dossiers  ou  de  documents destinés  à  une  conservation  définitive  ;  
* le  mode  opératoire  de  l’archivage  (à qui s’adresser pour détruire, pour verser, comment dresser les bordereaux, quels conteneurs utiliser...)

![](images/outillage.jpg)



Localisation des exemples et programme

Archifiltre : /home/pascal/github/archifiltre

ex : /home/pascal/Documents/DIMMO

Octave : /home/pascal/Documents/docuteam-packer
lancement : ./octaveLinux.sh 

ex : DIMMO

Resip : /home/pascal/github/sedatools/resip

lancement : java -jar target/resip-2.0.0-SNAPSHOT-shaded.jar


