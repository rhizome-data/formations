class: center, top
background-image: url(media/association-aristote.jpg)
background-position: left top;
background-repeat: no-repeat;
background-size: contain;

.credit[
Support de formation distribué en Creative Commons CC-By-SA 4.0 ; édité par Rhizome-data pour le compte de l'Association Aristote.]

# formation PIN septembre 2019

---

# Qui suis-je ?

.pull-left[
**Consultant-formateur** au sein de l'assocation **Rhizome-data** après avoir coordonné plusieurs projets dans le domaine de l'**ingénierie documentaire**
(SAE, GED, Open data) au sein du Département de la Gironde.

**Facilitateur numérique** : je contribue à renforcer le **pouvoir d'agir** des utilisateurs au travers d'un **accompagnement aux usages** (conscients) du numérique.

@keronos
[site web](https://www.rhizome-data.fr)
]
.pull-right[
![logo Rhizome](./media/triplet-rhizome-data-4.jpg)
]

---

layout: true
class: left, top
background-image: url(media/logo-aristote-site-orange.png)
background-position: right top
background-repeat: no-repeat

### La carte

.footnote[
[Programme](#programme1)]

---

name: programme1

1. co-construction programme et attentes
2. Cycle de vie des documents engageants (records)
3. Contraintes réglementaires et risques
4. La gestion de l'archivage au sens du records management.
5. Introduction sur les Métadonnées en général
6. Le modèle d’information de l’OAIS : place des Métadonnées
7. Quelles métadonnées pour la pérennisation ?
8. Encoder les métadonnées
9. L’accès à l’information grâce aux métadonnées.

---

layout: true
class: left, top
background-image: url(media/logo-aristote-site-orange.png)
background-position: right top
background-repeat: no-repeat

.footnote[
[Programme](#programme1)]

---

### Co-construction et attentes

![speedboat](./media/speedboat.jpg)

---

### Cycle de vie des documents (engageants)

![cycle de vie des données au sein du SI](./media/cycleVieData.jpg)

---

### Cycle de vie des documents - définition

> **Record management** : **organisation** des opérations permettant de **contrôler les processus** de production documentaire

- Identification des documents et données essentiels
- Cartographie des processus de gestion associés
- Mise en oeuvre des procédures de mise en sécurité et de gestion de la qualité

> **Archivage** : **conservation** des documents et données et **des informations** qu’ils contiennent

- Dans leur aspect physique comme dans leur aspect intellectuel
- Sur le très long terme
- De manière à ce qu’ils soient en permanence accessibles et compréhensibles

---

### Cycle de vie des documents - Norme de gestion des activités documentaires

La **norme ISO 30300** dite "gestion des documents d'activité" permet d'inclure les aspects essentiels à la mise en place d'une **stratégie de gestion de la production documentaire** en reprenant des concepts issus des normes de gestion de la qualité :

- l’efficacité de toutes les activités « métier » d’une organisation,
- la **responsabilisation** de tous les acteurs,
- la gestion des **risques**,
- la **continuité** des opérations.

---

### Cycle de vie des documents - Norme de gestion des archives

La **norme ISO 14721:2012** dite "Système ouvert d'archivage d'information" est un modèle conceptuel destiné à la gestion, à l'archivage et à la préservation à long terme de données numériques au travers de :

- la mise en place d'un modèle fonctionnel de gestion des archives,
- la mise en place d'un modèle d'information pour la structuration des métadonnées,
- la description des responsabilités en matière de pérennisation et d'interopérabilité,

---

### Les 3 âges électroniques

![graphique illustrant la gestion du cycle de vie des données](./media/cyclevieGEDSAE.png)

---

### Cycle de vie des documents engageants (records)

- problématique de nommage
- versionnage
- copie
- validation
- validité

![Le contrat](./media/contratLagaffe.jpg)

---

### Qualifier et classer l'information numérique : caractéristiques internes et externe

.pull-left[![une disquette informatique](./media/disquette.jpg)]
.pull-right[![des métadonnées EAD en XML](./media/metadonneesXML.png)]

---

### Caractéristiques internes

.pull-left[

- nom (identifiant)
- titre
- description
- date de création
- date de dernière modification
- auteur (contexte de production)
- format
- indexation
- somme de contrôle
  ]
  .pull-right[![vieille carte d'identité](./media/CarteIdentite.jpg)]

---

### Caractéristiques externes

.pull-left[

- version
- classification de diffusion
- classification de publication
- sort final
- DUA
- signature
  ]
  .pull-right[![contexte](./media/cerveauContexte.jpg)]

---

### Continuum information

![schéma d'illustration du continuum du cycle de vie](./media/continuiteInformation.png)

---

## Cycle de vie des documents - Un petit détour par la vision processus

> **processus** : ensemble ordonnées d'activités qui délivre un produit et/ou un service, à un "client" interne ou externe, lui apporte de la valeur, répond à ses besoins exprimés ou implicites et nécessite d'être maîtrisé/piloté

> **processus métier** : ensemble des activités qui s’enchaînent pour créer un produit ou un service à partir d’éléments de base. Un processus se décompose en sous-processus. (MAC, glossaire de l'archivage)

![illustration d'un processus](./media/dessinerProcessus2.jpg)

---

### Cycle de vie des documents - Exemple de processus

![exemple processus gestion action sociale num](./media/processusNum.png)

---

### Cycle de vie des documents - Macro-processus de RM

.pull-left[![illustration d'un processus de record Management](./media/processusRM.png)]
.pull-right[.reduite[![Diversité des activités](./media/rosaceActivites.png)]]

---

class: center, top

### Cycle de vie des documents - Modélisation d'un processus

1. On commence par définir les diagrammes de cas d'utilisation (Use Case)

![exemple de cas d'utilisation archiver](./media/useCaseArchiver.png)

<!--
Qui permettent de donner une vue globale de l'application. Pas seulement pour un client non avisé qui aura l'idée de sa future application mais aussi le développeur s'en sert pour le développement des interfaces.

La représentation d'un cas d'utilisation met en jeu trois concepts : l'acteur, le cas d'utilisation et l'interaction entre l'acteur et le cas d'utilisation.

Cas d'utilisation : Un cas d'utilisation (use case) représente un ensemble de séquences d'actions qui sont réalisées par le système et qui produisent un résultat observable intéressant pour un acteur particulier

-->

---

class: center, top

### Cycle de vie des documens - Modélisation d'un processus

2. Ensuite on va présenter la chronologie des opérations par les diagrammes de séquences.

### ![exemple de diagramme de classe verser archives](./media/diagrammeSequenceArchiver.png)

---

class: center, top

### Cycle de vie des documents - Modélisation d'un processus

3. Et finir par les diagrammes statiques, qui sont celles de classe de conception, de classe participantes et le modèle physique.

![exemple de diagramme de classe verser archives](./media/diagrammeClasseVerserArchives.png)

---

### Contraintes réglementaires et risques - Archiver pour prouver et pour tracer

.pull-left[

- transfert de responsabilités
- stockage sécurisé
- gel de l'original
- journal des événements
  ]
  .pull-right[![empreinte digitale](./media/tracabilite.jpg)]

---

## Contraintes réglementaires et risques - La gestion des risques

- éviter que des documents/données ne soient pas archivés
- éviter que des données/documents soient modifiés
- trouver facilement l'information
- éviter que des documents/données ne soient détruits trop tôt
- éviter que des documents/données soient conservés trop longtemps

.reduite[![image de saut en parachute](./media/gestionRisque.png)]

---

### Contraintes réglementaires et risques - Gestion de la qualité

Le records management impose aux producteurs de documents de considérer le document, quel que soit son support, **dès sa création**, en fonction des **différentes valeurs** qu’il peut avoir et **des fonctions** qu’il remplit.

![schéma exigence / satisfaction](./media/qualite.png)

---

### Contraintes réglementaires et risques - Evaluation de la production

Le but du records management est de **lier un document à l'activité qui l'a produit et au contexte dans lequel il a été créé** avec des moyens suffisants pour qu'il puisse ultérieurement être **accepté comme authentique** et représentatif de l'information d'origine.

![tableau des critères de sélection de l'information](./media/critereselectioninformation.png)

---

### Contraintes réglementaires et risques - Certification de la production

.pull-left[
Le mécanisme de signature doit permettre (selon art. 1316-1 du Code civil) :

- **d’identifier / authentifier** l’auteur d’un document (traçabilité)
- de **garantir** que l’auteur a bien émis ce document et pas un autre
  (intégrité : le document n’a pas été altéré entre l‘émission et le moment où le lecteur le consulte)

Ce mécanisme est cependant lourd à mettre en oeuvre et présente des contraintes pour la conservation à long terme.

Une gestion efficace des métadonnées peut permettre de produire la même valeur (identification de l'exemplaire valide) sans la lourdeur du dispositif technique :

- métadonnées administratives : habilitations et classification
- métadonnées de gestion : identification, provenance et contexte de production
- métadonnées descriptive : catégorisation, mise en relation, et désambiguïsation
  ]
  .pull-right[![illustration processus signature électronique](./media/signature.jpg)
  ![exemple prov-o](./media/expanded-terms-example-bundlePost.svg)]

---

### Contraintes réglementaires et risques - Gestion de la preuve

> Intégrité : le respect cumulé de trois critères

- la lisibilité du document
- la stabilité du contenu informationnel
- la traçabilité des opérations sur le document

Dans la mesure où devant le juge, se posent les questions de recevabilité et de force probante des documents électroniques archivés, les critères de l’intégrité doivent impérativement être édictés afin de permettre de définir les conditions dans lesquelles un document conservé pourra avoir valeur probante.

---

### Contraintes réglementaires et risques - RGPD

Le règlement général de protection des données vient renforcer l'importance des processus de gestion des documents d'activité et d'archivage :

- cartographie des flux de données et documents
- mise en oeuvre des finalités de traitement
- élimination des informations au-délà de leur durée d'utilité
- conception d'applications intégrant la protection des donnes par défaut (Privacy by desin)

![schema interface - organisation rgpd](./media/rgpd-cnil.jpg)

---

### Contraintes réfglementaires et risques - Open data

L'**ouverture des données** (et des algorithmes) est une obligation pour les collectivités publiques (et les délégataires ou associatiosn exercant une mission de service public) renforcée par la Loi pour une République Numérique de 2018.

Elle constitue une opportunité pour **valoriser le patrimoine informationnel** d'une organisation et favoriser les interactions avec des (ré-) utilisateurs dans un contexte d'écosystème ouvert.

![données ouvertes!!!](./media/donneesOuvertes.jpg)

---

### Contraintes réfglementaires et risques - Open data

Parmi les bénéfices on peut citer :

- contribution à l'urbanisation du SI (mise en place de référentiels de données, services web de consommations mutualisé pour l'interne et l'externe)
- valorisation des compétences internes et amélioration de la culture de la donnée des collaborat.eur.rice.s
- transparence et capacité à rendre compte comme pivot d'une gouvernance partagée de la donnée

![open data spectrum](./media/opendataSpectrum.png)

### La gestion de l'archivage au sens du records management - production vs. conservation

![illustration du désert B de Moebius](./media/desertB.jpg)

---

### La gestion de l'archivage au sens du records management - Parallélisme des âges

![schéma d'illustration du parallélisme des besoins de gestion](./media/paralleleActivite.png)

---

### La gestion de l'archivage au sens du records management - Organisation

.pull-left[

La dématérialisation produit ou renforce de nouveaux processus :

- Gestion des **droits** : la mise à jour de l'annuaire devient critique
- Gestion des **circuits** / processus dématérialisés
  - maintenance,
  - nouveaux processus,
  - adaptation vs réorganisation, ...
- Gestion des **référentiels** de métadonnées : outils et correspondants métiers sont essentiels

]
.pull-right[![Diversité des activités](./media/circuitProcessus.jpg)]

.footnote[Illustration: Edwin D. Babbitt’s The Principles of Light and Color (1878) — [Source](https://archive.org/details/gri_c00033125011227010).]

---

### La gestion de l'archivage au sens du records management - Organisation

.pull-left[

- Gestion de l'enrichissement des documents grâce aux référentiels : comment les utilisateurs enrichissent leurs documents, à quelle dose, comment ?
- Gestion de la **contribution** des utilisateurs aux référentiels
- Gestion documentaire: les basiques du classement et des fonctionnalités associées.
- La **recherche** d'information :c'est un processus qui doit être clair pour les utilisateurs.
- Le travail **collaboratif** : méthodologie à mettre en place.
- **Architecture** de l'information : un nouveau métier, une fonction dédiée ?

]
.pull-right[![illustration architecture futuriste](./media/architectureInformation.jpg)]

.footnote[Illustration: NASA Ames Research Center (1970) — [Source](https://settlement.arc.nasa.gov/70sArt/art.html).]

---

### La gestion de l'archivage au sens du records management - Collectivisation de la production

Proposer de passer d'une logique de gestion **individuelle inconsistante, éphémère et isolée** à une logique de gestion **collective organisée, contrôlée et partagée**

![Collectivisme documentaire](./media/collectivisation.jpg)

---

### La gestion de l'archivage au sens du records management - Enjeux pour l'organisation

### Performance et connaissance

- Réduire les coûts : **conserver efficacement** les informations critiques et y accéder rapidement
- Se conformer à la **règlementation**

--

- Le capital immatériel de la collectivité est un **levier majeur** de la chaîne de valeur
- Il répond à un besoin croissant de **collaboration et de communication**
- Il faut mettre en place les moyens pour **exploiter avec efficience** ces contenus

---

### La gestion de l'archivage au sens du records management - Enjeux pour l'organisation

### Risques juridiques et financiers

.pull-left[

- Coût global de stockage qui s’élève
- Coûts passé à rechercher et à extraire les données
- Un éparpillement dans des outils différents
- Pas de « référentiels »
- Une gestion désordonnée entraîne des pertes de temps et d’informations
- Pas de capitalisation de la connaissance commune
- Perte d’identité et contentieux
  ]
  .pull-right[
  ![baie de stockage informatique](./media/stockage.jpg)]

---

### La gestion de l'archivage au sens du records management - Gouvernance documentaire

![schéma des briques du système d'information](./media/gouvernancedocumentaire.png)

---

### La gestion de l'archivage au sens du records management - Gestion transverse de l'information

![schéma de la relation entre producteur et utilisateur de l'information](./media/gestiontransverseinformation.jpg)

---

### La gestion de l'archivage au sens du records management - Moyens transverses

![illustration des moyens transverses](./media/moyenstransverses.PNG)

---

### La gestion de l'archivage au sens du records management - Gestion

![illustration des membres de la dream team](./media/dreamteam.png)

---

### La gestion de l'archivage au sens du records management - Politique de gestion de l'information

> Définition du cycle de vie de l'information
> -> une analyse des **processus** de travail + **réglementation** + **besoins** de gestion

- **Identification** des documents liés à la continuité de l’activité
- Définition des **durées de conservation**
  (combien de temps les documents seront-ils nécessaires) ? À partir de quel moment peut-on les détruire ?)
- Définition des règles de **communicabilité**
  (quel est le délai pour rendre une information librement accessible à tous ?)

---

### La gestion de l'archivage au sens du records management - La politique d'archivage électronique

.reduite[![graphique illustrant les documents composant la politique d'archivage électronique](./media/referentielpolitiquearchivage.png)]

---

### La gestion de l'archivage au sens du records management - Référentiel documentaire PA

- **PSA/PA** : politique de service d'archivage / politique d'archivage
- **MOO** : mise en oeuvre opérationnelle
- **DPA** : déclaration des pratiques d'archivage (vise ensuite à définir comment l'AA s'organise pour répondre aux objectifs et engagements de la (des) PA ainsi qu'à identifier les procédures opérationnelles et les moyens mis en œuvre pour cela)
- **PSSI** : politique de sécurité des systèmes d'informations

[ressource complémentaire SSI](https://www.ssi.gouv.fr/archive/fr/confiance/documents/methodes/ArchivageSecurise-P2A-2006-07-24.pdf)

---

#### Savoir définir un projet de versement d’archivage électronique dans un SAE.

Les grandes étapes :

1. Cartographie de la production : tableau de gestion et cartographie des flux de données
2. Rédaction des profils de versement
3. Définition des modalités de versement (contrat de versement)
4. Définition de la procédure de communication
5. Définition de la procédure d'élimination
6. Formation et accompagnement des producteurs

---

### Mise en oeuvre - Pré-requis techniques et fonctionnels

![schéma des briques techniques et fonctionnelles pour le SAE](./media/saeTechniquefonctionnel.png)

---

### SAE - Architecture fonctionnelle

![schéma des briques techniques et fonctionnelles pour le SAE](./media/systeme_archivage_electronique_2.jpg)

---

### Avec VITAM par exemple

![vitam architecture](./media/SchemaSAEVitam.jpg)

---

### Qui fait quoi où avec VITAM ?

![fonctionnalités VITAM](./media/vitamfonctions.png)

---

### Par où commencer ?

| étape               |     solutions applicatives     |                           solutions humaines                           |                                     documentation                                     |
| ------------------- | :----------------------------: | :--------------------------------------------------------------------: | :-----------------------------------------------------------------------------------: |
| première étape      | stockage organisé et documenté |                       gestion de la conservation                       |                                 politique d'archivage                                 |
| cible intermédiaire |   Données de référence / SAE   |         gestion des référentiels / gestion de la conservation          |                    guide bonnes pratiques / politique d'archivage                     |
| cible RM            |              Ged               |     gestion de la production courant / identification des records      |                             guide de nommage / indexation                             |
| cible SAE           |    Référentiel / GED / SAE     | gestion des référentiels / record manager / gestion de la conservation | politique de gestion des données / politique d'archivage / Politique de gestion du SI |

---

### La première étape

![SAE alpha](./media/trotinette.jpg)

---

### Itération et amélioration continue

![SAE beta](./media/mobylette.jpg)

---

### Le lean canvas pour itérer sur la vision

1. Identifier les parties prenantes (éventuellement les premiers utilisateurs de la solution mise en place)
2. Identifier leurs problèmes (et les solutions (de contournement) qu’ils utilisent)
3. Identifier les solutions disponibles
4. Identifier les critères d’évaluation de la valeur apportée par la solution envisagée
5. Identifier les critères de légitimité du(des) porteur de projets
6. Identifier les canaux de communication mobilisables pour faire parler de son projet
7. Identifier le concept à haute valeur ajoutée et la proposition de valeur associée au projet
8. Identifier les coûts
9. Identifier les revenus ou les gains

---

### Choix du scenarii en fonction :

- des ressources humaines nécessaires,
- des moyens techniques à mobiliser,
- des moyens financiers à prévoir,
- de la volonté stratégique,
- la maturité vis-à-vis du numérique et des archives.

et pour les acteurs publics de **l’environnement institutionnel et du contexte territorial**

![la balance coût / service](./media/ecoEchelleAnime.gif)

---

class:center,top

### Votre projet : version classique

![Les étapes du projet de SAE](./media/syntheseArchivisteSI.png)

---

class: center, top

### MVP

![canvas vierge](./media/canvasvierge.png)

<!--
* « Logiciel fonctionnel plutôt que documentation complète » : il est vital que l'application fonctionne. Le reste, et notamment la documentation technique, est secondaire, même si une documentation succincte et précise est utile comme moyen de communication.

* « Collaboration avec le client plutôt que la négociation du contrat » : le client doit être impliqué dans le développement. On ne peut se contenter de négocier un contrat au début du projet, puis de négliger les demandes du client.

* « Réagir au changement plutôt que suivre un plan » : la planification initiale et la structure du logiciel doivent être flexibles afin de permettre l'évolution de la demande du client tout au long du projet.

-->

---

### exercice

1. Remplir le canvas pour mettre en place votre première itération

2. [correction](corrigeExercice.md)
3. [correction sae](corrigeExercice.md)

---

### Les métadonnées

![métaphore métadonnées comme partie imergée iceberg](./media/icebergMetadonnees.jpg)

---

#### Introduction sur les Métadonnées en général

**définition**

> Une métadonnée est une donnée servant à **définir ou décrire** une autre donnée. **Porteuse d'information** sur le **contexte**, le **sens** et la **finalité** de la ressource informationnelle portée par la **donnée brute**.

![explication des métadonnées par un métaphore sur le vin](./media/vinContexte.PNG)

---

### Introduction sur les métadonnées en général - Structuration de l'information

La structuration de l’information est la clé qui assure la versatilité des usages qui peuvent être capitalisés à partir d’un même processus de description

![schéma des différents types de métadonnées](./media/metadonnees.png)

---

### Le modèle d’information de l’OAIS - la place des métadonnées

### L'objet information

.pull-left[
l’Objet Information est composé d’un **objet données**
(physique ou numérique) et de l’information de représentation qui permettent d’interpréter les données sous la forme d’une **information compréhensible**.
]
--
.pull-right[.reduite[![schéma oais objet information](./media/objetinformation.png)]
]

---

### Le modèle d'information de l'OAIS - l'information de représentation

.pull-left[
Basée sur la récursivité, elle permet de représenter les différentes composantes de l'information

![schéma OAIS de l'information de représentation](./media/informationrepresentation.png)
]
--
.pull-right[
**Composition**

Elle peut être composée des typologies suivantes :

- **Information de structure** :
  types de données courants en informatique, comme des caractères, des nombres, des pixels, des tableaux

- **Information sémantique** :
  informations complémentaires associées aux éléments de structure, telles que la langue dans laquelle l’objet est exprimé, les opérations réalisables sur chaque type de données et leurs relations, etc.

- **Autres**:
  Identifiants d’autres standards comme la référence à la norme ASCII
  ]

---

### Le modèle d'information de l'OAIS - L'information de pérennisation

.pull-left[
Elle doit permettre d'expliciter le contexte de production

![schéma OAIS de l'information de pérennnisation](./media/informationperennisation.PNG)
]
--
.pull-right[
Elle peut être composée des typologies suivantes :

- **Information de provenance** : décrit l’origine du contenu d’information, qui en a la charge, et quel est l’historique de ses modifications

- **Information de contexte** : décrit comment le contenu d’information s’articule avec d’autres informations à l’extérieur du paquet

- **Information d’identification** : fournit un ou plusieurs identifiants ou systèmes d’identification

- **Information de droits d’accès** (Access Rights Information) : information qui identifie les restrictions d’accès portant sur l’information de contenu

- **Information d’intégrité** : protège le contenu d’information contre les altérations non documentées
  ]

---

### Quelles métadonnées pour la pérennisation ?

---

### Quelles métadonnées pour la pérennisation - Décrire les objets et garantir leur traçabilité

Avec le modèle de données PROV, on cherche à modéliser 3 types d'objets :

- Agents : qui a créé, qui a modifié, qui a contribué, qui a transformé, etc..
- Activités : création, modification, contribution, suppression, archivage, etc…
- Entités : fichier, dossier, collection manipulés par des agents au travers d'activités

.reduite[
![le modèle prov-o](./media/prov-o.png)
]

---

### Quelles métadonnées pour la pérennisation - Le modèle Prov-O

Dans le modèle PROV, une entité est une ressource dont on veut décrire la provenance.

Les activités sont **les processus** qui ont utilisé ou généré des entités, comme par exemple : calculer un résultat,
écrire un livre, faire une présentation. Les activités ne sont pas des entités.

Les agents sont **responsables des activités** affectant les entités.

Un agent est quelque chose qui porte une forme de responsabilité dans le déroulement d'une activité, dans l’existence
d’une entité ou dans l’activité d’un autre agent.

---

### Quelles métadonnées pour la pérennisation - Le modèle Prov-o étendu

![le modèle étendu](./media/prov-o-extended.png)

---

### Quelles métadonnées pour la pérennisation - Premis : conserver les données numériques

L'ontologie Premis [v3 2018](http://www.loc.gov/standards/premis/ontology/owl-version3.html) est publiée par la librairie du Congrès et reprend de nombreuses propriétés de Prov pour **décrire les objets numériques en lien avec les agents qui les manipulent et les évènements qui les affectent**.

2 des principales entités sont des sous-classes d'entités **provenant d'autres modèles de données** :

- agent : sous-classe de foaf:agent et prov:agent
- event : sous-classe de prov:activity

L'entité object a pour sous-classe premis:file, premis:intellectualEntity, premis:reprsentation, premis:bitSteam

L'entité Rights est propre à l'ontologie et définit les droits associés aux agents et aux objets

---

### Quelles métadonnées pour la pérennisation - Premis: le modèle de données

![premis model](./media/Premis.jpg)

---

### Quelles métadonnées pour la pérennisation - SEDA

> Le standard d'échange de données pour l'archivage **modélise les différentes transactions** qui peuvent avoir lieu **entre des acteurs** dans le cadre de l'**archivage de données**.

> Il précise les messages échangés ainsi que les métadonnées à utiliser pour **décrire, gérer et pérenniser** l’information.

---

### Quelles métadonnées pour la pérennisation - SEDA

Il vise à :

- proposer un **cadre d'interopérabilité** pour la dématérialisation des processus d'archivage
- **structurer les messages** renforcant la valeur probante des archives
- **relier des unités de description** au sein du processus de versement

![illustration du pourquoi du SEDA](./media/allInOneSeda.png)

---

name: structuration

### Quelles métadonnées pour la pérennisation - Les acteurs du SEDA

.pull-left[
![Les 5 acteurs du SEDA](./media/acteursSEDA2.png)
]
.pull-right[
Les acteurs sont au nombre de cinq :

- le service producteur,
- le service versant,
- le service d'archives,
- le service de contrôle,
- le demandeur d'archives.

> Ils peuvent avoir plusieurs rôles en même temps
> ]

---

### Quelles métadonnées pour la pérennisation - Les transactions du SEDA

.pull-left[

![Les transactions dans le SEDA](./media/transactionsSEDA.png)
]

.pull-right[
**Les transactions** sont au nombre de six :

- la demande de transfert,
- le transfert,
- la modification,
- l'élimination,
- la communication,
- la restitution.

]

---

### Quelles métadonnées pour la pérennisation - Structurer les message en SEDA

.pull-left[

![traçabilité des échanges](./media/structureMsg.png)
]

.pull-right[

- types d'acteurs
  - service producteur (OriginatingAgency)
  - service versant (TransferingAgency)
  - service d'archives (ArchivalAgency)
  - service de contrôle (AuthorizationControlAuthority)
- représentation
  - messages XML (Request / Reply / Notification / Acknowledgement)
  - schémas de validation (RNG ou XSD)
    ]

---

### Quelles métadonnées pour la pérennisation - Modèle conceptuel du transfert en SEDA v2

![le schéma SEDA V2](./media/schemaSeda2.png)

---

### Quelles métadonnées pour la pérennisation - SEDA : typologies de métadonnées

Le SEDA utilise quatre sortes de **métadonnées** :

![illustration métadonnées seda](./media/metadataSeda.png)

---

### Quelles métadonnées pour la pérennisation - SEDA : la description des évènements

Depuis le SEDA 2.0, il est possible de décrire des évènements suivant 3 significations

.reduite[![les évènements SEDA](./media/seda2Event.png)]

---

### Quelles métadonnées pour la pérennisation - Premis :Event

Les métadonnées de Event sont inspirées de [PREMIS](http://www.loc.gov/standards/premis/).

.reduite[![premis event](./media/premisEvent.png)]

???

- Pour lister les événements métier intervenus sur une unité d’archives (Content (métadonnées descriptives)).
- Pour lister les événements survenus lors de la gestion du cycle de vie des unités d’archives ou lors de la génération des DataObjectPackage échangés (
  LogBook (métadonnées de gestion)).
- Pour consigner les traces d’un journal des opérations sur l’ensemble du message (LogBook (métadonnées de gestion)). Correspond
  à la transmission des événements tracés dans le journal des événements au sens de la NFZ 42-013

---

### Quelles métadonnées pour la pérennisation - SEDA : Liens avec d'autres schéma de description

![illustration des liens](./media/webLinkedData.png)

---

### Encoder les métadonnées - Standards

- Le [XML](https://fr.wikipedia.org/wiki/XML) : syntaxe élémentaire, pour la structure du contenu dans les documents, mais il ne décrit pas la sémantique du document.
- Le [XSD](https://fr.wikipedia.org/wiki/XSD) : langage de description de format de document XML permettant de définir la structure et le type de contenu d'un document XML. Cette définition permet notamment de vérifier la validité de ce document.
- Le [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework) : langage simple pour exprimer des [modèles de données](https://fr.wikipedia.org/wiki/Modèle_de_données) sous forme d'objets ([« ressources »](https://fr.wikipedia.org/wiki/Ressource_du_World_Wide_Web)) et de leurs relations.
- [RDF Schema](https://fr.wikipedia.org/wiki/RDF_Schema) étend le RDF et son vocabulaire pour pouvoir structurer les propriétés et les classes au sein d'une ressource décrite en RDF.
- [OWL](https://fr.wikipedia.org/wiki/Web_Ontology_Language) ajoute plus de vocabulaire pour décrire les propriétés et les classes : comme avec les relations entre les classes, la [cardinalité](<https://fr.wikipedia.org/wiki/Cardinalité_(programmation)>), l'égalité, le typage des propriétés, les caractéristiques de propriétés (par exemple la symétrie), etc.
- [SPARQL](https://fr.wikipedia.org/wiki/SPARQL) : un [langage de requête](https://fr.wikipedia.org/wiki/Langage_de_requête) et un [protocole](https://fr.wikipedia.org/wiki/Protocole_de_communication) qui permet de rechercher, d'ajouter, de modifier ou de supprimer des données [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework) disponibles dans le Web à travers l'internet.

---

### Encoder les métadonnées - standard du web...de données

.reduite[![Les standards du WEB de données](./media/Semantic_Web_Stack.png)]

.credit[
Crédits: [Wikipédia](http://it.wikipedia.org/wiki/File:W3C-Semantic_Web_layerCake.png)

]

---

### Encoder les métadonnées - Le modèle de description RDF : Définition

> Définition : Le [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework) ou **_Resource Description Framework_** est un langage simple pour exprimer des [modèles de données](https://fr.wikipedia.org/wiki/Modèle_de_données) sous forme d'objets ([« ressources »](https://fr.wikipedia.org/wiki/Ressource_du_World_Wide_Web)) et de leurs relations.

Un document structuré en RDF est un ensemble de triplets.

Un triplet est toujours construit suivant la forme suivante : **sujet → prédicat → objet**

- Le **sujet** représente la **ressource** à décrire ;
- Le **prédicat** représente un **type de propriété** de la **ressource** ;
- L'**objet** représente **la valeur** de la **propriété**.

---

### Encoder les métadonnées - Le triplet RDF

![Triplet RDF](./media/triplet-RDF2.png)

---

### Encoder les métadonnées - Le modèle de description RDF : exemple

![Triplet RDF](./media/triplet-RDF.png)

---

### Encoder les métadonnées - URI et URL

> Un **URI**, _Uniform Resource Identifier_, soit littéralement **_identifiant uniforme de ressource_**, est une courte chaîne de caractères identifiant une ressource sur un réseau (par exemple une [ressource Web](https://fr.wikipedia.org/wiki/Ressource_Web)) physique ou abstraite, et dont la [syntaxe](https://fr.wikipedia.org/wiki/Syntaxe) respecte une norme d'Internet mise en place pour le Web

> Un URI doit permettre d'identifier une ressource de manière permanente, même si la ressource est déplacée.

L'URI regroupe deux types d'identifiants :

- Les URL : _Uniform Resource Locator_, littéralement « localisateur uniforme de ressource » pour adresser les ressources internet. Une URL identifie en fait l'emplacement d'une ressource, plutôt que la ressource elle-même ;
- Les URN : _Uniform Resource Name_, littéralement « nom uniforme de ressource »

> **Un des pilliers du Web de données** : utiliser des adresses URI uniques pour identifier les choses

---

### Encoder les métadonnées - les URI

Les URI (Unique Ressource Identifier) sont les cléfs de voute du Web

Syntaxe :

.remark-code.inline[

scheme:protocole://chaînede.caractère/ressource

http://monsite.com/dossier/fichier.html#ancre

http://mondomaine.org/ressource/1234
]

**Composition** :

- un préfixe qui indique le contexte dans lequel l’identifiant est attribué (par ex. http:,ftp:, urn:, etc.)
- un élément qui permet de désigner l’autorité nommante qui a attribué l’identifiant au sein de ce système
- le « nom » lui-même, c'est-à-dire une chaîne de caractères qui identifie la ressource de manière unique, au sein de ce système et pour cette autorité.

Les Url sont des URI qui identifient la ressource par le moyen d'y accéder

Les Url peuvent être pérennes à condition d'adopter quelques bonnes pratiques.

---

### Encoder les métadonnées - URI : le système de gestion ark

Ark propose un système d'attribution d'identifiants pour garantir leur unicité :

- Attribution centralisée d'un identifiant d'autorité nommante
- Les parties Name et qualifier sont construites sous la forme d’une chaine de caractères aléatoires avec les contraintes suivantes :
- une chaîne de caractères alphanumériques avec une séquence en base 29 :
  - uniquement des consonnes (sauf l) et des chiffres 0 à 9
  - la partie Name commence par un préfixe « rf » fixant le contexte d’attribution des identifiants
  - une longueur fixe de 10 caractères

Exemple :

.remark-code.inline[

identifiant de vocabulaire : ark:/25651/rf5v812szg

identifiant d'un concept dans le vocabulaire : ark:/25651/rf5v812szg/f7q82r9zhs

adresse du concept : https://referentiel.saemgirondin.fr/ark:/25651/rf5v812szg/f7q82r9zhs
]

---

### Encoder les métadonnées - Le modèle de description RDF : plus précisément

Le sujet est toujours un **URI**.

Toute “chose” sur laquelle on veut faire des assertions (sujet) doit avoir un URI.

Les “choses” ont toujours **un type** : **une CLASSE**.

Le prédicat est toujours un **URI**.

Il permet d'exprimer les **propriétés** des “choses”, ou les **relations** des “choses” entre elles

L‘objet peut être un **texte** (littéral) ou un **URI**.

Les **classes** et les **propriétés** sont déclarées dans des **vocabulaires** pour être réutilisées.

---

name: outils

### L’accès à l’information grâce aux métadonnées.

---

### L’accès à l’information grâce aux métadonnées - Qu’est ce que le Web de données ?

Le **Web des données** (_linked data_, en anglais) cherche donc à favoriser la publication de données structurées sur le Web, non pas sous la forme de silos de données isolés les uns des autres, mais en les reliant entre elles pour constituer un réseau global d'informations.

Pour le domaine de la Culture il permet :

- de gagner en visibilité.
- de décloisonner les données des catalogues et des instruments de recherche pour les rendre, afin plus accessibles aux usagers et ceux en devenir.
- de relier des données d'archives, de musées ou des bibliothèques pour offrir au chercheur une information plus complète.

---

### L’accès à l’information grâce aux métadonnées - Qu’est ce que le Web de données ?

.reduite[![De la ratatouille pour les data](./media/ratatouille.jpg)]

.credit[Crédits: Gautier Poupeau [notice Viaf](https://viaf.org/viaf/305852061/#Poupeau,_Gautier)]

---

### L’accès à l’information grâce aux métadonnées - Qu’est ce que le Web de données ?

.reduite[![De la datatouille pour les data](./media/datatouille.jpg)]

.credit[
Crédits: Gautier Poupeau [notice Viaf](https://viaf.org/viaf/305852061/#Poupeau,_Gautier)
]

---

### L’accès à l’information grâce aux métadonnées - Qu'est ce que le web de données ?

Je veux décrire des archives avec des informations sur les producteurs
et gérer les liens entre les différentes formes de représentation de ces archives conservées dans un lieu particulier et qui peuvent avoir des archives ou d'autres documents (livres) liés conservés dans des lieux différents.

--

Qu’est-ce que je souhaite décrire ?

- des archives = un intitulé, une description, des dates, des lieux, des sujets
- Un producteur = un nom, un prénom / un nom d'organisme, des dates, des lieux
- Des fonctions = des relations, des organisations, des mandats et des activités
- Des lieux de conservation : une provenance, des activités, des relations

--

Les ingrédients existants

- spécialisés : EAC / SEDA / EAD
- généralistes : Dublin Core / FOAF / wgs84-pos / time / SKOS / schema.org / PROV-O
- pour lier : RDF / RDFS / OWL

---

### L’accès à l’information grâce aux métadonnées - Visualisation de métadonnées archivistiques

Mise en ligne du prototype français PIAAF

« **Pilote d’interopérabilité pour les autorités archivistiques françaises** » (PIAAF) :

Objetif : constituer une première expérimentation dans l’application des technologies du web sémantique à la description archivistique.

Un partenariat : SIAF, Archives nationales, BNF et le CRIHN

Un socle logiciel : CubicWeb de Logilab

Disponible en ligne : http://piaaf.demo.logilab.fr/

---

### L’accès à l’information grâce aux métadonnées - Le prototype en ligne

.reduite[![PIAAF](./media/PIAAF.png)]

---

### L’accès à l’information grâce aux métadonnées - Au-delà des tranchées

Un [projet](http://www.canadiana.ca/rpcpd-dol) des données ouvertes liées autour de la Première Guerre mondiale

![Canadiana](./media/canadiana.png)

---

### L’accès à l’information grâce aux métadonnées - Linked Jazz

![(https://linkedjazz.org/network/)](./media/Linked-jazz.png)

site web : [Linked Jazz](https://linkedjazz.org/network/)

---

### L’accès à l’information grâce aux métadonnées - The New-York Public Library Archives & Manuscrits

![(http://archives.nypl.org/terms/)](./media/NYPL-1.png)

![The New-York Public Library](./media/NYPL-2.png)

---

### Outillage - produire des données structurées

- faire des profils : [SHERPA](https://francearchives.fr/sherpa/)
- visualiser des arborescences : [Archifiltre](https://archifiltre.github.io/)
- traiter des vracs numériques : [OCTAVE](https://francearchives.fr/fr/article/88482499)
- préparer des SIP pour VITAM : [ReSIP](http://www.programmevitam.fr/pages/ressources/resip/)
- exporter des bases de données : [SiardSuite](https://www.bar.admin.ch/bar/fr/home/archivage/outils-et-instruments/siard-suite.html)

![les outils](./media/outillage.jpg)

---

### Rétrospective

**Merci pour votre attention !**

--

1. Les points satisfaisants
2. les points à améliorer

--

A votre disposition pour en parler :

- par mail : pascal@rhizome-data.fr
- sur twitter : @keronos
